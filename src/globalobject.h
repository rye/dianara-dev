/*
 *   This file is part of Dianara
 *   Copyright 2012-2015  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#ifndef GLOBALOBJECT_H
#define GLOBALOBJECT_H

#include <QObject>
#include <QSettings>
#include <QFont>
#include <QStringList>
#include <QSize>
#include <QStandardItemModel>

#include <QDebug>


class GlobalObject : public QObject
{
    Q_OBJECT

public:
    explicit GlobalObject(QObject *parent = 0);
    ~GlobalObject();


    // General options
    void syncGeneralSettings();

    // Font options
    void syncFontSettings(QString postTitleFont,
                          QString postContentsFont,
                          QString commentsFont,
                          QString minorFeedFont);
    QString getPostTitleFont();
    QString getPostContentsFont();
    QString getCommentsFont();
    QString getMinorFeedFont();


    // Color options
    void syncColorSettings(QStringList newColorList);
    QStringList getColorsList();
    QString getColor(int colorIndex);


    // Timeline options
    void syncTimelinesSettings(int pppMain, int pppOther,
                               int minorFeedSnippets, int snippetsChars,
                               bool showDeleted, bool hideDuplicates,
                               bool jumpToNew);
    int getPostsPerPageMain();
    int getPostsPerPageOther();
    int getMinorFeedSnippetsType();
    int getSnippetsCharLimit();
    bool getShowDeleted();
    bool getHideDuplicates();
    bool getJumpToNewOnUpdate();



    // Post options
    void syncPostSettings(int postAvatarSizeIndex, bool extendedShares,
                          bool showExtraInfo, bool hlAuthorComments,
                          bool hlOwnComments, bool postIgnoreSslInImages);
    int getPostAvatarSizeIndex();
    QSize getPostAvatarSize();
    bool getPostExtendedShares();
    bool getPostShowExtraInfo();
    bool getPostHLAuthorComments();
    bool getPostHLOwnComments();
    bool getPostIgnoreSslInImages();


    // Composer options
    void syncComposerSettings(bool publicPosts, bool filenameAsTitle,
                              bool showCharCounter);
    bool getPublicPostsByDefault();
    bool getUseFilenameAsTitle();
    bool getShowCharacterCounter();


    // Privacy options
    void syncPrivacySettings(bool silentFollows, bool silentLists);
    bool getSilentFollows();
    bool getSilentLists();


    // Notification options
    void syncNotificationSettings();


    // Tray options
    void syncTrayOptions(bool hideInTrayStartup);
    bool getHideInTray();


    ///////////////////////////////////////////////////////////////////////////


    void createMessageForContact(QString id, QString name, QString url);

    void browseUserMessages(QString userId, QString userName,
                            QIcon userAvatar, QString userOutbox);

    void editPost(QString originalPostId,
                  QString type,
                  QString title,
                  QString contents);

    QStandardItemModel *getNickCompletionModel();
    void addToNickCompletionModel(QString id, QString name, QString url);
    void removeFromNickCompletionModel(QString id);
    void clearNickCompletionModel();
    void sortNickCompletionModel();

    void setStatusMessage(QString message);
    void logMessage(QString message, QString url="");

    void storeTimelineHeight(int height);
    int getTimelineHeight();


signals:
    void messagingModeRequested(QString id, QString name, QString url);

    void userTimelineRequested(QString userId, QString userName,
                               QIcon userAvatar, QString userOutbox);

    void postEditRequested(QString originalPostId,
                           QString type,
                           QString title,
                           QString contents);

    void messageForStatusBar(QString message);
    void messageForLog(QString message, QString url);


public slots:


private:
    // General options


    // Font options
    QString postTitleFontInfo;
    QString postContentsFontInfo;
    QString commentsFontInfo;
    QString minorFeedFontInfo;


    // Color options
    QStringList colorsList;


    // Timeline options
    int postsPerPageMain;
    int postsPerPageOther;
    int minorFeedSnippetsType;
    int snippetsCharLimit;
    bool showDeletedPosts;
    bool hideDuplicatedPosts;
    bool jumpToNewOnUpdate;


    // Post options
    int postAvatarSizeIndex;
    QSize postAvatarSize;
    bool postExtendedShares;
    bool postShowExtraInfo;
    bool postHLAuthorComments;
    bool postHLOwnComments;
    bool postIgnoreSslInImages;


    // Composer options
    bool publicPostsByDefault;
    bool useFilenameAsTitle;
    bool showCharacterCounter;


    // Privacy options
    bool silentFollowing;
    bool silentListsHandling;


    // Notification options


    // Tray options
    bool hideInTray;


    //////////////////////////////////////////////////////////////////////////

    // Other stuff
    QStandardItemModel *nickCompletionModel;
    int timelineHeight;
};

#endif // GLOBALOBJECT_H
