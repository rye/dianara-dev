/*
 *   This file is part of Dianara
 *   Copyright 2012-2015  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#include "listsmanager.h"

ListsManager::ListsManager(PumpController *pumpController,
                           QWidget *parent) : QWidget(parent)
{
    this->pController = pumpController;
    connect(pController, SIGNAL(personListReceived(QVariantList,QString)),
            this, SLOT(setPersonsInList(QVariantList,QString)));
    connect(pController, SIGNAL(personAddedToList(QString,QString,QString)),
            this, SLOT(addPersonItemToList(QString,QString,QString)));
    connect(pController, SIGNAL(personRemovedFromList(QString)),
            this, SLOT(removePersonItemFromList(QString)));


    // To show the current lists
    this->listsTreeWidget = new QTreeWidget();
    listsTreeWidget->setColumnCount(2);
    listsTreeWidget->setHeaderLabels(QStringList() << tr("Name") << tr("Members"));
    listsTreeWidget->setSortingEnabled(true);
    listsTreeWidget->sortByColumn(0, Qt::AscendingOrder);
    listsTreeWidget->setAlternatingRowColors(true);
    listsTreeWidget->setSizePolicy(QSizePolicy::Minimum,
                                   QSizePolicy::MinimumExpanding);
    connect(listsTreeWidget, SIGNAL(itemSelectionChanged()),
            this, SLOT(enableDisableDeleteButtons()));


    // Buttons to add/remove people from lists, and the lists; Initially disabled
    this->addPersonButton = new QPushButton(QIcon::fromTheme("list-add-user",
                                                             QIcon(":/images/list-add.png")),
                                            tr("Add Mem&ber"));
    addPersonButton->setDisabled(true);
    connect(addPersonButton, SIGNAL(clicked()),
            this, SLOT(showAddPersonDialog()));

    this->removePersonButton = new QPushButton(QIcon::fromTheme("list-remove-user",
                                                                QIcon(":/images/list-remove.png")),
                                               tr("&Remove Member"));
    removePersonButton->setDisabled(true);
    connect(removePersonButton, SIGNAL(clicked()),
            this, SLOT(removePerson()));


    this->deleteListButton = new QPushButton(QIcon::fromTheme("edit-table-delete-row",
                                                              QIcon(":/images/button-delete.png")),
                                             tr("&Delete Selected List"));
    deleteListButton->setDisabled(true);
    connect(deleteListButton, SIGNAL(clicked()),
            this, SLOT(deleteList()));


    // Groupbox for the "create new list" stuff
    this->newListGroupbox = new QGroupBox(tr("Add New &List"));

    this->newListNameLineEdit = new QLineEdit();
    newListNameLineEdit->setPlaceholderText(tr("Type a name for the new list..."));
    connect(newListNameLineEdit, SIGNAL(textChanged(QString)),
            this, SLOT(enableDisableCreateButton(QString)));
    connect(newListNameLineEdit, SIGNAL(returnPressed()),
            this, SLOT(createList()));


    this->newListDescTextEdit = new QTextEdit();
    newListDescTextEdit->setToolTip(tr("Type an optional description here"));
    newListDescTextEdit->setTabChangesFocus(true);


    this->createListButton = new QPushButton(QIcon::fromTheme("edit-table-insert-row-above",
                                                              QIcon(":/images/list-add.png")),
                                             tr("Create L&ist"));
    createListButton->setDisabled(true); // Disabled until user types a name
    connect(createListButton, SIGNAL(clicked()),
            this, SLOT(createList()));


    // Widget to find and select a contact
    this->peopleWidget = new PeopleWidget(tr("&Add to List"),
                                          PeopleWidget::StandaloneWidget,
                                          this->pController,
                                          this);
    connect(peopleWidget, SIGNAL(addButtonPressed(QIcon,QString,QString)),
            this, SLOT(addPerson(QIcon,QString,QString)));


    // Layout
    this->buttonsLayout = new QHBoxLayout();
    buttonsLayout->addWidget(addPersonButton,    0, Qt::AlignLeft);
    buttonsLayout->addWidget(removePersonButton, 0, Qt::AlignLeft);
    buttonsLayout->addStretch(1);
    buttonsLayout->addWidget(deleteListButton,   0, Qt::AlignRight);


    this->groupboxLeftLayout = new QVBoxLayout();
    groupboxLeftLayout->addWidget(newListNameLineEdit);
    groupboxLeftLayout->addWidget(newListDescTextEdit);

    this->groupboxMainLayout = new QHBoxLayout();
    groupboxMainLayout->addLayout(groupboxLeftLayout);
    groupboxMainLayout->addWidget(createListButton, 0, Qt::AlignBottom);
    newListGroupbox->setLayout(groupboxMainLayout);


    this->mainLayout = new QVBoxLayout();
    mainLayout->addWidget(listsTreeWidget, 5);
    mainLayout->addLayout(buttonsLayout,   1);
    mainLayout->addStretch(1);
    mainLayout->addWidget(newListGroupbox, 3);
    this->setLayout(mainLayout);

    qDebug() << "ListsManager created";
}


ListsManager::~ListsManager()
{
    qDebug() << "ListsManager destroyed";
}



void ListsManager::setListsList(QVariantList listsList)
{
    qDebug() << "Setting person lists contents";

    // Disable to avoid users messing with it while it loads
    this->setDisabled(true);

    this->listsTreeWidget->clear();
    this->personListsUrlList.clear();

    QString listName;
    QString listDescription;
    QString listMembersCount;
    QVariant listId;
    QVariant listUrl;

    foreach (QVariant list, listsList)
    {
        QVariantMap listMap = list.toMap();
        listName = listMap.value("displayName").toString();
        listDescription = listMap.value("content").toString();
        if (!listDescription.isEmpty())
        {
            listDescription.prepend("<b></b>"); // HTMLize it so it gets wordwrapped
        }

        listMembersCount = listMap.value("members").toMap()
                                  .value("totalItems").toString();
        listId = listMap.value("id");
        listUrl = listMap.value("members").toMap().value("url");
        qDebug() << "list ID:" << listId.toString();
        qDebug() << "list URL:" << listUrl.toString();


        QTreeWidgetItem *listItem = new QTreeWidgetItem();
        listItem->setText(0, listName);
        listItem->setToolTip(0, listDescription);
        listItem->setText(1, listMembersCount);
        listItem->setToolTip(1, listDescription);
        listItem->setData(0, Qt::UserRole, listId);
        listItem->setData(0, Qt::UserRole + 1, listUrl);


        this->listsTreeWidget->addTopLevelItem(listItem);

        QString membersUrl = listMap.value("members").toMap()
                                    .value("url").toString();
        this->personListsUrlList.append(membersUrl);
    }

    // Get list of people in each list
    foreach (QString url, personListsUrlList)
    {
        this->pController->getPersonList(url);
    }


    // this->listsTreeWidget->expandAll();
    this->listsTreeWidget->resizeColumnToContents(0);

    // Re-enable
    this->setEnabled(true);
}



QTreeWidgetItem *ListsManager::createPersonItem(QString id, QString name,
                                                QString avatarFile)
{
    QTreeWidgetItem *personItem = new QTreeWidgetItem();

    avatarFile = MiscHelpers::getCachedAvatarFilename(avatarFile);
    QPixmap avatarPixmap = QPixmap(avatarFile);
    if (avatarPixmap.isNull())
    {
        personItem->setIcon(0, QIcon::fromTheme("user-identity",
                                                QIcon(":/images/no-avatar.png")));
    }
    else
    {
        personItem->setIcon(0, QIcon(avatarPixmap));
    }
    personItem->setText(0, name);
    personItem->setText(1, id);


    return personItem;
}



/****************************************************************************/
/********************************* SLOTS ************************************/
/****************************************************************************/


/*
 * Fill one of the person lists with the names and ID's of the members
 *
 */
void ListsManager::setPersonsInList(QVariantList personList, QString listUrl)
{
    qDebug() << "ListsManager::setPersonsInList()" << listUrl;

    // Find out which TreeWidgetItem matches this list
    QTreeWidgetItem  *listItem = 0; // Avoid 'not initialized' warning
    foreach (QTreeWidgetItem *item, this->listsTreeWidget->findItems("", Qt::MatchContains))
    {
        listItem = item;

        // Data in UserRole+1 is the Members Url
        if (listItem->data(0, Qt::UserRole + 1).toString() == listUrl)
        {
            break;
        }
    }


    foreach (QVariant personVariant, personList)
    {
        // FIXME: this should use ASPerson()
        QVariantMap personMap = personVariant.toMap();

        QString id = ASPerson::cleanupId(personMap.value("id").toString());
        QString name = personMap.value("displayName").toString();
        QString avatar = personMap.value("image").toMap().value("url").toString();

        if (listItem != 0) // Ensure it's initialized!
        {
            QTreeWidgetItem *childItem = this->createPersonItem(id, name, avatar);
            listItem->addChild(childItem);
        }
    }
}



void ListsManager::createList()
{
    QString listName = this->newListNameLineEdit->text().trimmed();
    QString listDescription = this->newListDescTextEdit->toPlainText().trimmed();
    listDescription.replace("\n", "<br>");

    if (!listName.isEmpty())
    {
        qDebug() << "Creating list:" << listName;
        this->pController->createPersonList(listName, listDescription);

        this->newListNameLineEdit->clear();
        this->newListDescTextEdit->clear();
    }
    else
    {
        qDebug() << "Error: List name is empty!";
    }
}


void ListsManager::deleteList()
{
    if (listsTreeWidget->currentItem() == 0) // if nothing selected, so NULL...
    {
        return;
    }

    QString listName = this->listsTreeWidget->currentItem()->text(0);

    int confirmation = QMessageBox::question(this, tr("WARNING: Delete list?"),
                                             tr("Are you sure you want to delete %1?",
                                                "1=Name of a person list").arg(listName),
                                             tr("&Yes, delete it"), tr("&No"), "", 1, 1);

    if (confirmation == 0)
    {
        // Avoid the possibility of deleting twice!
        this->deleteListButton->setDisabled(true);

        QString listId = this->listsTreeWidget->currentItem()->data(0,
                                                                    Qt::UserRole).toString();

        this->pController->deletePersonList(listId);
    }
    else
    {
        qDebug() << "Confirmation canceled, not deleting the list";
    }
}


/*
 * Enable or disable the "Create List" button
 *
 */
void ListsManager::enableDisableCreateButton(QString listName)
{
    if (listName.trimmed().isEmpty())
    {
        this->createListButton->setDisabled(true);
    }
    else
    {
        this->createListButton->setEnabled(true);
    }
}


/*
 * Enable or disable the "remove person" and "delete list" buttons
 * according to what's currently selected
 *
 */
void ListsManager::enableDisableDeleteButtons()
{
    // A list is selected
    if (listsTreeWidget->currentItem()->parent() == 0)
    {
        this->deleteListButton->setEnabled(true);
        this->removePersonButton->setDisabled(true);
    }
    else  // One of the items inside a list is selected
    {
        this->deleteListButton->setDisabled(true);
        this->removePersonButton->setEnabled(true);
    }

    // Either way...
    this->addPersonButton->setEnabled(true);
}



void ListsManager::showAddPersonDialog()
{
    if (listsTreeWidget->currentItem() == 0) // No list selected
    {
        return;
    }

    // Show the people widget, which will return the selected contact in a SIGNAL
    this->peopleWidget->resize(400, this->height());
    this->peopleWidget->resetWidget();
    this->peopleWidget->show();
}


void ListsManager::addPerson(QIcon icon, QString contactString,
                             QString contactUrl)
{
    Q_UNUSED(icon)
    Q_UNUSED(contactUrl)
    this->peopleWidget->hide();


    QRegExp contactRE("(.+)\\s+\\<(.+@.+)\\>", Qt::CaseInsensitive);
    contactRE.setMinimal(true);
    contactRE.indexIn(contactString);

    QString personId = contactRE.cap(2).trimmed(); // The part between < and >
    if (personId.isEmpty())
    {
        return;
    }

    QString listId;
    if (listsTreeWidget->currentItem()->parent() == 0) // If a list is selected
    {
        listId = this->listsTreeWidget->currentItem()->data(0, Qt::UserRole).toString();
    }
    else  // If a member is selected, get its matching list
    {
        listId = this->listsTreeWidget->currentItem()->parent()->data(0, Qt::UserRole).toString();
    }

    this->setDisabled(true);
    pController->addPersonToList(listId, "acct:" + personId);
}


void ListsManager::removePerson()
{
    if (listsTreeWidget->currentItem() == 0) // Nothing selected, so it's NULL
    {
        return;
    }

    QString personId = this->listsTreeWidget->currentItem()->text(1);
    QString personName = this->listsTreeWidget->currentItem()->text(0);
    if (personName.isEmpty())
    {
        personName = personId;
    }

    QString listId;
    QString listName;
    if (listsTreeWidget->currentItem()->parent() != 0) // Ensure it's not a list
    {
        listId = this->listsTreeWidget->currentItem()->parent()->data(0, Qt::UserRole).toString();
        listName = this->listsTreeWidget->currentItem()->parent()->text(0);
    }

    int confirmation = QMessageBox::question(this, tr("Remove person from list?"),
                                             tr("Are you sure you want to remove %1 "
                                                "from the %2 list?",
                                                "1=Name of a person, "
                                                "2=name of a list")
                                             .arg(personName).arg(listName),
                                             tr("&Yes"), tr("&No"), "", 1, 1);

    if (confirmation == 0)
    {
        this->setDisabled(true);

        QString id = this->listsTreeWidget->currentItem()->text(1);        
        this->pController->removePersonFromList(listId, "acct:" + personId);
    }
    else
    {
        qDebug() << "Confirmation canceled, not removing" << personName
                 << "from" << listName;
    }
}


/*
 * Add the new item itself after a person is added correctly in PumpController
 *
 */
void ListsManager::addPersonItemToList(QString personId,
                                       QString personName,
                                       QString avatarUrl)
{
    QTreeWidgetItem *newItem = this->createPersonItem(personId,
                                                      personName,
                                                      avatarUrl);

    QTreeWidgetItem *selectedItem = this->listsTreeWidget->currentItem();
    // When the list itself is NOT selected, but a child, point to the parent
    if (selectedItem->parent() != 0)
    {
        selectedItem = selectedItem->parent();
    }    
    selectedItem->addChild(newItem);
    // Update member counter
    selectedItem->setText(1, QString("%1").arg(selectedItem->childCount()));

    this->setEnabled(true);
}


void ListsManager::removePersonItemFromList(QString personId)
{
    if (personId == listsTreeWidget->currentItem()->text(1))
    {
        QTreeWidgetItem *parentList = listsTreeWidget->currentItem()->parent();
        delete listsTreeWidget->currentItem();

        // Update member counter
        parentList->setText(1, QString("%1").arg(parentList->childCount()));
    }

    this->setEnabled(true);
}
