/*
 *   This file is part of Dianara
 *   Copyright 2012-2015  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#include "logviewer.h"

LogViewer::LogViewer(QWidget *parent) : QWidget(parent)
{
    this->setWindowTitle(tr("Log") + " - Dianara");
    this->setWindowIcon(QIcon::fromTheme("text-x-log",
                                         QIcon(":/images/log.png")));
    this->setWindowFlags(Qt::Window);
    this->setMinimumSize(320, 400);

    QSettings settings;
    this->resize(settings.value("LogViewer/logWindowSize",
                                QSize(760, 560)).toSize());


    QList<QKeySequence> closeShortcuts;
    closeShortcuts << QKeySequence(Qt::Key_Escape);
    closeShortcuts << QKeySequence(Qt::Key_F12);

    closeAction = new QAction(this);    
    closeAction->setShortcuts(closeShortcuts);
    connect(closeAction, SIGNAL(triggered()),
            this, SLOT(hide()));
    this->addAction(closeAction);


    logTextBrowser = new QTextBrowser(this);
    logTextBrowser->setReadOnly(true);
    logTextBrowser->setOpenExternalLinks(true);


    clearButton = new QPushButton(QIcon::fromTheme("edit-clear-list",
                                                   QIcon(":/images/button-delete.png")),
                                  tr("Clear &Log"),
                                  this);
    connect(clearButton, SIGNAL(clicked()),
            logTextBrowser, SLOT(clear()));

    closeButton = new QPushButton(QIcon::fromTheme("window-close"),
                                  tr("&Close"),
                                  this);
    connect(closeButton, SIGNAL(clicked()),
            this, SLOT(hide()));


    // Layout
    buttonsLayout = new QHBoxLayout();
    buttonsLayout->addWidget(clearButton);
    buttonsLayout->addStretch();
    buttonsLayout->addWidget(closeButton);

    mainLayout = new QVBoxLayout();
    mainLayout->addWidget(logTextBrowser);
    mainLayout->addLayout(buttonsLayout);
    this->setLayout(mainLayout);

    qDebug() << "LogViewer created";
}


LogViewer::~LogViewer()
{
    qDebug() << "LogViewer destroyed";
}



void LogViewer::closeEvent(QCloseEvent *event)
{
    this->hide();
    event->ignore();
}

void LogViewer::hideEvent(QHideEvent *event)
{
    QSettings settings;
    if (settings.isWritable())
    {
        settings.setValue("LogViewer/logWindowSize", this->size());
    }

    event->accept();
}


/*
 * Scroll log to bottom when showing
 *
 */
void LogViewer::showEvent(QShowEvent *event)
{
    this->logTextBrowser->moveCursor(QTextCursor::End);
    event->accept();
}


/****************************************************************************/
/********************************* SLOTS ************************************/
/****************************************************************************/



void LogViewer::addToLog(QString message, QString url)
{
    QString logLine = "<b>["
                      + QDateTime::currentDateTime().toString(Qt::DefaultLocaleShortDate)
                      + "]</b> ";
    logLine.append(message);

    if (!url.isEmpty())
    {
        logLine.append(": <a href=\"" + url + "\">"
                       + MiscHelpers::elidedText(url, 40)
                       + "</a>");
    }

    logTextBrowser->append(logLine);
}


void LogViewer::toggleVisibility()
{
    if (this->isVisible())
    {
        this->hide();
    }
    else
    {
        this->show();
    }
}
