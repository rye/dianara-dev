/*
 *   This file is part of Dianara
 *   Copyright 2012-2015  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#ifndef ASOBJECT_H
#define ASOBJECT_H

#include <QObject>
#include <QVariantMap>

#include <QDebug>

#include "asperson.h"
#include "timestamp.h"


class ASObject : public QObject
{
    Q_OBJECT

public:
    explicit ASObject(QVariantMap objectMap,
                      QObject *parent = 0);
    ~ASObject();

    void updateAuthorFromPerson(ASPerson *person);

    ASPerson *author();

    QString getId();
    QString getType();
    static QString getTranslatedType(QString typeString);
    QString getUrl();
    QString getCreatedAt();
    QString getUpdatedAt();
    QString getLocationName();
    QString getLocationFormatted();
    QString getLocationCountry();
    QString getLocationTooltip();

    QString getDeletedTime();
    QString getDeletedOnString();
    static QString makeDeletedOnString(QString deletionTime);
    QString isLiked();

    QString getTitle();
    QString getSummary();
    QString getContent();

    QString getImageUrl();
    QString getAudioUrl();
    QString getVideoUrl();
    QString getFileUrl();
    QString getMimeType();
    QString getAttachmentPureUrl();

    int getMemberCount();
    QString getMemberUrl();

    QString getLikesCount();
    QString getCommentsCount();
    QString getSharesCount();
    bool hasProxiedUrls();

    QVariantList getLastLikesList();
    QVariantList getLastCommentsList();
    QVariantList getLastSharesList();

    QString getLikesUrl();
    QString getCommentsUrl();
    QString getSharesUrl();

    QVariantMap getOriginalObject();
    QVariantMap getInReplyTo();
    QString getInReplyToId();

    // This one to deprecate
    static QString personStringFromList(QVariantList variantList,
                                        int count=-1);

    static QVariantMap simplePersonMapFromList(QVariantList variantList);
    static void addOnePersonToSimpleMap(QString personId, QString personName,
                                        QString personUrl, QVariantMap *personMap);
    static QString personStringFromSimpleMap(QVariantMap personMap,
                                             int totalCount);

    static bool canDisplayObject(QString objectType);


signals:

public slots:


private:
    ASPerson *asAuthor;

    QString id;
    QString type;
    QString url;
    QString createdAt;
    QString updatedAt;
    QString locationName;
    QString locationFormatted;
    QString locationCountry;

    QString deleted;
    QString liked;


    QString title;
    QString summary;
    QString content;

    QString imageUrl;
    QString audioUrl;
    QString videoUrl;
    QString fileUrl;
    QString mimeType;
    QString attachmentPureUrl; // To have filename with extension when using a proxyURL

    int memberCount;
    QString memberUrl;

    QString likesCount;
    QString commentsCount;
    QString sharesCount;

    QVariantList lastLikesList;
    QVariantList lastCommentsList;
    QVariantList lastSharesList;

    QString likesUrl;
    QString commentsUrl;
    QString sharesUrl;
    bool proxiedUrls;

    QVariantMap originalObjectMap;
    QVariantMap inReplyToMap;
};

#endif // ASOBJECT_H
