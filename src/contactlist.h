/*
 *   This file is part of Dianara
 *   Copyright 2012-2015  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#ifndef CONTACTLIST_H
#define CONTACTLIST_H

#include <QWidget>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QScrollArea>
#include <QLineEdit>
#include <QPushButton>

#include <QDebug>

#include "pumpcontroller.h"
#include "globalobject.h"
#include "contactcard.h"


class ContactList : public QWidget
{
    Q_OBJECT

public:
    explicit ContactList(PumpController *pumpController,
                         GlobalObject *globalObject,
                         QString listType,
                         QWidget *parent = 0);
    ~ContactList();

    void clearListContents();
    void setListContents(QVariantList contactList);

    QString getContactsStringForExport();


signals:
    void contactCountChanged(int difference);


public slots:
    void filterList(QString filterText);

    void addSingleContact(ASPerson *contact);
    void removeSingleContact(ASPerson *contact);



private:
    PumpController *pController;
    GlobalObject *globalObj;

    QVBoxLayout *mainLayout;

    QAction *goToFilterAction;
    QHBoxLayout *filterLayout;
    QLabel *filterIcon;
    QLineEdit *filterLineEdit;
    QPushButton *removeFilterButton;

    QVBoxLayout *contactsLayout;
    QWidget *contactsWidget;
    QScrollArea *contactsScrollArea;

    QList<ContactCard *> contactsInList;

    QString contactsStringForExport;

    bool isFollowing;
};

#endif // CONTACTLIST_H
