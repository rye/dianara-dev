/*
 *   This file is part of Dianara
 *   Copyright 2012-2015  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#include "minorfeed.h"


MinorFeed::MinorFeed(PumpController::requestTypes minorFeedType,
                     PumpController *pumpController,
                     GlobalObject *globalObject,
                     FilterChecker *filterChecker,
                     QWidget *parent) : QFrame(parent)
{
    this->feedType = minorFeedType;
    this->pController = pumpController;
    this->globalObj = globalObject;
    this->fChecker = filterChecker;
    this->setFrameStyle(QFrame::StyledPanel | QFrame::Sunken);
    this->setContentsMargins(0, 0, 0, 0);

    // Layout for the items
    itemsLayout = new QVBoxLayout();
    itemsLayout->setContentsMargins(0, 0, 0, 0);
    itemsLayout->setSpacing(1); // Very small spacing
    itemsLayout->setAlignment(Qt::AlignTop);

    // Separator frame, to mark where new items end
    separatorFrame = new QFrame(this);
    separatorFrame->setFrameStyle(QFrame::HLine);
    separatorFrame->setMinimumHeight(28);
    separatorFrame->setContentsMargins(0, 8, 0, 8);
    separatorFrame->hide();

    // Button to get newer items (pending stuff)
    getPendingButton = new QPushButton(QIcon::fromTheme("view-refresh",
                                                        QIcon(":/images/menu-refresh.png")),
                                       "*get pending activities*",
                                       this);
    getPendingButton->setFlat(true);
    connect(getPendingButton, SIGNAL(clicked()),
            this, SLOT(updateFeed()));
    getPendingButton->hide();


    // Button to get more items (older stuff)
    getOlderButton = new QPushButton(QIcon::fromTheme("list-add",
                                                     QIcon(":/images/list-add.png")),
                                     tr("Older Activities"),
                                     this);
    getOlderButton->setFlat(true);
    getOlderButton->setToolTip(tr("Get previous minor activities"));
    connect(getOlderButton, SIGNAL(clicked()),
            this, SLOT(getMoreActivities()));

    // Set disabled initially; will be enabled when contents are set
    getOlderButton->setDisabled(true);


    // Main layout
    mainLayout = new QVBoxLayout();
    mainLayout->setContentsMargins(1, 1, 1, 1);
    mainLayout->setAlignment(Qt::AlignTop);
    mainLayout->addWidget(getPendingButton);
    mainLayout->addLayout(itemsLayout);
    mainLayout->addWidget(getOlderButton);
    this->setLayout(mainLayout);



    // Demo activity, stating that there's nothing to show
    QVariantMap demoGenerator;
    demoGenerator.insert("displayName", "Dianara");

    QVariantMap demoActivityMap;
    demoActivityMap.insert("published", QDateTime::currentDateTimeUtc()
                                        .toString(Qt::ISODate));
    demoActivityMap.insert("generator", demoGenerator);
    demoActivityMap.insert("content",   tr("There are no activities to show yet."));

    ASActivity *demoActivity = new ASActivity(demoActivityMap, this);

    MinorFeedItem *demoFeedItem = new MinorFeedItem(demoActivity,
                                                    false, // Not highlighted by filter
                                                    this->pController,
                                                    this->globalObj);
    demoFeedItem->setItemAsNew(false,
                               false); // Do not inform the feed
    this->itemsLayout->addWidget(demoFeedItem);
    this->itemsInFeed.append(demoFeedItem);

    this->firstLoad = true;
    this->gettingNew = true; // First time should be true
    this->unreadItemsCount = 0;
    this->highlightedItemsCount = 0;
    this->pendingToReceiveNextTime = 0;

    // Remember what was the newest activity last time
    QSettings settings;
    settings.beginGroup("MinorFeedStates");
    switch (this->feedType)
    {
    case PumpController::MinorFeedMainRequest:
        this->previousNewestActivityId = settings
                                         .value("previousNewestItemIdMain")
                                         .toString();
        this->fullFeedItemCount = settings.value("totalItemsMain").toInt();
        this->feedBatchItemCount = 50;
        break;

    case PumpController::MinorFeedDirectRequest:
        this->previousNewestActivityId = settings
                                         .value("previousNewestItemIdDirect")
                                         .toString();
        this->fullFeedItemCount = settings.value("totalItemsDirect").toInt();
        this->feedBatchItemCount = 20;
        break;

    case PumpController::MinorFeedActivityRequest:
        this->previousNewestActivityId = settings
                                         .value("previousNewestItemIdActivity")
                                         .toString();
        this->fullFeedItemCount = settings.value("totalItemsActivity").toInt();
        this->feedBatchItemCount = 20;
        break;

    default:
        qDebug() << "MinorFeed created with wrong type:" << this->feedType;
    }
    settings.endGroup();


    // Sync all avatar's follow state when there are changes in the Following list
    connect(pController, SIGNAL(followingListChanged()),
            this, SLOT(updateAvatarFollowStates()));

    qDebug() << "MinorFeed created";
}


MinorFeed::~MinorFeed()
{
    QSettings settings;
    settings.beginGroup("MinorFeedStates");

    switch (this->feedType)
    {
    case PumpController::MinorFeedMainRequest:
        settings.setValue("previousNewestItemIdMain",
                          this->previousNewestActivityId);
        settings.setValue("totalItemsMain",
                          this->fullFeedItemCount);
        break;

    case PumpController::MinorFeedDirectRequest:
        settings.setValue("previousNewestItemIdDirect",
                          this->previousNewestActivityId);
        settings.setValue("totalItemsDirect",
                          this->fullFeedItemCount);
        break;

    case PumpController::MinorFeedActivityRequest:
        settings.setValue("previousNewestItemIdActivity",
                          this->previousNewestActivityId);
        settings.setValue("totalItemsActivity",
                          this->fullFeedItemCount);
        break;

    default:
        qDebug() << "MinorFeed destructor: feed type was wrong";
    }
    settings.endGroup();

    qDebug() << "MinorFeed destroyed; Type:" << this->feedType;
}



void MinorFeed::clearContents()
{
    qDebug() << "MinorFeed::clearContents()";
    foreach (MinorFeedItem *feedItem, itemsInFeed)
    {
        this->itemsLayout->removeWidget(feedItem);
        delete feedItem;
    }

    itemsInFeed.clear();

    itemsLayout->removeWidget(separatorFrame);
    separatorFrame->hide();
}


/*
 * Remove oldest items, to avoid ever-increasing memory usage
 * Called after updating the feed, only when getting newer items
 *
 * At the very least, keep as many items as were received in last update
 *
 */
void MinorFeed::removeOldItems(int minimumToKeep)
{
    int maxItems = qMax(this->feedBatchItemCount * 2, // TMP FIXME
                        minimumToKeep);

    if (itemsInFeed.count() <= maxItems)
    {
        // Not too many items yet
        return;
    }

    int itemCounter = 0;
    int deletedCounter = 0; // tmp, TESTS (EXTRALOGGING)
    foreach (MinorFeedItem *feedItem, itemsInFeed)
    {
        if (itemCounter >= maxItems)
        {
            if (!feedItem->isNew()) // Don't remove if it's unread (optional?)
            {
                this->itemsLayout->removeWidget(feedItem);
                this->itemsInFeed.removeOne(feedItem);
                delete feedItem;
                ++deletedCounter; // tmp, TESTS (EXTRALOGGING)
            }
        }

        ++itemCounter;
    }

    // Update "next" link manually, based on the last item present in the feed
    QByteArray lastItemId = itemsInFeed.last()->getActivityId().toLocal8Bit();
    lastItemId = lastItemId.toPercentEncoding(); // Needs to be percent-encoded

    this->nextLink = this->pController->getFeedApiUrl(this->feedType)
                   + "?before=" + lastItemId;

#ifdef EXTRALOGGING
    // tmp, TESTS - debugging via log
    this->globalObj->logMessage(QString(">>> %1 ITEMS IN '%2' AFTER CLEANUP; "
                                        "%3 CAME, %4 REMOVED - Next: %5")
                                .arg(itemsInFeed.count())
                                .arg(PumpController::getFeedNameAndPath(this->feedType).first())
                                .arg(minimumToKeep)
                                .arg(deletedCounter)
                                .arg(this->nextLink));
#endif
}


void MinorFeed::insertSeparator(int position)
{
    this->itemsLayout->insertWidget(position,
                                    this->separatorFrame);
    this->separatorFrame->show();
}


void MinorFeed::markAllAsRead()
{
    foreach (MinorFeedItem *feedItem, itemsInFeed)
    {
        feedItem->setItemAsNew(false,   // Mark as not new
                               false);  // Don't inform the feed
    }

    this->unreadItemsCount = 0;
    this->highlightedItemsCount = 0;
}

/*
 * Update fuzzy timestamps in all items
 *
 */
void MinorFeed::updateFuzzyTimestamps()
{
    foreach (MinorFeedItem *feedItem, itemsInFeed)
    {
        feedItem->setFuzzyTimeStamp();
    }
}


void MinorFeed::syncActivityWithTimelines(ASActivity *activity)
{
    //////////////////////////////////////////////////////////// An edited post
    if (activity->getVerb() == "update")
    {
        if (ASObject::canDisplayObject(activity->object()->getType()))
        {
            emit objectUpdated(activity->object());
        }
    }    


    //////////////////////////////////////////// A new comment posted to a post
    if (activity->getVerb() == "post")
    {
        ASObject *activityObject = activity->object();

        // Check if the object has proper author info; if not, use activity's author
        if (activityObject->author()->getId().isEmpty())
        {
            activityObject->updateAuthorFromPerson(activity->author());
        }
        // FIXME: this needs some checking...
        // v1.3.0b1+11: seems OK

        emit objectReplyAdded(activityObject);
    }


    ////////////////////////////////////////////////////////////// A liked post
    if (activity->getVerb() == "favorite"
     || activity->getVerb() == "like")
    {
        // FIXME: handle liking of comments (1.3.2)

        if (ASObject::canDisplayObject(activity->object()->getType()))
        {
            emit objectLiked(activity->object()->getId(),
                             activity->object()->getType(),
                             activity->author()->getId(),
                             activity->author()->getNameWithFallback(),
                             activity->author()->getUrl());
        }
    }

    /////////////////////////////////////////////////////////// An unliked post
    if (activity->getVerb() == "unfavorite"
     || activity->getVerb() == "unlike")
    {
        // FIXME: handle unliking of comments (1.3.2)

        if (ASObject::canDisplayObject(activity->object()->getType()))
        {
            emit objectUnliked(activity->object()->getId(),
                               activity->object()->getType(),
                               activity->author()->getId());
        }
    }


    //////////////////////////////////////////////////////////// A deleted post
    if (activity->getVerb() == "delete")
    {
        if (ASObject::canDisplayObject(activity->object()->getType()))
        {
            emit objectDeleted(activity->object());
        }
    }

    // Sync "follow" and "stop-following"
}


/******************************************************************************/
/******************************************************************************/
/********************************** SLOTS *************************************/
/******************************************************************************/
/******************************************************************************/


/*
 * Get the latest activities
 *
 */
void MinorFeed::updateFeed()
{
    this->gettingNew = true;
    this->getOlderButton->setDisabled(true);

    this->pController->getFeed(this->feedType,
                               200, // Maximum item count allowed by API
                               this->prevLink);
}

/*
 * Get additional older activities
 *
 */
void MinorFeed::getMoreActivities()
{
    this->gettingNew = false;
    this->getOlderButton->setDisabled(true);

    this->pController->getFeed(this->feedType,
                               this->feedBatchItemCount,
                               this->nextLink);
}



void MinorFeed::setFeedContents(QVariantList activitiesList,
                                QString previous, QString next,
                                int totalItemCount)
{
    if (firstLoad)
    {
        this->prevLink = previous;
        this->nextLink = next;

        this->clearContents();
    }
    else
    {
        if (gettingNew)
        {
            if (!previous.isEmpty())
            {
                this->prevLink = previous;
            }
        }
        else
        {
            if (!next.isEmpty())
            {
                this->nextLink = next;
            }
        }
    }

    qDebug() << "Current MinorFeed links:" << this->prevLink << this->nextLink;


    int activitiesListSize = activitiesList.size();
    int totalItemDifference = -1; // TMP FIXME; for cases where it's unknown
    if (gettingNew)
    {
        // Check how many new items we should we expecting
        totalItemDifference = totalItemCount - this->fullFeedItemCount;
        this->fullFeedItemCount = totalItemCount;
        qDebug() << "MinorFeed::setFeedContents(); Should load"
                 << totalItemDifference << "items this time ###";

        // Check how many more items need to be received, if more than max are pending
        pendingToReceiveNextTime += totalItemDifference;
        pendingToReceiveNextTime -= activitiesListSize;
        if (pendingToReceiveNextTime > 0)
        {
            if (firstLoad)
            {
                // The difference in pending items is in the older activities, so doesn't count
                this->pendingToReceiveNextTime = 0;

                // TODO: On first load, could display the "pending" number at the "older" button
            }
            else
            {
                // Button at the top to fetch the pending activities, even more new stuff
                this->getPendingButton->setText(tr("Get %1 newer",
                                                   "As in: Get 3 newer (activities)")
                                                .arg(pendingToReceiveNextTime));
                this->getPendingButton->show();
            }
        }
        else
        {
            this->pendingToReceiveNextTime = 0; // In case it was less than 0
            this->getPendingButton->hide();
        }
    }

    // Remove the separator line
    itemsLayout->removeWidget(separatorFrame);
    separatorFrame->hide();

    int newItemsCount = 0;
    int newHighlightedItemsCount = 0;
    int newFilteredItemsCount = 0;

    bool itemIsNew;
    bool itemHighlightedByFilter;

    bool allNewItemsCounted = false;
    QString newestActivityId; // To store the activity ID for the newest item in the feed
                              // so we can know how many new items we receive next time

    int insertedItemsCount = 0;
    bool needToInsertSeparator = false;

    QList<ASActivity *> activitiesToSync;

    foreach (QVariant activityVariant, activitiesList)
    {
        itemIsNew = false;

        ASActivity *activity = new ASActivity(activityVariant.toMap(), this);

        int filtered = fChecker->validateActivity(activity);

        // If there is no reason to filter out the item, add it to the feed
        if (filtered != FilterChecker::FilterOut)
        {
            // Store activities in reverse order, for later processing (sync with TL)
            activitiesToSync.prepend(activity);

            // Determine which activities are new
            if (newestActivityId.isEmpty()) // Only first time, for newest item
            {
                if (gettingNew)
                {
                    newestActivityId = activity->getId();
                }
                else
                {
                    newestActivityId = this->previousNewestActivityId;
                    allNewItemsCounted = true;
                }
            }

            // Determine if this item is new, or if not anymore
            if (!allNewItemsCounted)
            {
                if (activity->getId() == this->previousNewestActivityId)
                {
                    allNewItemsCounted = true;
                    if (newItemsCount > 0)
                    {
                        needToInsertSeparator = true;
                    }
                }
                else
                {
                    // If activity is not ours, add it to the count
                    if (activity->author()->getId() != pController->currentUserId())
                    {
                        ++newItemsCount;
                        itemIsNew = true;
                    }
                }
            }


            if (filtered == FilterChecker::Highlight) // kinda TMP
            {
                itemHighlightedByFilter = true;
            }
            else
            {
                itemHighlightedByFilter = false;
            }

            MinorFeedItem *newFeedItem = new MinorFeedItem(activity,
                                                           itemHighlightedByFilter,
                                                           this->pController,
                                                           this->globalObj);
            newFeedItem->setItemAsNew(itemIsNew,
                                      false); // Don't inform the feed
            if (itemIsNew)
            {
                connect(newFeedItem, SIGNAL(itemRead(bool)),
                        this, SLOT(decreaseNewItemsCount(bool)));

                if (newFeedItem->getItemHighlightType() != -1)
                {
                    ++newHighlightedItemsCount;
                }
            }

            if (gettingNew)
            {
                if (needToInsertSeparator)  // -------
                {
                    this->insertSeparator(insertedItemsCount);
                    ++insertedItemsCount;
                    needToInsertSeparator = false;
                }

                this->itemsLayout->insertWidget(insertedItemsCount,
                                                newFeedItem);
                this->itemsInFeed.insert(insertedItemsCount,
                                         newFeedItem);
                ++insertedItemsCount;
            }
            else // Not new, getting 'older', so add at the bottom
            {
                this->itemsLayout->addWidget(newFeedItem);
                this->itemsInFeed.append(newFeedItem);
            }
        }
        else
        {
            ++newFilteredItemsCount;

            // Since the item is not added to the feed, we need to delete the activity
            delete activity; // FIXME: should not delete, just hide
        }
    } // End foreach


    // The first time stuff is received from the server, there's no need to sync
    if (!firstLoad)
    {
        // Activities in activitiesToSync are stored in reverse
        foreach (ASActivity *activity, activitiesToSync)
        {
            // Send notifications to update objects in the timelines
            this->syncActivityWithTimelines(activity);
        }
    }


    // If there were new items, and not already added, add separator: -------
    if (newItemsCount > 0 && this->separatorFrame->isHidden())
    {
        this->insertSeparator(insertedItemsCount);
    }


    if (!newestActivityId.isEmpty()) // If some items were received should be valid
    {
        this->previousNewestActivityId = newestActivityId;
    }

    this->unreadItemsCount += newItemsCount;
    this->highlightedItemsCount += newHighlightedItemsCount;

    qDebug() << "Minor feed updated";
    if (gettingNew) // not when getting more, older ones
    {
        emit newItemsCountChanged(this->unreadItemsCount,
                                  this->highlightedItemsCount);
        emit newItemsReceived(this->feedType,
                              newItemsCount,
                              newHighlightedItemsCount,
                              newFilteredItemsCount,
                              this->pendingToReceiveNextTime);
        qDebug() << "New items:" << newItemsCount
                 << "; New highlighted:" << newHighlightedItemsCount;


        // Clean up, keeping at least the ones that were just received
        if (activitiesListSize > 0) // but only if something was received
        {
            this->removeOldItems(activitiesListSize);
        }
    }
    else
    {
        emit newItemsReceived(this->feedType,
                              activitiesListSize, // Actual number of received items
                              -1, -1, -1); // -1 highlighted, filtered and pending
                                           // means these are old items
    }

    this->getOlderButton->setEnabled(true);
    firstLoad = false;
}


void MinorFeed::decreaseNewItemsCount(bool wasHighlighted)
{
    --unreadItemsCount;
    if (wasHighlighted)
    {
        --highlightedItemsCount;
    }

    emit newItemsCountChanged(unreadItemsCount, highlightedItemsCount);
}


void MinorFeed::updateAvatarFollowStates()
{
    foreach (MinorFeedItem *feedItem, itemsInFeed)
    {
        feedItem->syncAvatarFollowState();
    }
}
