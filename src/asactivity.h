/*
 *   This file is part of Dianara
 *   Copyright 2012-2015  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#ifndef ASACTIVITY_H
#define ASACTIVITY_H

#include <QObject>
#include <QVariantMap>
#include <QStringList>
#include <QTextCursor>

#include <QDebug>


#include "asobject.h"
#include "asperson.h"
#include "mischelpers.h"


class ASActivity : public QObject
{
    Q_OBJECT

public:
    explicit ASActivity(QVariantMap activityMap,
                        QObject *parent = 0);
    ~ASActivity();

    ASPerson *author();
    ASObject *object();
    ASObject *target();
    ASPerson *personObject();

    QString getId();
    QString getVerb();
    QString getGenerator();
    QString getCreatedAt();
    QString getUpdatedAt();
    QString getContent();

    QString getToString();
    QString getCCString();
    QStringList getRecipientsIdList();

    bool isShared();
    QString getSharedByName();
    QString getSharedById();
    QString getSharedByAvatar();


    QString generateTooltip();
    QString generateSnippet(int charLimit);


signals:


public slots:


private:
    ASPerson *asAuthor;
    ASObject *asObject;
    ASObject *asTarget;
    ASPerson *asPersonObject;

    QString id;
    QString verb;
    QString generator;
    QString createdAt;
    QString updatedAt;
    QString content;

    QString recipientsToString;
    QString recipientsCcString;

    QStringList recipientsIdList;

    bool shared;
    QString sharedByName;
    QString sharedById;
    QString sharedByAvatar;
};

#endif // ASACTIVITY_H
