/*
 *   This file is part of Dianara
 *   Copyright 2012-2015  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#include "contactmanager.h"

ContactManager::ContactManager(PumpController *pumpController,
                               GlobalObject *globalObject,
                               QWidget *parent) : QWidget(parent)
{
    this->pController = pumpController;
    this->globalObj = globalObject;

    // After receiving a contact list, update it
    connect(pController, SIGNAL(contactListReceived(QString,QVariantList,int)),
            this, SLOT(setContactListsContents(QString,QVariantList,int)));

    // After receiving the list of lists, update it
    connect(pController, SIGNAL(listsListReceived(QVariantList)),
            this, SLOT(setListsListContents(QVariantList)));



    mainLayout = new QVBoxLayout();
    mainLayout->setAlignment(Qt::AlignTop);


    QString webfingerHelpMessage = tr("username@server.org or "
                                      "https://server.org/username");

    topLayout = new QHBoxLayout();
    enterAddressLabel = new QLabel(tr("&Enter address to follow:"), this);
    enterAddressLabel->setToolTip("<b></b>"
                                  + webfingerHelpMessage); // HTML tags for wordwrap
    // Ensure it will get focus first, before addressLineEdit
    enterAddressLabel->setFocusPolicy(Qt::StrongFocus);

    addressLineEdit = new QLineEdit(this);
    addressLineEdit->setPlaceholderText(webfingerHelpMessage);
    addressLineEdit->setToolTip("<b></b>"
                                + webfingerHelpMessage);  // HTML tags to get wordwrap
    connect(addressLineEdit, SIGNAL(textChanged(QString)),
            this, SLOT(toggleFollowButton(QString)));
    connect(addressLineEdit, SIGNAL(returnPressed()),
            this, SLOT(followContact()));

    enterAddressLabel->setBuddy(addressLineEdit);


    followButton = new QPushButton(QIcon::fromTheme("list-add-user",
                                                    QIcon(":/images/list-add.png")),
                                   tr("&Follow"),
                                   this);
    followButton->setDisabled(true); // Disabled until an address is typed
    connect(followButton, SIGNAL(clicked()),
            this, SLOT(followContact()));
    topLayout->addWidget(enterAddressLabel);
    topLayout->addWidget(addressLineEdit);
    topLayout->addWidget(followButton);

    mainLayout->addLayout(topLayout);
    mainLayout->addSpacing(12);


    // Widgets for list of 'following' and 'followers'
    this->followingWidget = new ContactList(this->pController,
                                            this->globalObj,
                                            "following", this);
    connect(pController, SIGNAL(contactFollowed(ASPerson*)),
            followingWidget, SLOT(addSingleContact(ASPerson*)));
    connect(pController, SIGNAL(contactUnfollowed(ASPerson*)),
            followingWidget, SLOT(removeSingleContact(ASPerson*)));

    connect(followingWidget, SIGNAL(contactCountChanged(int)),
            this, SLOT(changeFollowingCount(int)));


    this->followersWidget = new ContactList(this->pController,
                                            this->globalObj,
                                            "followers", this);


    // Widget for the list of 'person lists'
    this->listsManager = new ListsManager(this->pController, this);
    listsScrollArea = new QScrollArea(this);
    listsScrollArea->setWidget(this->listsManager);
    listsScrollArea->setWidgetResizable(true);
    listsScrollArea->setFrameStyle(QFrame::NoFrame);


    // Widget for the list of site users; users from your own server
    this->siteUsersList = new SiteUsersList(this->pController,
                                            this->globalObj,
                                            this);

    // Options menu
    optionsMenu = new QMenu("*options-menu*", this);
    optionsMenu->addAction(QIcon::fromTheme("view-refresh",
                                            QIcon(":/images/menu-refresh.png")),
                           tr("Reload Followers"),
                           this,
                           SLOT(refreshFollowers()));
    optionsMenu->addAction(QIcon::fromTheme("view-refresh",
                                            QIcon(":/images/menu-refresh.png")),
                           tr("Reload Following"),
                           this,
                           SLOT(refreshFollowing()));
    optionsMenu->addSeparator();
    optionsMenu->addAction(QIcon::fromTheme("document-export",
                                            QIcon(":/images/button-download.png")),
                           tr("Export Followers"),
                           this,
                           SLOT(exportFollowers()));
    optionsMenu->addAction(QIcon::fromTheme("document-export",
                                            QIcon(":/images/button-download.png")),
                           tr("Export Following"),
                           this,
                           SLOT(exportFollowing()));
    optionsMenu->addSeparator();
    optionsMenu->addAction(QIcon::fromTheme("view-refresh",
                                            QIcon(":/images/menu-refresh.png")),
                           tr("Reload Lists"),
                           this,
                           SLOT(refreshPersonLists()));


    optionsButton = new QPushButton(QIcon::fromTheme("configure",
                                                     QIcon(":/images/button-configure.png")),
                                    "", this);
    optionsButton->setMenu(optionsMenu);


    this->tabWidget = new QTabWidget(this);
    tabWidget->addTab(followersWidget,
                      QIcon::fromTheme("meeting-observer",
                                       QIcon(":/images/no-avatar.png")),
                      "*followers*");
    tabWidget->addTab(followingWidget,
                      QIcon::fromTheme("meeting-participant",
                                       QIcon(":/images/no-avatar.png")),
                      "*following*");
    tabWidget->addTab(listsScrollArea,
                      QIcon::fromTheme("preferences-contact-list",
                                       QIcon(":/images/button-edit.png")),
                      "*lists*");
    tabWidget->addTab(siteUsersList,
                      QIcon::fromTheme("system-users",
                                       QIcon(":/images/no-avatar.png")),
                      tr("Site &Users"));
    tabWidget->setCornerWidget(this->optionsButton);



    this->followersCount = 0;
    this->followingCount = 0;
    this->listsCount = 0;
    this->setTabLabels();


    mainLayout->addWidget(tabWidget);
    this->setLayout(mainLayout);

    qDebug() << "Contact manager created";
}


ContactManager::~ContactManager()
{
    qDebug() << "Contact manager destroyed";
}




void ContactManager::setTabLabels()
{
    this->tabWidget->setTabText(0, tr("Follo&wers")
                                   + QString(" (%1)").arg(this->followersCount));

    this->tabWidget->setTabText(1, tr("Followin&g")
                                   + QString(" (%1)").arg(this->followingCount));

    this->tabWidget->setTabText(2, tr("&Lists")
                                + QString(" (%1)").arg(this->listsCount));
}




/*
 * Write the list of contacts (following or followers)
 * to a file selected by the user
 *
 */
void ContactManager::exportContactsToFile(QString listType)
{
    QString dialogTitle = listType == "following" ?
                    tr("Export list of 'following' to a file") :
                    tr("Export list of 'followers' to a file");

    QString suggestedFilename = "dianara-"
                                + pController->currentUsername()
                                + "-"
                                + listType;

    QString filename = QFileDialog::getSaveFileName(this, dialogTitle,
                                                    QDir::homePath() + "/"
                                                    + suggestedFilename,
                                                    "").trimmed();

    if (filename.isEmpty()) // If dialog canceled, do nothing
    {
        return;
    }


    qDebug() << "Exporting to:"  << filename;

    QFile exportFile(filename);
    exportFile.open(QIODevice::WriteOnly);

    if (listType == "following")
    {
        exportFile.write(this->followingWidget->getContactsStringForExport().toLocal8Bit());
    }
    else // "followers"
    {
        exportFile.write(this->followersWidget->getContactsStringForExport().toLocal8Bit());
    }
    exportFile.close();
}




/*****************************************************************************/
/*********************************** SLOTS ***********************************/
/*****************************************************************************/



void ContactManager::setContactListsContents(QString listType,
                                             QVariantList contactList,
                                             int totalReceivedCount)
{
    qDebug() << "ContactManager; Setting contact list contents";

    if (listType == "following")
    {
        if (totalReceivedCount <= 200) // Only for the first batch
        {
            this->followingWidget->clearListContents();
            followingCount = 0;
        }
        followingWidget->setListContents(contactList);

        if (totalReceivedCount < pController->currentFollowingCount())
        {
            pController->getContactList(listType, totalReceivedCount);
        }

        this->followingCount += contactList.size();
    }
    else
    {
        if (totalReceivedCount <= 200)
        {
            this->followersWidget->clearListContents();
            followersCount = 0;
        }
        followersWidget->setListContents(contactList);

        if (totalReceivedCount < pController->currentFollowersCount())
        {
            pController->getContactList(listType, totalReceivedCount);
        }

        this->followersCount += contactList.size();
    }


    // Update tab labels with number of following or followers, which were updated before
    this->setTabLabels();
}



/*
 * Fill the list of lists
 *
 */
void ContactManager::setListsListContents(QVariantList listsList)
{
    this->listsCount = listsList.count();
    // Update tab labels with number of following or followers, which were updated before
    this->setTabLabels();


    this->listsManager->setListsList(listsList);
}


void ContactManager::changeFollowingCount(int difference)
{
    this->followingCount += difference; // FIXME: pumpController should be notified

    this->setTabLabels();

    // Set "Following" tab as active, only when the contact manager is not visible
    // (following someone from an AvatarButton menu)
    if (!this->isVisible())
    {
        this->tabWidget->setCurrentIndex(1);
    }
}



/*
 * Ask for the updated list of Following
 *
 */
void ContactManager::refreshFollowing()
{
    qDebug() << "Refreshing list of Following...";
    this->pController->getContactList("following");
}


/*
 * Ask for the updated list of followers
 *
 */
void ContactManager::refreshFollowers()
{
    qDebug() << "Refreshing list of Followers...";
    this->pController->getContactList("followers");
}


/*
 * Export list of "Following" to a text file
 *
 */
void ContactManager::exportFollowing()
{
    qDebug() << "Exporting Following...";
    exportContactsToFile("following");
}


/*
 * Export list of "Followers" to a text file
 *
 */
void ContactManager::exportFollowers()
{
    qDebug() << "Exporting Followers...";
    exportContactsToFile("followers");
}


void ContactManager::refreshPersonLists()
{
    qDebug() << "Refreshing list of person lists...";
    this->pController->getListsList();
}



/*
 * Enable or disable Follow button
 *
 */
void ContactManager::toggleFollowButton(QString currentAddress)
{
    if (currentAddress.isEmpty())
    {
        this->followButton->setDisabled(true);
    }
    else
    {
        this->followButton->setEnabled(true);
    }
}



/*
 * Add the address entered by the user to the /following list.
 *
 * This supports adding webfinger addresses in the form
 * user@hostname or https://host/username.
 *
 */
void ContactManager::followContact()
{
    QString address = this->addressLineEdit->text().trimmed();
    bool validAddress = false;

    qDebug() << "ContactManager::followContact(); Address entered:" << address;

    // First, if the address is in URL form, convert it
    if (address.startsWith("https://") || address.startsWith("http://"))
    {
        address.remove("https://");
        address.remove("http://");

        if (address.contains("/")) // Very basic sanity check
        {
            QStringList addressParts = address.split("/");
            address = addressParts.at(1) + "@" + addressParts.at(0);
            // .at(2) could also have stuff, so don't use .first() and .last()
        }
    }

    // Then, check that the address actually matches something@somewhere.tld
    if (address.contains(QRegExp(".+@.+\\..+")))
    {
        validAddress = true;
    }
    else
    {
        qDebug() << "Invalid webfinger address!";
        this->addressLineEdit->setFocus();
    }


    if (validAddress)
    {
        qDebug() << "About to follow this address:" << address;
        this->pController->followContact(address);

        this->addressLineEdit->clear();
        // Activate 'Following' tab
        this->tabWidget->setFocus();
        this->tabWidget->setCurrentIndex(1);
    }

}
