/*
 *   This file is part of Dianara
 *   Copyright 2012-2015  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#include "imageviewer.h"

ImageViewer::ImageViewer(QString fileURI,
                         QString title,
                         QString suggestedFN,
                         bool isAnimated,
                         QWidget *parent) : QWidget(parent)
{
    this->setMinimumSize(460, 320);

    this->suggestedFilename = suggestedFN;
    this->imageIsAnimated = isAnimated;

    fileURI.remove(0, 7); // remove "image:/" from filename URI
    this->originalFileURI = fileURI;

    originalPixmap = QPixmap(originalFileURI);
    QString resolution = QString("%1x%2")
                         .arg(originalPixmap.width())
                         .arg(originalPixmap.height());

    QString imageDetails = resolution + ", "
                           + MiscHelpers::fileSizeString(originalFileURI);


    if (title.isEmpty())
    {
        title = "--";
    }
    this->setWindowTitle(tr("Image") + ": " + title + " - Dianara");
    this->setWindowIcon(QIcon::fromTheme("folder-image",
                                         QIcon(":/images/attached-image.png")));

    imageLabel = new QLabel(this);
    imageLabel->setFrameStyle(QFrame::StyledPanel | QFrame::Raised);
    imageLabel->setAlignment(Qt::AlignCenter);


    saveImageButton = new QPushButton(QIcon::fromTheme("document-save-as",
                                                       QIcon(":/images/button-save.png")),
                                      tr("&Save As..."),
                                      this);
    connect(saveImageButton, SIGNAL(clicked()),
            this, SLOT(saveImage()));


    if (imageIsAnimated)
    {
        this->movie = new QMovie(this);
        movie->setFileName(this->originalFileURI);
        movie->start();

        imageLabel->setMovie(movie);

        restartButton = new QPushButton(QIcon::fromTheme("media-playback-start",
                                                         QIcon(":/images/menu-refresh.png")),
                                        tr("&Restart Animation"),
                                        this);
        connect(restartButton, SIGNAL(clicked()),
                this, SLOT(restartAnimation()));
    }


    infoLabel = new QLabel(imageDetails, this);
    infoLabel->setAlignment(Qt::AlignCenter);

    closeButton = new QPushButton(QIcon::fromTheme("window-close"),
                                  tr("&Close"),
                                  this);
    connect(closeButton, SIGNAL(clicked()),
            this, SLOT(close()));


    this->createContextMenu();


    // Layout
    buttonsLayout = new QHBoxLayout();
    buttonsLayout->addWidget(saveImageButton);
    if (imageIsAnimated)
    {
        buttonsLayout->addWidget(restartButton);
    }
    buttonsLayout->addWidget(infoLabel, 1);
    buttonsLayout->addWidget(closeButton);

    mainLayout = new QVBoxLayout();
    mainLayout->addWidget(imageLabel);
    mainLayout->addLayout(buttonsLayout);
    this->setLayout(mainLayout);


    // Set initial window size according to image size and desktop (screen) size
    QDesktopWidget desktopWidget;
    int windowWidth = qMin(originalPixmap.width() + 100, // Some margin for the window itself
                           desktopWidget.availableGeometry().width());

    int windowHeight = qMin(originalPixmap.height() + 100,
                            desktopWidget.availableGeometry().height());

    this->resize(windowWidth, windowHeight);

    qDebug() << "ImageViewer created";
}


ImageViewer::~ImageViewer()
{
    qDebug() << "ImageViewer destroyed";
}



void ImageViewer::createContextMenu()
{
    this->saveAction = new QAction(QIcon::fromTheme("document-save-as",
                                                    QIcon(":/images/button-save.png")),
                                   tr("Save Image..."),
                                   this);
    saveAction->setShortcut(QKeySequence("Ctrl+S"));
    connect(saveAction, SIGNAL(triggered()),
            this, SLOT(saveImage()));

    this->closeAction = new QAction(QIcon::fromTheme("window-close"),
                                    tr("Close Viewer"),
                                    this);
    closeAction->setShortcut(QKeySequence(Qt::Key_Escape));
    connect(closeAction, SIGNAL(triggered()),
            this, SLOT(close()));


    this->contextMenu = new QMenu("imageViewerMenu", this);
    contextMenu->addAction(saveAction);
    contextMenu->addAction(closeAction);

    // Make the shortcuts work
    this->addAction(saveAction);
    this->addAction(closeAction);
}




/****************************************************************************/
/************************************ SLOTS *********************************/
/****************************************************************************/


void ImageViewer::saveImage()
{
    bool savedCorrectly;

    QString filename;
    filename = QFileDialog::getSaveFileName(this, tr("Save Image As..."),
                                            QDir::homePath() + "/" + suggestedFilename,
                                            tr("Image files") + " (*.jpg *.png);;"
                                            + tr("All files") + " (*)");

    if (!filename.isEmpty())
    {
        // Save pixmap from original file
        savedCorrectly = QPixmap(this->originalFileURI).save(filename);

        // FIXME: change this to directly copy the unmodified original instead
        if (!savedCorrectly)
        {
            QMessageBox::warning(this,
                                 tr("Error saving image"),
                                 tr("There was a problem while saving %1.\n\n"
                                    "Filename should end in .jpg "
                                    "or .png extensions.").arg(filename));
        }
    }
}

void ImageViewer::restartAnimation()
{
    if (imageIsAnimated)
    {
        this->movie->stop();
        this->movie->start();
    }
}



/****************************************************************************/
/********************************** PROTECTED *******************************/
/****************************************************************************/


void ImageViewer::closeEvent(QCloseEvent *event)
{
    qDebug() << "ImageViewer::closeEvent(); hiding and destroying widget!";
    event->ignore();

    this->hide();
    this->deleteLater();
}

void ImageViewer::hideEvent(QHideEvent *event)
{
    qDebug() << "ImageViewer::hideEvent()";
    event->accept();
}


void ImageViewer::resizeEvent(QResizeEvent *event)
{
    qDebug() << "ImageViewer::resizeEvent()";

    int width = qMin(imageLabel->width() - 16,   // 16px as margin
                     originalPixmap.width());
    int height = qMin(imageLabel->height() - 16,
                      originalPixmap.height());
    // FIXME: maybe for animated images (usually small GIFs)
    // the image could be allowed to grow to > 1x

    if (!this->imageIsAnimated)
    {
        imageLabel->setPixmap(originalPixmap.scaled(width, height,
                                                    Qt::KeepAspectRatio,
                                                    Qt::SmoothTransformation));
    }
    else
    {
        QSize movieSize(originalPixmap.width(),
                        originalPixmap.height());
        movieSize.scale(width, height, Qt::KeepAspectRatio);
        movie->setScaledSize(movieSize);
    }

    event->accept();
}



void ImageViewer::contextMenuEvent(QContextMenuEvent *event)
{
    this->contextMenu->exec(event->globalPos());
}
