/*
 *   This file is part of Dianara
 *   Copyright 2012-2015  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#include "proxydialog.h"


ProxyDialog::ProxyDialog(int proxyType,
                         QString hostname, QString port,
                         bool useAuth,
                         QString user, QString password,
                         QWidget *parent) : QWidget(parent)
{
    this->setWindowTitle(tr("Proxy Configuration") + " - Dianara");
    this->setWindowIcon(QIcon::fromTheme("preferences-system-network",
                                         QIcon(":/images/button-configure.png")));
    this->setWindowFlags(Qt::Dialog);
    this->setWindowModality(Qt::WindowModal);
    this->setMinimumSize(380, 320);

    proxyTypeComboBox = new QComboBox();
    proxyTypeComboBox->addItem(tr("Do not use a proxy"));
    proxyTypeComboBox->addItem("SOCKS 5");
    proxyTypeComboBox->addItem("HTTP");
    proxyTypeComboBox->setCurrentIndex(proxyType);

    hostnameLineEdit = new QLineEdit(hostname);
    hostnameLineEdit->setPlaceholderText("example.org");

    portLineEdit = new QLineEdit(port);
    portLineEdit->setPlaceholderText("1080, 8080..."); // defaults for socks5 and http

    authCheckBox = new QCheckBox();
    authCheckBox->setChecked(useAuth);
    connect(authCheckBox, SIGNAL(toggled(bool)),
            this, SLOT(toggleAuth(bool)));

    userLineEdit = new QLineEdit(user);
    userLineEdit->setPlaceholderText(tr("Your proxy username"));

    passwordLineEdit = new QLineEdit(password);
    passwordLineEdit->setEchoMode(QLineEdit::Password);

    passwordNoteLabel = new QLabel(tr("Note: Password is not stored in a "
                                      "secure manner. If you wish, you can "
                                      "leave the field empty, and you'll be "
                                      "prompted for the password on startup."));
    passwordNoteLabel->setWordWrap(true);
    QFont noteFont;
    noteFont.setPointSize(noteFont.pointSize() - 1);
    passwordNoteLabel->setFont(noteFont);

    this->toggleAuth(useAuth); // Enable or disable initially

    // Bottom
    saveButton = new QPushButton(QIcon::fromTheme("document-save",
                                                  QIcon(":/images/button-save.png")),
                                 tr("&Save"));
    connect(saveButton, SIGNAL(clicked()),
            this, SLOT(saveSettings()));

    cancelButton = new QPushButton(QIcon::fromTheme("dialog-cancel",
                                                    QIcon(":/images/button-cancel.png")),
                                   tr("&Cancel"));
    connect(cancelButton, SIGNAL(clicked()),
            this, SLOT(hide()));


    // ESC to close
    closeAction = new QAction(this);
    closeAction->setShortcut(QKeySequence(Qt::Key_Escape));
    connect(closeAction, SIGNAL(triggered()),
            this, SLOT(hide()));
    this->addAction(closeAction);


    //////////////////////////////////////////// Layout
    fieldsLayout = new QFormLayout();
    fieldsLayout->addRow(tr("Proxy &Type"),
                       proxyTypeComboBox);
    fieldsLayout->addRow(tr("&Hostname"),
                       hostnameLineEdit);
    fieldsLayout->addRow(tr("&Port"),
                       portLineEdit);
    fieldsLayout->addRow(tr("Use &Authentication"),
                         authCheckBox);
    fieldsLayout->addRow(tr("&User"),
                       userLineEdit);
    fieldsLayout->addRow(tr("Pass&word"),
                       passwordLineEdit);
    fieldsLayout->addRow("", passwordNoteLabel);

    buttonsLayout = new QHBoxLayout();
    buttonsLayout->setAlignment(Qt::AlignRight);
    buttonsLayout->addWidget(saveButton);
    buttonsLayout->addWidget(cancelButton);

    mainLayout = new QVBoxLayout();
    mainLayout->addLayout(fieldsLayout);
    mainLayout->addSpacing(16);
    mainLayout->addStretch(1);
    mainLayout->addLayout(buttonsLayout);

    this->setLayout(mainLayout);


    qDebug() << "ProxyDialog created";
}


ProxyDialog::~ProxyDialog()
{
    qDebug() << "ProxyDialog destroyed";
}


//////////////////////////////////////////////////////////////////////////////
///////////////////////////////// SLOTS //////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////


void ProxyDialog::toggleAuth(bool state)
{
    this->userLineEdit->setEnabled(state);
    this->passwordLineEdit->setEnabled(state);
}


void ProxyDialog::saveSettings()
{
    QSettings settings;
    settings.beginGroup("Configuration");
    settings.setValue("proxyType",     this->proxyTypeComboBox->currentIndex());
    settings.setValue("proxyHostname", this->hostnameLineEdit->text());
    settings.setValue("proxyPort",     this->portLineEdit->text());

    settings.setValue("proxyUseAuth",  this->authCheckBox->isChecked());
    if (!authCheckBox->isChecked()) // If no auth, clear saved username/passwd
    {
        userLineEdit->clear();
        passwordLineEdit->clear();
    }

    settings.setValue("proxyUser",     this->userLineEdit->text());
    // VERY TMP: Saving passwd as base64 for now; FIXME
    settings.setValue("proxyPassword", this->passwordLineEdit->text().toUtf8().toBase64());
    settings.endGroup();

    this->hide();

    qDebug() << "ProxyDialog::saveSettings()"
             << hostnameLineEdit->text() << portLineEdit->text();
}
