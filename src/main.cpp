/*
 *   This file is part of Dianara
 *   Copyright 2012-2015  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#include <QApplication>
#include <QTranslator>
#include <iostream>

#include "mainwindow.h"





#if QT_VERSION < QT_VERSION_CHECK(5, 0, 0)
void customMessageHandlerQt4(QtMsgType type, const char *msg)
{
    // do nothing
    Q_UNUSED(type)
    Q_UNUSED(msg)  // FIXME, memory leak?

    return;
}
#else
void customMessageHandlerQt5(QtMsgType type,
                             const QMessageLogContext &context,
                             const QString &msg)
{
    Q_UNUSED(type)
    Q_UNUSED(context)
    Q_UNUSED(msg)

    // Do nothing

    return;
}
#endif



int main(int argc, char *argv[])
{
    QApplication dianaraApp(argc, argv);
    dianaraApp.setApplicationName("Dianara");
    dianaraApp.setApplicationVersion("1.3.2-dev+5");
    dianaraApp.setOrganizationName("JanCoding");
    dianaraApp.setOrganizationDomain("jancoding.wordpress.com");

    std::cout << QString("Dianara v%1 - JanKusanagi 2012-2015\n"
                         "http://jancoding.wordpress.com/dianara\n\n")
                        .arg(dianaraApp.applicationVersion()).toStdString();
    std::cout << QString("- Built with Qt v%1 on %2, %3\n")
                 .arg(QT_VERSION_STR)
                 .arg(__DATE__)
                 .arg(__TIME__).toStdString();

    std::cout << QString("- Running with Qt v%1\n\n").arg(qVersion()).toStdString();
    std::cout.flush();

    // To make the mswin version of Dianara distributable as a standalone package
#ifdef Q_OS_WIN
    dianaraApp.addLibraryPath("./plugins/");
#endif


    QStringList cmdLine = qApp->arguments();
    cmdLine.removeFirst(); // Remove the program executable name/path

    bool debugMode = false;
    bool nextParameterIsConfig = false;
    bool ignoreSslErrors = false;
    bool nohttps = false;

    // Parse command line parameters
    if (!cmdLine.isEmpty())
    {
        foreach (QString argument, cmdLine)
        {
            // Help
            if (argument.startsWith("--help", Qt::CaseInsensitive)
             || argument.startsWith("-h", Qt::CaseInsensitive))
            {
                std::cout << "\nHelp:\n";
                std::cout << "    " << argv[0] << " [options]\n\n";
                std::cout << "Options:\n";
                std::cout << "    -c  --config [name]       Use a different "
                             "configuration file\n";
                std::cout << "    -d  --debug               Show debug messages "
                             "in the terminal\n";
                std::cout << "        --linkcolor=[color]   Set color for "
                             "links, such as 'green' or '#00B500'\n"
                             "\n";
                std::cout << "        --nohttps             Use this if your "
                             "server does not support HTTPS\n";
                std::cout << "        --ignoresslerrors     Ignore SSL errors "
                             "(dangerous, use with care!)\n"
                             "\n";
                std::cout << "    -v  --version             Show version "
                             "information and exit\n";
                std::cout << "    -h  --help                Show this help and "
                             "exit\n";
                std::cout << "\n";

                return 0; // Exit to shell
            }

            if (argument.startsWith("--version", Qt::CaseInsensitive)
             || argument.startsWith("-v", Qt::CaseInsensitive))
            {
                // Version information already shown
                return 0;
            }


            // Use different config file, by setting a different applicationName
            if (argument.startsWith("--config", Qt::CaseInsensitive)
             || argument.startsWith("-c", Qt::CaseInsensitive))
            {
                nextParameterIsConfig = true;
                cmdLine.removeAll(argument);
            }
            else if (nextParameterIsConfig)
            {
                QString configName = "Dianara_" + argument.toLower();
                dianaraApp.setApplicationName(configName);
                nextParameterIsConfig = false;

                std::cout << "Using alternate config file: "
                          << configName.toStdString() << "\n";
            }

            // Debug mode
            if (argument.startsWith("--debug", Qt::CaseInsensitive)
             || argument.startsWith("-d", Qt::CaseInsensitive))
            {
                debugMode = true;
                cmdLine.removeAll(argument);

                std::cout << "Debug messages enabled\n";
            }


            // Option to set link color (useful in GTK environments)
            if (argument.startsWith("--linkcolor=", Qt::CaseInsensitive))
            {
                QString linkColorName = argument.split("=").last();
                QColor linkColor = QColor(linkColorName);
                if (linkColor.isValid())
                {
                    QPalette newPalette = dianaraApp.palette();
                    newPalette.setColor(QPalette::Link, linkColor);
                    dianaraApp.setPalette(newPalette);

                    std::cout << "Forcing link color to: "
                              << linkColorName.toStdString() << "\n";
                }
                else
                {
                    std::cout << "Invalid link color specified!\n";
                }
            }


            // Option to use non-https servers
            if (argument.startsWith("--nohttps", Qt::CaseInsensitive))
            {
                nohttps = true;
                cmdLine.removeAll(argument);

                std::cout << "*** Using No-HTTPS mode\n";
            }


            // Option to ignore SSL errors
            if (argument.startsWith("--ignoresslerrors", Qt::CaseInsensitive))
            {
                ignoreSslErrors = true;
                cmdLine.removeAll(argument);

                std::cout << "*************************************\n"
                             "*** WARNING: Ignoring SSL errors. ***\n"
                             "***          This is insecure!    ***\n"
                             "*************************************\n";
            }

        } // End foreach
    }


    // Register custom message handler, to hide debug messages unless specified
    if (!debugMode)
    {
    #ifdef Q_OS_WIN
        FreeConsole();
    #endif

        std::cout << "To see debug messages while running, use --debug\n";

    #if QT_VERSION < QT_VERSION_CHECK(5, 0, 0)
        qInstallMsgHandler(customMessageHandlerQt4);
    #else
        qInstallMessageHandler(customMessageHandlerQt5);
    #endif
    }


    // Load translation files
    QTranslator translator;
    // Get language from LANG environment variable or system's locale
    QString languageString = qgetenv("LANG");
    if (languageString.isEmpty())
    {
        languageString = QLocale::system().name();
    }
    QString languageFile;
    languageFile = QString(":/translations/dianara_%1")
                   .arg(languageString);
    std::cout << "Using translation file: "
              << languageFile.toStdString() << "\n";

    translator.load(languageFile);
    dianaraApp.installTranslator(&translator);

    std::cout.flush();


    MainWindow dianaraWindow;

    if (ignoreSslErrors)
    {
        dianaraWindow.enableIgnoringSslErrors();
    }

    if (nohttps)
    {
        dianaraWindow.enableNoHttpsMode();
    }

    dianaraWindow.toggleMainWindow(true);  // show(), firstTime=true


    return dianaraApp.exec();
}
