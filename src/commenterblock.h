/*
 *   This file is part of Dianara
 *   Copyright 2012-2015  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#ifndef COMMENTER_H
#define COMMENTER_H

#include <QWidget>
#include <QVBoxLayout>
#include <QGridLayout>
#include <QScrollArea>
#include <QPushButton>
#include <QLabel>
#include <QScrollBar>
#include <QTimer>

#include "pumpcontroller.h"
#include "globalobject.h"
#include "composer.h"
#include "asobject.h"
#include "comment.h"


class CommenterBlock : public QWidget
{
    Q_OBJECT

public:
    explicit CommenterBlock(PumpController *pumpController,
                            GlobalObject *globalObject,
                            QString parentAuthorId,
                            bool parentStandalone,
                            QWidget *parent = 0);
    ~CommenterBlock();

    void clearComments();
    void setComments(QVariantList commentsList, int commentCount);
    void appendComment(ASObject *object, bool justOne=false);
    void updateCommentFromObject(ASObject *object);
    void setCommentDeletedFromObject(ASObject *object);

    void updateFuzzyTimestamps();
    void updateAvatarFollowStates();
    void updateShowAllLink();

    void adjustCommentsWidth();
    void adjustCommentsHeight();
    void adjustCommentArea();
    void redrawComments();

    bool isFullMode();

    int getCommentCount();

    Composer *getComposer();


signals:
    void commentSent(QString commentText);
    void commentUpdated(QString commentId, QString commentText);
    void allCommentsRequested();


public slots:
    void setMinimumMode();
    void setFullMode(QString initialText="");

    void quoteComment(QString content);

    void editComment(QString id, QString content);

    void requestAllComments();
    void onPostingCommentOk();
    void onPostingCommentFailed();

    void sendComment();

    void scrollCommentsToBottom();


protected:
    virtual void resizeEvent(QResizeEvent *event);

private:
    QVBoxLayout *mainLayout;
    QGridLayout *bottomLayout;

    QScrollArea *commentsScrollArea;
    QWidget *commentsWidget;
    QVBoxLayout *commentsLayout;
    QTimer *scrollToBottomTimer;


    QString reloadCommentsString;
    QLabel *showAllCommentsLinkLabel;
    QTimer *getAllCommentsTimer; // To get all comments after a delay

    Composer *commentComposer;
    QPushButton *toolsButton;
    QLabel *statusInfoLabel;
    QPushButton *commentButton;
    QPushButton *cancelButton;

    bool editingMode;
    QString editingCommentId;

    bool fullMode;

    QList<Comment *> commentsInBlock;
    int currentCommentCount;
    bool allCommentsShown;

    QString parentPostAuthorId;
    bool parentPostStandalone;

    PumpController *pController;
    GlobalObject *globalObj;
};

#endif // COMMENTER_H
