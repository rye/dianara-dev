/*
 *   This file is part of Dianara
 *   Copyright 2012-2015  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#include "colorpicker.h"

ColorPicker::ColorPicker(QString description,
                         QString initialColorString,
                         QWidget *parent) : QWidget(parent)
{
    descriptionLabel = new QLabel(description);
    descriptionLabel->setWordWrap(true);

    checkBox = new QCheckBox();

    buttonPixmap = QPixmap(32, 32);

    button = new QPushButton(tr("Change"));
    button->setIconSize(QSize(32, 32));
    button->setDisabled(true); // Disabled initially
    connect(button, SIGNAL(clicked()),
            this, SLOT(changeColor()));

    connect(checkBox, SIGNAL(toggled(bool)),
            button, SLOT(setEnabled(bool)));

    layout = new QHBoxLayout();
    layout->addWidget(descriptionLabel, 10);
    layout->addSpacing(4);
    layout->addStretch(1);
    layout->addWidget(checkBox,         0);
    layout->addSpacing(8);
    layout->addWidget(button,           0);
    this->setLayout(layout);


    QColor initialColor(initialColorString);
    if (initialColor.isValid())
    {
        this->currentColor = initialColor;
        this->checkBox->setChecked(true);
    }
    else
    {
        if (initialColorString.startsWith("DISABLED"))
        {
            this->currentColor = initialColorString.remove("DISABLED");
        }
        else
        {
            this->currentColor = QColor(Qt::gray);
        }
    }
    this->setButtonColor(currentColor);


    qDebug() << "ColorPicker created";
}


ColorPicker::~ColorPicker()
{
    qDebug() << "ColorPicker destroyed";
}


void ColorPicker::setButtonColor(QColor color)
{
    buttonPixmap.fill(color);
    button->setIcon(QIcon(buttonPixmap));
}


QString ColorPicker::getCurrentColor()
{
    if (checkBox->isChecked())
    {
        return this->currentColor.name(); // return in #RRGGBB format
    }
    else
    {
        return QString("DISABLED%1").arg(currentColor.name()); // Return invalid color
    }
}


////////////////////////////////////////////////////////////////////////////
////////////////////////////////// SLOTS ///////////////////////////////////
////////////////////////////////////////////////////////////////////////////


void ColorPicker::changeColor()
{
    QColor newColor = QColorDialog::getColor(currentColor, this);

    if (newColor.isValid())
    {
        currentColor = newColor;

        setButtonColor(currentColor);
    }

    qDebug() << "New color:" << currentColor;
}
