/*
 *   This file is part of Dianara
 *   Copyright 2012-2015  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#include "asactivity.h"

ASActivity::ASActivity(QVariantMap activityMap,
                       QObject *parent) : QObject(parent)
{
    id = activityMap.value("id").toString();


    // Get the author of the _activity_ (not the object)
    this->asAuthor = new ASPerson(activityMap.value("actor").toMap(), this);

    // Get the object included with the activity: the post itself, or whatever
    this->asObject = new ASObject(activityMap.value("object").toMap(), this);

    // If the object is a person, as in an activity where someone adds someone else
    if (asObject->getType() == "person")
    {
        this->asPersonObject = new ASPerson(activityMap.value("object").toMap(), this);
    }
    else
    {
        // Otherwise, at least initialize empty, but valid
        this->asPersonObject = new ASPerson(QVariantMap(), this);
    }

    // Get possible "target"
    this->asTarget = new ASObject(activityMap.value("target").toMap(), this);

    // FIXME: add bools hasAuthor, hasObject and hasTarget, maybe?


    // Activity verb: post, share...
    verb = activityMap.value("verb").toString();

    if (verb == "share")
    {
        shared = true;
        sharedByName = asAuthor->getNameWithFallback();
        sharedById = asAuthor->getId();
        sharedByAvatar = asAuthor->getAvatar();
    }
    else
    {
        shared = false;
    }


    // Timestamps
    createdAt = activityMap.value("published").toString();
    updatedAt = activityMap.value("updated").toString();

    // Software used to generate the activity: webUI, a client, a service...
    generator = activityMap.value("generator").toMap().value("displayName").toString();

    // Content of the activity, usually a description like "User followed someone"
    content = activityMap.value("content").toString();


    // Audience: To
    QVariantMap aToMap;
    foreach (QVariant toVariant, activityMap.value("to").toList())
    {
        aToMap = toVariant.toMap();

        QString toId = aToMap.value("id").toString();
        if (toId == "http://activityschema.org/collection/public")
        {
            recipientsToString += "<b>" + tr("Public") + "</b>, ";
        }
        else
        {
            QString displayName = aToMap.value("displayName").toString().trimmed();
            // If name's empty, and ID is from a person
            if (displayName.isEmpty() && toId.startsWith("acct:"))
            {
                displayName = "&lt;" + ASPerson::cleanupId(toId) + "&gt;";
            }
#ifdef SHOWWRONGTOCC
            if (displayName.isEmpty()) // if STILL empty, some weirdness is there
            {
                displayName = "-?-";
            }
#endif
            if (!displayName.isEmpty())
            {
                recipientsToString += "<a href=\""
                                      + aToMap.value("url").toString() + "\">"
                                      + displayName + "</a>, ";
            }

            recipientsIdList.append(toId);
        }
    }
    recipientsToString.remove(-2, 2); // remove last comma and space


    // and Cc
    QVariantMap aCcMap;
    foreach (QVariant ccVariant, activityMap.value("cc").toList())
    {
        aCcMap = ccVariant.toMap();

        QString ccId = aCcMap.value("id").toString();
        if (ccId == "http://activityschema.org/collection/public")
        {
            recipientsCcString += "<b>" + tr("Public") + "</b>, ";
        }
        else
        {
            QString displayName = aCcMap.value("displayName").toString().trimmed();
            // If name's empty, and ID is from a person
            if (displayName.isEmpty() && ccId.startsWith("acct:"))
            {
                displayName = "&lt;" + ASPerson::cleanupId(ccId) + "&gt;";
            }
#ifdef SHOWWRONGTOCC
            if (displayName.isEmpty()) // if STILL empty, for some reason...
            {
                displayName = "-?-";
            }
#endif
            if (!displayName.isEmpty())
            {
                recipientsCcString += "<a href=\""
                                      + aCcMap.value("url").toString() + "\">"
                                      + displayName + "</a>, ";
            }

            recipientsIdList.append(ccId);
        }
    }
    recipientsCcString.remove(-2, 2);


    qDebug() << "ASActivity created" << this->id;
}



ASActivity::~ASActivity()
{
    qDebug() << "ASActivity destroyed" << this->id;
}



////// Getters!

ASPerson *ASActivity::author()
{
    return this->asAuthor;
}

ASObject *ASActivity::object()
{
    return this->asObject;
}

ASObject *ASActivity::target()
{
    return this->asTarget;
}

ASPerson *ASActivity::personObject()
{
    return this->asPersonObject;
}


QString ASActivity::getId()
{
    return this->id;
}

QString ASActivity::getVerb()
{
    return this->verb;
}

QString ASActivity::getGenerator()
{
    return this->generator;
}

QString ASActivity::getCreatedAt()
{
    return this->createdAt;
}

QString ASActivity::getUpdatedAt()
{
    return this->updatedAt;
}

QString ASActivity::getContent()
{
    return this->content;
}

QString ASActivity::getToString()
{
    return this->recipientsToString;
}

QString ASActivity::getCCString()
{
    return this->recipientsCcString;
}

QStringList ASActivity::getRecipientsIdList()
{
    return this->recipientsIdList;
}


bool ASActivity::isShared()
{
    return this->shared;
}

QString ASActivity::getSharedByName()
{
    return this->sharedByName;
}

QString ASActivity::getSharedById()
{
    return this->sharedById;
}

QString ASActivity::getSharedByAvatar()
{
    return this->sharedByAvatar;
}


/*
 * Create a string to be used as a tooltip
 *
 */
QString ASActivity::generateTooltip()
{
    QString activityTooltip;


    // Content
    QString activityObjectContent = this->asObject->getContent();

    if (this->asObject->getDeletedTime().isEmpty())
    {
        QString objectType = this->asObject->getType();
        if (ASObject::canDisplayObject(objectType)) // False also for empty type
        {
            if (!activityObjectContent.startsWith("<p"))
            {
                // Doesn't start with <p>, so add extra newline
                activityObjectContent.prepend("<br>");
            }

            activityObjectContent.prepend("[ "
                                          + ASObject::getTranslatedType(objectType)
                                          + " ]<br>");
        }
    }
    else
    {
        activityObjectContent.prepend("["
                                      + this->asObject->getDeletedOnString()
                                      + "]");
    }


    if (!activityObjectContent.isEmpty())
    {
        // Object's author name and ID
        QString activityObjectAuthorName = this->asObject->author()->getNameWithFallback();
        QString activityObjectAuthorId = this->asObject->author()->getId();

        // If there's a name and/or an ID, show them
        if (!activityObjectAuthorName.isEmpty())
        {
            activityTooltip = "<b>" + activityObjectAuthorName + "</b><br>"
                              "<i>" + activityObjectAuthorId + "</i>"
                              "<hr><br>"; // horizontal rule as separator
        }

        // Title, only if there's content, too; redundant otherwise
        QString activityObjectTitle = this->asObject->getTitle();
        if (!activityObjectTitle.isEmpty())
        {
            activityTooltip.append("<b><u>"
                                   + activityObjectTitle
                                   + "</u></b>"
                                     "<br><br>");
        }

        activityTooltip.append(activityObjectContent + "<br>");

        // Object ID, might be interesting to show if it's a person
        if (this->asObject->getType() == "person")
        {
            activityTooltip.append("<br><i>"
                                   + this->asObject->getId()
                                   + "</i><br><br>");
        }
    }


    // Target info
    QString activityTargetName = this->asTarget->getTitle();
    QString activityTargetUrl = this->asTarget->getUrl();
    if (activityTargetUrl.isEmpty())
    {
        activityTargetUrl = this->asTarget->getId();
    }
    if (!activityTargetUrl.isEmpty())
    {
        activityTooltip.append("<hr>&gt;&gt; "); // Horizontal rule, and >>

        if (!activityTargetName.isEmpty())
        {
            activityTooltip.append(QString("<b>%1</b><br><i>%2</i>")
                                   .arg(activityTargetName)
                                   .arg(activityTargetUrl));
        }
        else
        {
            activityTooltip.append("<i>" + activityTargetUrl + "</i>");
        }

        // Show type of target object
        activityTooltip.append("<br>"
                               + ASObject::getTranslatedType(asTarget->getType()));
    }


    // Remove last <br>, if needed
    if (activityTooltip.endsWith("<br>"))
    {
        activityTooltip.remove(-4, 4);
        // Check again, for double <br>'s
        if (activityTooltip.endsWith("<br>"))
        {
            activityTooltip.remove(-4, 4);
        }
    }


    activityTooltip = activityTooltip.trimmed();

    // If there's something, ensure it gets treated as HTML
    if (!activityTooltip.isEmpty())
    {
        activityTooltip.prepend("<b></b>");
    }

    return activityTooltip;
}



/*
 * Create a snippet of the activity and its object
 *
 */
QString ASActivity::generateSnippet(int charLimit)
{
    QString snippet;


    QString authorName = this->asObject->author()->getNameWithFallback();
    // If author name IS empty, probably means the author of the activity is the
    // author of the object (like posting a comment), so this wouldn't be useful
    if (this->asObject->author()->getId() != this->author()->getId()
        && !authorName.isEmpty())
    {
        snippet.append("<p><i>"    // Inside a <p> to avoid a weird newline bug
                       + tr("%1 by %2",
                            "1=kind of object: note, comment, etc; "
                            "2=author's name")
                         .arg(ASObject::getTranslatedType(this->asObject->getType()))
                         .arg("<b>" + authorName + "</b>")
                       + ":</i></p>");
    }


    QString objectContent = this->asObject->getContent();
    if (!objectContent.isEmpty())
    {
        /*   //// Disabled: Showing the title is always redundant
             //// considering how the Pump.io core generates the
             //// activities descriptions.
        QString objectTitle = this->asObject->getTitle();
        if (!objectTitle.isEmpty())
        {
            snippet.append("<b><u>" + objectTitle + "</u></b><br>");
        }
        */

        QTextDocument textDocument;
        textDocument.setHtml(objectContent);

        //  Limit characters in snippet
        if (textDocument.characterCount() > charLimit)
        {
            QTextCursor cursor = textDocument.find(QRegExp(".*"), charLimit);
            cursor.movePosition(QTextCursor::End, QTextCursor::KeepAnchor);
            cursor.removeSelectedText();
            cursor.insertHtml(" &nbsp;<b>[...]</b>");
        }

        snippet.append(MiscHelpers::cleanupHtml(textDocument.toHtml()));
    }

    return snippet.trimmed();
}
