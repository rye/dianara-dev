/*
 *   This file is part of Dianara
 *   Copyright 2012-2015  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#ifndef PUBLISHER_H
#define PUBLISHER_H

#include <QWidget>
#include <QGridLayout>
#include <QHBoxLayout>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QMenu>
#include <QFileDialog>
#include <QMessageBox>
#include <QProgressBar>

#include <QDebug>

#include "composer.h"
#include "pumpcontroller.h"
#include "globalobject.h"
#include "mischelpers.h"
#include "audienceselector.h"


class Publisher : public QWidget
{
    Q_OBJECT

public:
    explicit Publisher(PumpController *pumpController,
                       GlobalObject *globalObject,
                       QWidget *parent = 0);
    ~Publisher();

    void syncFromConfig();
    void setEmptyMediaData();
    void setTitleAndContent(QString title, QString content);
    void updatePublicFollowersLabels();

    QMap<QString,QString> getAudienceMap();

    void setMediaModeWidgets();
    void toggleWidgetsWhileSending(bool widgetsEnabled);

    bool isFullMode();


signals:


public slots:
    void setMinimumMode();
    void setFullMode();

    void setPictureMode();
    void setAudioMode();
    void setVideoMode();
    void setFileMode();

    void cancelMediaMode();

    void setEditingMode(QString postId, QString postType,
                        QString postTitle, QString postText);


    void startMessageForContact(QString id, QString name, QString url);
    void addNickToRecipients(QString id, QString name, QString url);

    void onPublishingOk();
    void onPublishingFailed();

    void setToPublic(bool activated);
    void setToFollowers(bool activated);
    void setCCPublic(bool activated);
    void setCCFollowers(bool activated);

    void updateToCcFields(QString selectorType, QStringList contactsList,
                          QStringList urlsList);

    void updateListsMenus(QVariantList listsList);
    void updateToListsFields(QAction *listAction);
    void updateCcListsFields(QAction *listAction);

    void showHighlightedUrl(QString url);

    void sendPost();

    void findMediaFile();
    void updateProgressBar(qint64 sent, qint64 total);

    void updateCharacterCounter();


private:
    QGridLayout *mainLayout;
    QHBoxLayout *titleLayout;

    QLabel *titleLabel;
    QLineEdit *titleLineEdit;
    QLabel *mediaInfoLabel;
    QPushButton *selectMediaButton;
    QPushButton *removeMediaButton;
    QLabel *pictureLabel;
    QProgressBar *uploadProgressBar;

    QPushButton *toolsButton;

    Composer *composerBox;

    QPushButton *toSelectorButton;
    QAction *toPublicAction;
    QAction *toFollowersAction;
    QMenu *toSelectorMenu;
    QMenu *toSelectorListsMenu;

    QPushButton *ccSelectorButton;
    QAction *ccPublicAction;
    QAction *ccFollowersAction;
    QMenu *ccSelectorMenu;
    QMenu *ccSelectorListsMenu;

    AudienceSelector *audienceSelectorTo;
    AudienceSelector *audienceSelectorCC;

    QLabel *toPublicFollowersLabel;
    QLabel *toAudienceLabel;
    QLabel *ccPublicFollowersLabel;
    QLabel *ccAudienceLabel;

    QStringList toAddressStringList;
    QStringList ccAddressStringList;

    QStringList toListsNameStringList;
    QStringList toListsIdStringList;
    QStringList ccListsNameStringList;
    QStringList ccListsIdStringList;

#ifdef GROUPSUPPORT
    QLabel *groupIdLabel;
    QLineEdit *groupIdLineEdit;
#endif

    QPushButton *addMediaButton;
    QMenu *addMediaMenu;
    QAction *addMediaImageAction;
    QAction *addMediaAudioAction;
    QAction *addMediaVideoAction;
    QAction *addMediaFileAction;

    QLabel *charCounterLabel;

    QLabel *statusInfoLabel;

    QPushButton *postButton;
    QPushButton *cancelButton;


    bool defaultPublicPosting;
    bool showCharacterCounter;

    bool onlyToFollowers;

    QString mediaFilename;
    QString mediaContentType;
    QString lastUsedDirectory;
    QString postType;

    bool editingMode;
    QString editingPostId;

    bool fullMode;

    QNetworkReply *uploadNetworkReply;

    PumpController *pController;
    GlobalObject *globalObj;
};

#endif // PUBLISHER_H
