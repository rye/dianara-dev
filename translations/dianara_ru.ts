<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="ru_RU">
<context>
    <name>ASActivity</name>
    <message>
        <location filename="../src/asactivity.cpp" line="88"/>
        <location filename="../src/asactivity.cpp" line="126"/>
        <source>Public</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/asactivity.cpp" line="398"/>
        <source>%1 by %2</source>
        <comment>1=kind of object: note, comment, etc; 2=author&apos;s name</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ASObject</name>
    <message>
        <location filename="../src/asobject.cpp" line="237"/>
        <source>Note</source>
        <comment>Noun, an object type</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/asobject.cpp" line="242"/>
        <source>Article</source>
        <comment>Noun, an object type</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/asobject.cpp" line="247"/>
        <source>Image</source>
        <comment>Noun, an object type</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/asobject.cpp" line="252"/>
        <source>Audio</source>
        <comment>Noun, an object type</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/asobject.cpp" line="257"/>
        <source>Video</source>
        <comment>Noun, an object type</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/asobject.cpp" line="262"/>
        <source>File</source>
        <comment>Noun, an object type</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/asobject.cpp" line="267"/>
        <source>Comment</source>
        <comment>Noun, as in object type: a comment</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/asobject.cpp" line="272"/>
        <source>Group</source>
        <comment>Noun, an object type</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/asobject.cpp" line="277"/>
        <source>Collection</source>
        <comment>Noun, an object type</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/asobject.cpp" line="282"/>
        <source>Other</source>
        <comment>As in: other type of post</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/asobject.cpp" line="338"/>
        <source>No detailed location</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/asobject.cpp" line="363"/>
        <source>Deleted on %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/asobject.cpp" line="524"/>
        <location filename="../src/asobject.cpp" line="594"/>
        <source>and one other</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/asobject.cpp" line="528"/>
        <location filename="../src/asobject.cpp" line="598"/>
        <source>and %1 others</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ASPerson</name>
    <message>
        <location filename="../src/asperson.cpp" line="146"/>
        <source>Hometown</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AccountDialog</name>
    <message>
        <location filename="../src/accountdialog.cpp" line="75"/>
        <source>Your Pump.io address:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="83"/>
        <source>Get &amp;Verifier Code</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="114"/>
        <source>Verifier code:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="117"/>
        <source>Enter or paste the verifier code provided by your Pump server here</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="151"/>
        <source>&amp;Save Details</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="371"/>
        <source>If the browser doesn&apos;t open automatically, copy this address manually</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="25"/>
        <source>Account Configuration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="42"/>
        <source>First, enter your Webfinger ID, your pump.io address.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="44"/>
        <source>Your address looks like username@pumpserver.org, and you can find it in your profile, in the web interface.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="48"/>
        <source>If your profile is at https://pump.example/yourname, then your address is yourname@pump.example</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="52"/>
        <source>If you don&apos;t have an account yet, you can sign up for one at %1. This link will take you to a random public server.</source>
        <comment>1=link to website</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="60"/>
        <source>If you need help: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="63"/>
        <source>Pump.io User Guide</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="78"/>
        <source>Your address, like username@pumpserver.org</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="86"/>
        <source>After clicking this button, a web browser will open, requesting authorization for Dianara</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="96"/>
        <source>Once you have authorized Dianara from your Pump server web interface, you&apos;ll receive a code called VERIFIER.
Copy it and paste it into the field below.</source>
        <comment>Don&apos;t translate the VERIFIER word!</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="124"/>
        <source>&amp;Authorize Application</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="159"/>
        <source>&amp;Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="167"/>
        <source>Your account is properly configured.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="171"/>
        <source>Press Unlock if you wish to configure a different account.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="179"/>
        <source>&amp;Unlock</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="312"/>
        <source>A web browser will start now, where you can get the verifier code</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="321"/>
        <source>Your Pump address is invalid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="340"/>
        <source>Verifier code is empty</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="352"/>
        <source>Dianara is authorized to access your data</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AudienceSelector</name>
    <message>
        <location filename="../src/audienceselector.cpp" line="32"/>
        <source>&apos;To&apos; List</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/audienceselector.cpp" line="36"/>
        <source>&apos;Cc&apos; List</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/audienceselector.cpp" line="57"/>
        <source>&amp;Add to Selected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/audienceselector.cpp" line="71"/>
        <source>All Contacts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/audienceselector.cpp" line="77"/>
        <source>Select people from the list on the left.
You can drag them with the mouse, click or double-click on them, or select them and use the button below.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/audienceselector.cpp" line="92"/>
        <source>Clear &amp;List</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/audienceselector.cpp" line="100"/>
        <source>&amp;Done</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/audienceselector.cpp" line="106"/>
        <source>&amp;Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/audienceselector.cpp" line="126"/>
        <source>Selected People</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AvatarButton</name>
    <message>
        <location filename="../src/avatarbutton.cpp" line="142"/>
        <source>Open %1&apos;s profile in web browser</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/avatarbutton.cpp" line="146"/>
        <source>Open your profile in web browser</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/avatarbutton.cpp" line="165"/>
        <source>Send message to %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/avatarbutton.cpp" line="174"/>
        <source>Browse messages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/avatarbutton.cpp" line="230"/>
        <source>Stop following</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/avatarbutton.cpp" line="241"/>
        <source>Follow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/avatarbutton.cpp" line="282"/>
        <source>Stop following?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/avatarbutton.cpp" line="283"/>
        <source>Are you sure you want to stop following %1?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/avatarbutton.cpp" line="286"/>
        <source>&amp;Yes, stop following</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/avatarbutton.cpp" line="287"/>
        <source>&amp;No</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ColorPicker</name>
    <message>
        <location filename="../src/colorpicker.cpp" line="34"/>
        <source>Change</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Comment</name>
    <message>
        <location filename="../src/comment.cpp" line="250"/>
        <source>Posted on %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="255"/>
        <source>Modified on %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="113"/>
        <source>Like or unlike this comment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="120"/>
        <source>Quote</source>
        <comment>This is a verb, infinitive</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="127"/>
        <source>Reply quoting this comment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="135"/>
        <source>Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="141"/>
        <source>Modify this comment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="147"/>
        <source>Delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="153"/>
        <source>Erase this comment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="294"/>
        <source>Unlike</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="300"/>
        <source>Like</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="322"/>
        <source>%1 like this comment</source>
        <comment>Plural: %1=list of people like John, Jane, Smith</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="316"/>
        <source>%1 likes this comment</source>
        <comment>Singular: %1=name of just 1 person</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="542"/>
        <source>WARNING: Delete comment?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="543"/>
        <source>Are you sure you want to delete this comment?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="544"/>
        <source>&amp;Yes, delete it</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="544"/>
        <source>&amp;No</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CommenterBlock</name>
    <message>
        <location filename="../src/commenterblock.cpp" line="124"/>
        <source>You can press Control+Enter to send the comment with the keyboard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/commenterblock.cpp" line="40"/>
        <source>Reload comments</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/commenterblock.cpp" line="121"/>
        <location filename="../src/commenterblock.cpp" line="462"/>
        <source>Comment</source>
        <comment>Infinitive verb</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/commenterblock.cpp" line="131"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/commenterblock.cpp" line="133"/>
        <source>Press ESC to cancel the comment if there is no text</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/commenterblock.cpp" line="360"/>
        <source>Show all %1 comments</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/commenterblock.cpp" line="497"/>
        <source>Error: Already composing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/commenterblock.cpp" line="498"/>
        <source>You can&apos;t edit a comment at this time, because another comment is already being composed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/commenterblock.cpp" line="511"/>
        <source>Editing comment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/commenterblock.cpp" line="566"/>
        <source>Posting comment failed.

Try again.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/commenterblock.cpp" line="596"/>
        <source>Sending comment...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/commenterblock.cpp" line="601"/>
        <source>Updating comment...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/commenterblock.cpp" line="610"/>
        <source>Comment is empty.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Composer</name>
    <message>
        <location filename="../src/composer.cpp" line="179"/>
        <source>Type a message here to post it</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="35"/>
        <source>Click here or press Control+N to post a note...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="42"/>
        <source>Symbols</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="60"/>
        <source>Formatting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="62"/>
        <source>Normal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="66"/>
        <source>Bold</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="71"/>
        <source>Italic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="76"/>
        <source>Underline</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="81"/>
        <source>Strikethrough</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="88"/>
        <source>Header</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="93"/>
        <source>List</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="97"/>
        <source>Table</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="101"/>
        <source>Preformatted block</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="105"/>
        <source>Quote block</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="113"/>
        <source>Make a link</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="118"/>
        <source>Insert an image from a web site</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="123"/>
        <source>Insert line</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="134"/>
        <source>&amp;Format</source>
        <comment>Button for text formatting and related options</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="138"/>
        <source>Text Formatting Options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="143"/>
        <source>Paste Text Without Formatting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="186"/>
        <source>Type a comment here</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="449"/>
        <source>Insert as image?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="450"/>
        <source>The link you are pasting seems to point to an image.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="452"/>
        <source>Insert as visible image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="453"/>
        <source>Insert as link</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="629"/>
        <source>Table Size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="633"/>
        <source>How many rows (height)?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="644"/>
        <source>How many columns (width)?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="722"/>
        <source>Insert a link</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="723"/>
        <source>Type or paste a web address here.
You could also select some text first, to turn it into a link.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="735"/>
        <source>Make a link from selected text</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="736"/>
        <source>Type or paste a web address here.
The selected text (%1) will be converted to a link.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="772"/>
        <source>Insert an image from a URL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="773"/>
        <source>Type or paste the image address here.
The link must point to the image file directly.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="789"/>
        <source>Error: Invalid URL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="790"/>
        <source>The address you entered (%1) is not valid.
Image addresses should begin with http:// or https://</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="881"/>
        <source>Cancel message?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="882"/>
        <source>Are you sure you want to cancel this message?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="883"/>
        <source>&amp;Yes, cancel it</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="883"/>
        <source>&amp;No</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ConfigDialog</name>
    <message>
        <location filename="../src/configdialog.cpp" line="68"/>
        <source>minutes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="82"/>
        <source>Top</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="84"/>
        <source>Bottom</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="86"/>
        <source>Left side</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="88"/>
        <source>Right side</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="32"/>
        <source>Program Configuration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="70"/>
        <source>Timeline &amp;update interval</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="210"/>
        <source>posts</source>
        <comment>Goes after a number, as: 25 posts</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="214"/>
        <source>&amp;Posts per page, main timeline</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="219"/>
        <source>posts</source>
        <comment>This goes after a number, like: 10 posts</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="223"/>
        <source>Posts per page, &amp;other timelines</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="90"/>
        <source>&amp;Tabs position</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="96"/>
        <source>&amp;Movable tabs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="325"/>
        <source>Public posts as &amp;default</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="108"/>
        <source>Pro&amp;xy Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="111"/>
        <source>Network configuration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="116"/>
        <source>Set Up F&amp;ilters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="119"/>
        <source>Filtering rules</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="227"/>
        <source>Highlighted activities, except mine</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="228"/>
        <source>Any highlighted activity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="229"/>
        <source>Always</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="230"/>
        <source>Never</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="134"/>
        <source>Comments</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="156"/>
        <source>You are among the recipients of the activity, such as a comment addressed to you.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="160"/>
        <source>Used also when highlighting posts addressed to you in the timelines.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="166"/>
        <source>The activity is in reply to something done by you, such as a comment posted in reply to one of your notes.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="173"/>
        <source>You are the object of the activity, such as someone adding you to a list.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="179"/>
        <source>The activity is related to one of your objects, such as someone liking one of your posts.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="183"/>
        <source>Used also when highlighting your own posts in the timelines.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="189"/>
        <source>Item highlighted due to filtering rules.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="194"/>
        <source>Item is new.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="232"/>
        <source>Show snippets in minor feeds</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="253"/>
        <source>Hide duplicated posts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="258"/>
        <source>Jump to new posts line on update</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="296"/>
        <source>Avatar size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="298"/>
        <source>Show extended share information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="300"/>
        <source>Show extra information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="302"/>
        <source>Highlight post author&apos;s comments</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="304"/>
        <source>Highlight your own comments</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="306"/>
        <source>Ignore SSL errors in images</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="329"/>
        <source>Show character counter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="344"/>
        <source>Don&apos;t inform followers when following someone</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="346"/>
        <source>Don&apos;t inform followers when handling lists</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="359"/>
        <source>As system notifications</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="361"/>
        <source>Using own notifications</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="363"/>
        <source>Don&apos;t show notifications</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="366"/>
        <source>Notification Style</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="375"/>
        <source>Notify when receiving:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="380"/>
        <source>New posts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="385"/>
        <source>Highlighted posts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="390"/>
        <source>New activities in minor feed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="395"/>
        <source>Highlighted activities in minor feed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="416"/>
        <source>Default</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="417"/>
        <source>System iconset, if available</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="418"/>
        <source>Show your current avatar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="419"/>
        <source>Custom icon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="422"/>
        <source>System Tray Icon &amp;Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="425"/>
        <source>S&amp;elect...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="442"/>
        <source>Custom &amp;Icon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="447"/>
        <source>Hide window on startup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="483"/>
        <source>Timelines</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="486"/>
        <source>Posts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="489"/>
        <source>Composer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="492"/>
        <source>Privacy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="495"/>
        <source>Notifications</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="498"/>
        <source>System Tray</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="612"/>
        <source>This is a system notification</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="616"/>
        <source>System notifications are not available!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="618"/>
        <source>Own notifications will be used.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="629"/>
        <source>This is a basic notification</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="791"/>
        <source>Select custom icon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="130"/>
        <source>Post Titles</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="237"/>
        <source>characters</source>
        <comment>This is a suffix, after a number</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="240"/>
        <source>Snippet limit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="132"/>
        <source>Post Contents</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="136"/>
        <source>Minor Feeds</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="288"/>
        <source>Only for images inserted from web sites.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="291"/>
        <source>Use with care.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="327"/>
        <source>Use attachment filename as initial post title</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="474"/>
        <source>General Options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="477"/>
        <source>Fonts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="480"/>
        <source>Colors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="527"/>
        <source>Dianara stores data in this folder:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="538"/>
        <source>&amp;Save Configuration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="544"/>
        <source>&amp;Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="793"/>
        <source>Image files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="795"/>
        <source>All files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="813"/>
        <source>Invalid image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="814"/>
        <source>The selected image is not valid.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ContactCard</name>
    <message>
        <location filename="../src/contactcard.cpp" line="73"/>
        <source>Hometown</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/contactcard.cpp" line="80"/>
        <source>Joined: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/contactcard.cpp" line="88"/>
        <source>Updated: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/contactcard.cpp" line="107"/>
        <source>Bio for %1</source>
        <comment>Abbreviation for Biography, but you can use the full word; %1=contact name</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/contactcard.cpp" line="120"/>
        <source>This user doesn&apos;t have a biography</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/contactcard.cpp" line="124"/>
        <source>No biography for %1</source>
        <comment>%1=contact name</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/contactcard.cpp" line="145"/>
        <source>Open Profile in Web Browser</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/contactcard.cpp" line="152"/>
        <source>Send Message</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/contactcard.cpp" line="159"/>
        <source>Browse Messages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/contactcard.cpp" line="165"/>
        <source>In Lists...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/contactcard.cpp" line="182"/>
        <source>User Options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/contactcard.cpp" line="215"/>
        <source>Follow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/contactcard.cpp" line="227"/>
        <source>Stop Following</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/contactcard.cpp" line="288"/>
        <source>Stop following?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/contactcard.cpp" line="289"/>
        <source>Are you sure you want to stop following %1?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/contactcard.cpp" line="291"/>
        <source>&amp;Yes, stop following</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/contactcard.cpp" line="291"/>
        <source>&amp;No</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ContactList</name>
    <message>
        <location filename="../src/contactlist.cpp" line="32"/>
        <source>Type a partial name or ID to find a contact...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/contactlist.cpp" line="50"/>
        <source>F&amp;ull List</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ContactManager</name>
    <message>
        <location filename="../src/contactmanager.cpp" line="44"/>
        <source>username@server.org or https://server.org/username</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="47"/>
        <source>&amp;Enter address to follow:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="67"/>
        <source>&amp;Follow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="110"/>
        <source>Reload Followers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="115"/>
        <source>Reload Following</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="121"/>
        <source>Export Followers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="126"/>
        <source>Export Following</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="132"/>
        <source>Reload Lists</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="182"/>
        <source>Follo&amp;wers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="185"/>
        <source>Followin&amp;g</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="188"/>
        <source>&amp;Lists</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="203"/>
        <source>Export list of &apos;following&apos; to a file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="204"/>
        <source>Export list of &apos;followers&apos; to a file</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DownloadWidget</name>
    <message>
        <location filename="../src/downloadwidget.cpp" line="46"/>
        <source>Download</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="47"/>
        <source>Save the attached file to your folders</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="59"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="116"/>
        <source>Save File As...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="119"/>
        <source>All files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="153"/>
        <source>Abort download?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="154"/>
        <source>Do you want to stop downloading the attached file?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="155"/>
        <source>&amp;Yes, stop</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="155"/>
        <source>&amp;No, continue</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="162"/>
        <source>Download aborted</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="176"/>
        <source>Download completed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="185"/>
        <source>Download failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="205"/>
        <source>Downloading %1 KiB...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="207"/>
        <source>%1 KiB downloaded</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>EmailChanger</name>
    <message>
        <location filename="../src/emailchanger.cpp" line="28"/>
        <source>Change E-mail Address</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/emailchanger.cpp" line="66"/>
        <source>Change</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/emailchanger.cpp" line="74"/>
        <source>&amp;Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/emailchanger.cpp" line="92"/>
        <source>E-mail Address:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/emailchanger.cpp" line="93"/>
        <source>Again:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/emailchanger.cpp" line="94"/>
        <source>Your Password:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/emailchanger.cpp" line="149"/>
        <source>E-mail addresses don&apos;t match!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/emailchanger.cpp" line="157"/>
        <source>Password is empty!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FilterEditor</name>
    <message>
        <location filename="../src/filtereditor.cpp" line="28"/>
        <source>Filter Editor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="36"/>
        <source>%1 if %2 contains: %3</source>
        <comment>This explains a filter rule, like: Hide if Author ID contains JohnDoe</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="43"/>
        <source>Here you can set some rules for hiding or highlighting stuff. You can filter by content, author or application.

For instance, you can filter out messages posted by the application Open Farm Game, or which contain the word NSFW in the message. You could also highlight messages that contain your name.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="60"/>
        <source>Hide</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="63"/>
        <source>Highlight</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="69"/>
        <source>Post Contents</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="72"/>
        <source>Author ID</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="75"/>
        <source>Application</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="78"/>
        <source>Activity Description</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="81"/>
        <source>Keywords...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="85"/>
        <source>&amp;Add Filter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="93"/>
        <source>Filters in use</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="97"/>
        <source>&amp;Remove Selected Filter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="104"/>
        <source>&amp;Save Filters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="110"/>
        <source>&amp;Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="124"/>
        <source>if</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="128"/>
        <source>contains</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="134"/>
        <source>&amp;New Filter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="141"/>
        <source>C&amp;urrent Filters</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FontPicker</name>
    <message>
        <location filename="../src/fontpicker.cpp" line="50"/>
        <source>Change</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>HelpWidget</name>
    <message>
        <location filename="../src/helpwidget.cpp" line="26"/>
        <source>Basic Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="39"/>
        <source>Getting started</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="70"/>
        <source>The first time you start Dianara, you should see the Account Configuration dialog. There, enter your Pump.io address as name@server and press the Get Verifier Code button.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="75"/>
        <source>Then, your usual web browser should load the authorization page in your Pump.io server. There, you&apos;ll have to copy the full VERIFIER code, and paste it into Dianara&apos;s second field. Then press Authorize Application, and once it&apos;s confirmed, press Save Details.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="81"/>
        <source>At this point, your profile, contact lists and timelines will be loaded.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="84"/>
        <source>You should take a look at the Program Configuration window, under the Settings - Configure Dianara menu. There are several interesting options there.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="40"/>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="104"/>
        <source>You can configure several things to your liking in the settings, like the time interval between timeline updates, how many posts per page you want, highlight colors, notifications or how the system tray icon looks.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="41"/>
        <source>Timelines</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="47"/>
        <source>Contents</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="88"/>
        <source>Keep in mind that there are a lot of places in Dianara where you can get more information by hovering over some text or button with your mouse, and waiting for the tooltip to appear.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="93"/>
        <source>If you&apos;re new to Pump.io, take a look at this guide:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="109"/>
        <source>Here, you can also activate the option to always publish your posts as Public by default. You can always change that at the moment of posting.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="122"/>
        <source>The main timeline, where you&apos;ll see all the stuff posted or shared by the people you follow.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="125"/>
        <source>Messages timeline, where you&apos;ll see messages sent to you specifically. These messages might have been sent to other people too.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="129"/>
        <source>Activity timeline, where you&apos;ll see your own posts, or posts shared by you.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="132"/>
        <source>Favorites timeline, where you&apos;ll see the posts and comments you&apos;ve liked. This can be used as a bookmark system.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="137"/>
        <source>The fifth timeline is the minor timeline, also known as the Meanwhile. This is visible on the left side, though it can be hidden. Here you&apos;ll see minor activities done by everyone you follow, such as comment actions, liking posts or following people.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="148"/>
        <source>These activities might have a &apos;+&apos; button in them. Press it to open the post they&apos;re referencing. Also, as in many other places, you can hover with your mouse to see relevant information in the tooltip.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="42"/>
        <source>Posting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="153"/>
        <source>New messages appear highlighted in a different color. You can mark them as read just by clicking on any empty parts of the message.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="163"/>
        <source>You can post notes by clicking in the text field at the top of the window or by pressing Control+N. Setting a title for your post is optional, but highly recommended, as it will help to better identify references to your post in the minor feed, e-mail notifications, etc.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="170"/>
        <source>It is possible to attach images, audio, video, and general files, like PDF documents, to your post.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="173"/>
        <source>You can use the Format button to add formatting to your text, like bold or italics. Some of these options require text to be selected before they are used.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="180"/>
        <source>If you add a specific person to the &apos;To&apos; list, they will receive your message in their direct messages tab.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="187"/>
        <source>Choose one with the arrow keys and press Enter to complete the name. This will add that person to the recipients list.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="191"/>
        <source>You can create private messages by adding specific people to these lists, and unselecting the Followers or the Public options.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="43"/>
        <source>Managing contacts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="201"/>
        <source>You can see the lists of people you follow, and who follow you from the Contacts tab.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="204"/>
        <source>There, you can also manage person lists, used mainly to send posts to specific groups of people.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="211"/>
        <source>You can click on any avatars in the posts, the comments, and the Meanwhile column, and you will get a menu with several options, one of which is following or unfollowing that person.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="44"/>
        <source>Keyboard controls</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="95"/>
        <source>Pump.io User Guide</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="119"/>
        <source>There are seven timelines:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="143"/>
        <source>The sixth and seventh timelines are also minor timelines, similar to the Meanwhile, but containing only activities directly addressed to you (Mentions) and activities done by you (Actions).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="177"/>
        <source>You can select who will see your post by using the To and Cc buttons.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="183"/>
        <source>You can also type &apos;@&apos; and the first characters of the name of a contact to bring up a popup menu with matching choices.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="207"/>
        <source>There is a text field at the top, where you can directly enter addresses of new contacts to follow them.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="216"/>
        <source>You can also send a direct message (initially private) to that contact from this menu.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="219"/>
        <source>You can find a list with some Pump.io users and other information here:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="225"/>
        <source>Users by language</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="234"/>
        <source>The most common actions found on the menus have keyboard shortcuts written next to them, like F5 or Control+N.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="238"/>
        <source>Besides that, you can use:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="241"/>
        <source>Control+Up/Down/PgUp/PgDown/Home/End to move around the timeline.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="244"/>
        <source>Control+Left/Right to jump one page in the timeline.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="247"/>
        <source>Control+G to go to any page in the timeline directly.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="250"/>
        <source>Control+1/2/3 to switch between the minor feeds.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="253"/>
        <source>Control+Enter to post, when you&apos;re done composing a note or a comment. If the note is empty, you can cancel it by pressing ESC.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="45"/>
        <source>Command line options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="257"/>
        <source>While composing a note, press Enter to jump from the title to the message body. Also, pressing the Up arrow while you&apos;re at the start of the message, jumps back to the title.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="262"/>
        <source>Control+Enter to finish creating a list of recipients for a post, in the &apos;To&apos; or &apos;Cc&apos; lists.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="272"/>
        <source>You can use the --config parameter to run the program with a different configuration. This can be useful to use two or more different accounts. You can even run two instances of Dianara at the same time.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="277"/>
        <source>Use the --debug parameter to have extra information in your terminal window, about what the program is doing.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="291"/>
        <source>&amp;Close</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ImageViewer</name>
    <message>
        <location filename="../src/imageviewer.cpp" line="50"/>
        <source>Image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/imageviewer.cpp" line="61"/>
        <source>&amp;Save As...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/imageviewer.cpp" line="77"/>
        <source>&amp;Restart Animation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/imageviewer.cpp" line="88"/>
        <source>&amp;Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/imageviewer.cpp" line="138"/>
        <source>Save Image...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/imageviewer.cpp" line="145"/>
        <source>Close Viewer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/imageviewer.cpp" line="174"/>
        <source>Save Image As...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/imageviewer.cpp" line="176"/>
        <source>Image files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/imageviewer.cpp" line="177"/>
        <source>All files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/imageviewer.cpp" line="188"/>
        <source>Error saving image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/imageviewer.cpp" line="189"/>
        <source>There was a problem while saving %1.

Filename should end in .jpg or .png extensions.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ListsManager</name>
    <message>
        <location filename="../src/listsmanager.cpp" line="38"/>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="38"/>
        <source>Members</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="51"/>
        <source>Add Mem&amp;ber</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="58"/>
        <source>&amp;Remove Member</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="66"/>
        <source>&amp;Delete Selected List</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="73"/>
        <source>Add New &amp;List</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="90"/>
        <source>Create L&amp;ist</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="97"/>
        <source>&amp;Add to List</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="309"/>
        <source>Are you sure you want to delete %1?</source>
        <comment>1=Name of a person list</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="441"/>
        <source>Remove person from list?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="442"/>
        <source>Are you sure you want to remove %1 from the %2 list?</source>
        <comment>1=Name of a person, 2=name of a list</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="447"/>
        <source>&amp;Yes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="76"/>
        <source>Type a name for the new list...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="84"/>
        <source>Type an optional description here</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="308"/>
        <source>WARNING: Delete list?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="311"/>
        <source>&amp;Yes, delete it</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="311"/>
        <location filename="../src/listsmanager.cpp" line="447"/>
        <source>&amp;No</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LogViewer</name>
    <message>
        <location filename="../src/logviewer.cpp" line="25"/>
        <source>Log</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/logviewer.cpp" line="54"/>
        <source>Clear &amp;Log</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/logviewer.cpp" line="60"/>
        <source>&amp;Close</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../src/mainwindow.cpp" line="1000"/>
        <source>Side &amp;Panel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1020"/>
        <source>Status &amp;Bar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2352"/>
        <source>&amp;Timeline</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2353"/>
        <source>The main timeline</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2366"/>
        <source>&amp;Activity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2367"/>
        <source>Your own posts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2374"/>
        <source>Your favorited posts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2359"/>
        <source>&amp;Messages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2360"/>
        <source>Messages sent explicitly to you</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="395"/>
        <source>&amp;Contacts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="684"/>
        <location filename="../src/mainwindow.cpp" line="1268"/>
        <source>Initializing...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="723"/>
        <source>Your account is not configured yet.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="707"/>
        <source>Dianara started.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="155"/>
        <source>Minor activities done by everyone, such as replying to posts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="174"/>
        <source>Minor activities addressed to you</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="190"/>
        <source>Minor activities done by you</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="398"/>
        <source>The people you follow, the ones who follow you, and your person lists</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="669"/>
        <source>Press F1 for help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="708"/>
        <source>Running with Qt v%1.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="729"/>
        <source>Click here to configure your account</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="855"/>
        <source>&amp;Session</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="953"/>
        <source>Auto-update &amp;Timelines</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="963"/>
        <source>Mark All as Read</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="974"/>
        <source>&amp;Post a Note</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="988"/>
        <source>&amp;Quit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="997"/>
        <source>&amp;View</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1011"/>
        <source>&amp;Toolbar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1032"/>
        <source>Full &amp;Screen</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1043"/>
        <source>&amp;Log</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1055"/>
        <source>S&amp;ettings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1059"/>
        <source>Edit &amp;Profile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1067"/>
        <source>&amp;Account</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1098"/>
        <source>Basic &amp;Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1117"/>
        <source>Report a &amp;Bug</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1128"/>
        <source>Pump.io User &amp;Guide</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1135"/>
        <source>Some Pump.io &amp;Tips</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1142"/>
        <source>List of Some Pump.io &amp;Users</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1149"/>
        <source>Pump.io &amp;Network Status Website</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1206"/>
        <source>Toolbar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1252"/>
        <source>Open the log viewer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1275"/>
        <source>Auto-updating enabled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1282"/>
        <source>Auto-updating disabled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1799"/>
        <source>Proxy password required</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1800"/>
        <source>You have configured a proxy server with authentication, but the password is not set.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1805"/>
        <source>Enter the password for your proxy server:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1938"/>
        <source>Starting automatic update of timelines, once every %1 minutes.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1945"/>
        <source>Stopping automatic update of timelines.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2123"/>
        <source>Received %1 older posts in &apos;%2&apos;.</source>
        <comment>%1 is a number, %2 = name of a timeline</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2174"/>
        <source>1 more pending to receive.</source>
        <comment>singular, one post</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2179"/>
        <source>%1 more pending to receive.</source>
        <comment>plural, several posts</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2191"/>
        <location filename="../src/mainwindow.cpp" line="2576"/>
        <source>Also:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2299"/>
        <source>Last update: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2130"/>
        <location filename="../src/mainwindow.cpp" line="2514"/>
        <source>&apos;%1&apos; updated.</source>
        <comment>%1 is the name of a feed</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2198"/>
        <source>1 filtered out.</source>
        <comment>singular, refers to a post</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2203"/>
        <source>%1 filtered out.</source>
        <comment>plural, refers to posts</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2216"/>
        <source>1 deleted.</source>
        <comment>singular, refers to a post</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2221"/>
        <source>%1 deleted.</source>
        <comment>plural, refers to posts</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2526"/>
        <source>There is 1 new activity.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2530"/>
        <source>There are %1 new activities.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2541"/>
        <source>1 highlighted.</source>
        <comment>singular, refers to an activity</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2546"/>
        <source>%1 highlighted.</source>
        <comment>plural, refers to activities</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2580"/>
        <source>1 filtered out.</source>
        <comment>singular, refers to one activity</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2585"/>
        <source>%1 filtered out.</source>
        <comment>plural, several activities</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2640"/>
        <source>No new activities.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2713"/>
        <source>Error storing image!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2715"/>
        <source>%1 bytes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3134"/>
        <source>Closing due to environment shutting down...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3157"/>
        <location filename="../src/mainwindow.cpp" line="3242"/>
        <source>Quit?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3158"/>
        <source>You are composing a note or a comment.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3160"/>
        <source>Do you really want to close Dianara?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3161"/>
        <location filename="../src/mainwindow.cpp" line="3250"/>
        <source>&amp;Yes, close the program</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3161"/>
        <location filename="../src/mainwindow.cpp" line="3250"/>
        <source>&amp;No</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3184"/>
        <source>Shutting down Dianara...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3243"/>
        <source>System tray icon is not available.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3245"/>
        <source>Dianara cannot be hidden in the system tray.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3248"/>
        <source>Do you want to close the program completely?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2136"/>
        <source>Timeline updated at %1.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="858"/>
        <source>Update %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2429"/>
        <source>Your Pump.io account is not configured</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2757"/>
        <source>Link to: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3045"/>
        <source>With Dianara you can see your timelines, create new posts, upload pictures and other media, interact with posts, manage your contacts and follow new people.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3054"/>
        <source>English translation by JanKusanagi.</source>
        <comment>TRANSLATORS: Change this with your language and name. If there was another translator before you, add your name after theirs ;)</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3068"/>
        <source>Dianara is licensed under the GNU GPL license, and uses some Oxygen icons: http://www.oxygen-icons.org/ (LGPL licensed)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1084"/>
        <source>&amp;Configure Dianara</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1076"/>
        <source>&amp;Filters and Highlighting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1094"/>
        <source>&amp;Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1110"/>
        <source>Visit &amp;Website</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1159"/>
        <source>About &amp;Dianara</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1878"/>
        <source>Your biography is empty</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1913"/>
        <source>Click to edit your profile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2156"/>
        <source>1 highlighted.</source>
        <comment>singular, refers to a post</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2161"/>
        <source>%1 highlighted.</source>
        <comment>plural, refers to posts</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2270"/>
        <source>No new posts.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2347"/>
        <source>Total posts: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2373"/>
        <source>Favor&amp;ites</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2506"/>
        <source>Received %1 older activities in &apos;%2&apos;.</source>
        <comment>%1 is a number, %2 = name of feed</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2519"/>
        <source>Minor feed updated at %1.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2559"/>
        <source>1 more pending to receive.</source>
        <comment>singular, 1 activity</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2564"/>
        <source>%1 more pending to receive.</source>
        <comment>plural, several activities</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3102"/>
        <source>&amp;Hide Window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3112"/>
        <source>&amp;Show Window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2142"/>
        <source>There is 1 new post.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2146"/>
        <source>There are %1 new posts.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3032"/>
        <source>About Dianara</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3042"/>
        <source>Dianara is a pump.io social networking client.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3061"/>
        <source>Thanks to all the testers, translators and packagers, who help make Dianara better!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MinorFeed</name>
    <message>
        <location filename="../src/minorfeed.cpp" line="64"/>
        <source>Older Activities</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/minorfeed.cpp" line="67"/>
        <source>Get previous minor activities</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/minorfeed.cpp" line="94"/>
        <source>There are no activities to show yet.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/minorfeed.cpp" line="466"/>
        <source>Get %1 newer</source>
        <comment>As in: Get 3 newer (activities)</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MinorFeedItem</name>
    <message>
        <location filename="../src/minorfeeditem.cpp" line="67"/>
        <source>Using %1</source>
        <comment>Application used to generate this activity</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/minorfeeditem.cpp" line="81"/>
        <source>To: %1</source>
        <comment>1=people to whom this activity was sent</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/minorfeeditem.cpp" line="88"/>
        <source>Cc: %1</source>
        <comment>1=people to whom this activity was sent as CC</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/minorfeeditem.cpp" line="257"/>
        <source>Open referenced post</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MiscHelpers</name>
    <message>
        <location filename="../src/mischelpers.cpp" line="238"/>
        <source>bytes</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PageSelector</name>
    <message>
        <location filename="../src/pageselector.cpp" line="26"/>
        <source>Jump to page</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/pageselector.cpp" line="34"/>
        <source>Page number:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/pageselector.cpp" line="48"/>
        <source>&amp;First</source>
        <comment>As in: first page</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/pageselector.cpp" line="55"/>
        <source>&amp;Last</source>
        <comment>As in: last page</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/pageselector.cpp" line="62"/>
        <source>Newer</source>
        <comment>As in: newer pages</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/pageselector.cpp" line="78"/>
        <source>Older</source>
        <comment>As in: older pages</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/pageselector.cpp" line="86"/>
        <source>&amp;Go</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/pageselector.cpp" line="94"/>
        <source>&amp;Cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PeopleWidget</name>
    <message>
        <location filename="../src/peoplewidget.cpp" line="40"/>
        <source>&amp;Search:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/peoplewidget.cpp" line="43"/>
        <source>Enter a name here to search for it</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/peoplewidget.cpp" line="95"/>
        <source>Add a contact to a list</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/peoplewidget.cpp" line="105"/>
        <source>&amp;Cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Post</name>
    <message>
        <location filename="../src/post.cpp" line="1699"/>
        <source>Like</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1698"/>
        <source>Like this post</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1332"/>
        <source>1 like</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1401"/>
        <source>1 comment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1485"/>
        <source>Shared %1 times</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="252"/>
        <source>Shared on %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1613"/>
        <source>Edited: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="958"/>
        <source>In</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="256"/>
        <location filename="../src/post.cpp" line="910"/>
        <source>Using %1</source>
        <comment>1=Program used for posting or sharing</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="649"/>
        <source>Share</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="673"/>
        <source>Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1098"/>
        <source>Loading image...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1337"/>
        <source>%1 likes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1406"/>
        <source>%1 comments</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="684"/>
        <source>Delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="142"/>
        <source>Post</source>
        <comment>Noun, not verb</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="199"/>
        <source>Via %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="901"/>
        <source>Posted on %1</source>
        <comment>1=Date</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="208"/>
        <location filename="../src/post.cpp" line="421"/>
        <source>To</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="630"/>
        <source>If you select some text, it will be quoted.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="657"/>
        <source>Unshare</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="659"/>
        <source>Unshare this post</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="302"/>
        <source>Open post in web browser</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="37"/>
        <source>Click to download the attachment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="214"/>
        <location filename="../src/post.cpp" line="437"/>
        <source>Cc</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="311"/>
        <source>Copy post link to clipboard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="330"/>
        <source>Normalize text colors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="343"/>
        <location filename="../src/post.cpp" line="526"/>
        <source>&amp;Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="903"/>
        <source>Type</source>
        <comment>As in: type of object</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="917"/>
        <source>Modified on %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="505"/>
        <source>Parent</source>
        <comment>As in &apos;Open the parent post&apos;. Try to use the shortest word!</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="513"/>
        <source>Open the parent post, to which this one replies</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="626"/>
        <source>Comment</source>
        <comment>verb, for the comment button</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="628"/>
        <source>Reply to this post.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="651"/>
        <source>Share this post with your contacts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="675"/>
        <source>Modify this post</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="686"/>
        <source>Erase this post</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="749"/>
        <source>Join Group</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="754"/>
        <source>%1 members in the group</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1071"/>
        <source>Image is animated. Click on it to play.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1093"/>
        <source>Couldn&apos;t load image!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1116"/>
        <source>Attached Audio</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1126"/>
        <source>Attached Video</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1136"/>
        <source>Attached File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1306"/>
        <source>%1 likes this</source>
        <comment>One person</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1311"/>
        <source>%1 like this</source>
        <comment>More than one person</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1446"/>
        <source>%1 shared this</source>
        <comment>%1 = One person name</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1451"/>
        <source>%1 shared this</source>
        <comment>%1 = Names for more than one person</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1480"/>
        <source>Shared once</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1691"/>
        <source>You like this</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1692"/>
        <source>Unlike</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1820"/>
        <source>Share post?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1821"/>
        <source>Do you want to share %1&apos;s post?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1823"/>
        <source>&amp;Yes, share it</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1823"/>
        <location filename="../src/post.cpp" line="1842"/>
        <location filename="../src/post.cpp" line="1886"/>
        <source>&amp;No</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1839"/>
        <source>Unshare post?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1840"/>
        <source>Do you want to unshare %1&apos;s post?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1842"/>
        <source>&amp;Yes, unshare it</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1883"/>
        <source>WARNING: Delete post?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1884"/>
        <source>Are you sure you want to delete this post?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1886"/>
        <source>&amp;Yes, delete it</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="36"/>
        <source>Click the image to see it in full size</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ProfileEditor</name>
    <message>
        <location filename="../src/profileeditor.cpp" line="27"/>
        <source>Profile Editor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="44"/>
        <source>This is your Pump address</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="46"/>
        <source>This is the e-mail address associated with your account, for things such as notifications and password recovery</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="61"/>
        <source>Change &amp;E-mail...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="73"/>
        <source>Change &amp;Avatar...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="83"/>
        <source>This is your visible name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="102"/>
        <source>&amp;Save Profile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="109"/>
        <source>&amp;Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="136"/>
        <source>Webfinger ID</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="137"/>
        <source>E-mail</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="139"/>
        <source>Avatar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="141"/>
        <source>Full &amp;Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="142"/>
        <source>&amp;Hometown</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="143"/>
        <source>&amp;Bio</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="186"/>
        <source>Not set</source>
        <comment>In reference to the e-mail not being set for the account</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="243"/>
        <source>Select avatar image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="245"/>
        <source>Image files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="247"/>
        <source>All files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="270"/>
        <source>Invalid image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="271"/>
        <source>The selected image is not valid.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ProxyDialog</name>
    <message>
        <location filename="../src/proxydialog.cpp" line="30"/>
        <source>Proxy Configuration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/proxydialog.cpp" line="38"/>
        <source>Do not use a proxy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/proxydialog.cpp" line="55"/>
        <source>Your proxy username</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/proxydialog.cpp" line="60"/>
        <source>Note: Password is not stored in a secure manner. If you wish, you can leave the field empty, and you&apos;ll be prompted for the password on startup.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/proxydialog.cpp" line="74"/>
        <source>&amp;Save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/proxydialog.cpp" line="80"/>
        <source>&amp;Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/proxydialog.cpp" line="95"/>
        <source>Proxy &amp;Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/proxydialog.cpp" line="97"/>
        <source>&amp;Hostname</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/proxydialog.cpp" line="99"/>
        <source>&amp;Port</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/proxydialog.cpp" line="101"/>
        <source>Use &amp;Authentication</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/proxydialog.cpp" line="103"/>
        <source>&amp;User</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/proxydialog.cpp" line="105"/>
        <source>Pass&amp;word</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Publisher</name>
    <message>
        <location filename="../src/publisher.cpp" line="69"/>
        <source>Title</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="801"/>
        <source>Select Picture...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="803"/>
        <source>Find the picture in your folders</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="151"/>
        <location filename="../src/publisher.cpp" line="184"/>
        <location filename="../src/publisher.cpp" line="431"/>
        <location filename="../src/publisher.cpp" line="448"/>
        <source>Public</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="78"/>
        <source>Add a brief title for the post here (recommended)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="106"/>
        <source>Remove</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="108"/>
        <source>Cancel the attachment, and go back to a regular note</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="156"/>
        <location filename="../src/publisher.cpp" line="189"/>
        <location filename="../src/publisher.cpp" line="435"/>
        <location filename="../src/publisher.cpp" line="452"/>
        <source>Followers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="161"/>
        <location filename="../src/publisher.cpp" line="194"/>
        <source>Lists</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="176"/>
        <source>To...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="211"/>
        <source>Select who will get a copy of this post</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="270"/>
        <source>Other</source>
        <comment>as in other kinds of files</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="320"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="322"/>
        <source>Cancel the post</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="805"/>
        <source>Picture not set</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="824"/>
        <source>Select Audio File...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="826"/>
        <source>Find the audio file in your folders</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="828"/>
        <source>Audio file not set</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="848"/>
        <source>Select Video...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="850"/>
        <source>Find the video in your folders</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="852"/>
        <source>Video not set</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="871"/>
        <source>Select File...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="873"/>
        <source>Find the file in your folders</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="875"/>
        <source>File not set</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="912"/>
        <location filename="../src/publisher.cpp" line="955"/>
        <source>Error: Already composing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="913"/>
        <source>You can&apos;t edit a post at this time, because a post is already being composed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="929"/>
        <source>Update</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="942"/>
        <source>Editing post</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="956"/>
        <source>You can&apos;t create a message for %1 at this time, because a post is already being composed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1022"/>
        <source>Posting failed.

Try again.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1311"/>
        <source>Warning: You have no followers yet</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1312"/>
        <source>You&apos;re trying to post to your followers only, but you don&apos;t have any followers yet.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1316"/>
        <source>If you post like this, no one will be able to see your message.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1319"/>
        <source>Do you want to make the post public instead of followers-only?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1322"/>
        <source>&amp;Yes, make it public</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1324"/>
        <source>&amp;Cancel, go back to the post</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1374"/>
        <source>Updating...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1385"/>
        <source>Post is empty.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1389"/>
        <source>File not selected.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1410"/>
        <source>Select one image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1411"/>
        <source>Image files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1440"/>
        <source>Select one file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1443"/>
        <source>Invalid file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1444"/>
        <source>The file type cannot be detected.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1451"/>
        <source>All files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1466"/>
        <source>Since you&apos;re uploading an image, you could scale it down a little or save it in a more compressed format, like JPG.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1471"/>
        <source>File is too big</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1472"/>
        <source>Dianara currently limits file uploads to 10 MiB per post, to prevent possible storage or network problems in the servers.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1477"/>
        <source>This is a temporary measure, since the servers cannot set their own limits yet.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1482"/>
        <source>Sorry for the inconvenience.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1502"/>
        <source>Resolution</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1513"/>
        <source>Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1515"/>
        <source>Size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1555"/>
        <source>%1 KiB of %2 KiB uploaded</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1414"/>
        <source>Invalid image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="64"/>
        <source>Setting a title helps make the Meanwhile feed more informative</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="209"/>
        <source>Cc...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="312"/>
        <location filename="../src/publisher.cpp" line="713"/>
        <source>Post</source>
        <comment>verb</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1323"/>
        <source>&amp;No, post to my followers only</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1415"/>
        <source>The image format cannot be detected.
The extension might be wrong, like a GIF image renamed to image.jpg or similar.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1421"/>
        <source>Select one audio file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1422"/>
        <source>Audio files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1425"/>
        <source>Invalid audio file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1426"/>
        <source>The audio format cannot be detected.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1430"/>
        <source>Select one video file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1431"/>
        <source>Video files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1435"/>
        <source>Invalid video file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1436"/>
        <source>The video format cannot be detected.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1344"/>
        <source>Posting...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="171"/>
        <location filename="../src/publisher.cpp" line="204"/>
        <source>People...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="178"/>
        <source>Select who will see this post</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="252"/>
        <source>Picture</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="258"/>
        <source>Audio</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="264"/>
        <source>Video</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="286"/>
        <source>Ad&amp;d...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="288"/>
        <source>Upload media, like pictures or videos</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="314"/>
        <source>Hit Control+Enter to post with the keyboard</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PumpController</name>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="609"/>
        <source>Creating person list...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="621"/>
        <source>Deleting person list...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="636"/>
        <source>Getting a person list...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="680"/>
        <source>Adding person to list...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="722"/>
        <source>Removing person from list...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="770"/>
        <source>Creating group...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="800"/>
        <source>Joining group...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="825"/>
        <source>Leaving group...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="840"/>
        <source>Getting likes...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="872"/>
        <source>Getting comments...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="977"/>
        <source>Activity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="980"/>
        <source>Favorites</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="984"/>
        <source>Meanwhile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="987"/>
        <source>Mentions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="990"/>
        <source>Actions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1113"/>
        <source>Uploading %1</source>
        <comment>1=filename</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1375"/>
        <source>HTTP error</source>
        <comment>For the following HTTP error codesyou can check http://en.wikipedia.org/wiki/List_of_HTTP_status_codes in your language</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1387"/>
        <source>Gateway Timeout</source>
        <comment>HTTP 504 error string</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1397"/>
        <source>Service Unavailable</source>
        <comment>HTTP 503 error string</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1415"/>
        <source>Not Implemented</source>
        <comment>HTTP 501 error string</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1425"/>
        <source>Internal Server Error</source>
        <comment>HTTP 500 error string</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1445"/>
        <source>Gone</source>
        <comment>HTTP 410 error string</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1455"/>
        <source>Not Found</source>
        <comment>HTTP 404 error string</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1465"/>
        <source>Forbidden</source>
        <comment>HTTP 403 error string</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1475"/>
        <source>Unauthorized</source>
        <comment>HTTP 401 error string</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1486"/>
        <source>Bad Request</source>
        <comment>HTTP 400 error string</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1505"/>
        <source>Moved Temporarily</source>
        <comment>HTTP 302 error string</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1515"/>
        <source>Moved Permanently</source>
        <comment>HTTP 301 error string</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1524"/>
        <source>Error connecting to %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1533"/>
        <source>Unhandled HTTP error code %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1651"/>
        <source>Profile received.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1653"/>
        <source>Followers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1656"/>
        <source>Following</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1670"/>
        <source>Profile updated.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1837"/>
        <source>Comment %1 posted successfully.</source>
        <comment>%1 is a piece of the comment</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1744"/>
        <source>Avatar published successfully.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1862"/>
        <source>1 comment received.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1866"/>
        <source>%1 comments received.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1893"/>
        <source>Post by %1 shared successfully.</source>
        <comment>1=author of the post we are sharing</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1937"/>
        <source>Adding items...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2036"/>
        <source>Following %1 (%2) successfully.</source>
        <comment>%1 is a person&apos;s name, %2 is the ID</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2046"/>
        <source>Stopped following %1 (%2) successfully.</source>
        <comment>%1 is a person&apos;s name, %2 is the ID</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2096"/>
        <source>List of &apos;following&apos; completely received.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2104"/>
        <source>Partial list of &apos;following&apos; received.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2127"/>
        <source>List of &apos;followers&apos; completely received.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2133"/>
        <source>Partial list of &apos;followers&apos; received.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2202"/>
        <source>Person list deleted successfully.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2222"/>
        <source>Person list received.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2353"/>
        <source>File downloaded successfully.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2390"/>
        <source>File uploaded successfully. Posting message...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2655"/>
        <source>OAuth error while authorizing application.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="236"/>
        <source>Authorized to use account %1. Getting initial data.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="241"/>
        <source>There is no authorized account.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="508"/>
        <source>Getting list of &apos;Following&apos;...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="520"/>
        <source>Getting list of &apos;Followers&apos;...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="573"/>
        <source>Getting list of person lists...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="867"/>
        <source>The comments for this post cannot be loaded due to missing data on the server.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="924"/>
        <source>Getting &apos;%1&apos;...</source>
        <comment>%1 is the name of a feed</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="971"/>
        <source>Timeline</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="974"/>
        <source>Messages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="994"/>
        <source>User timeline</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1679"/>
        <source>E-mail updated: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1707"/>
        <source>%1 published successfully. Updating post content...</source>
        <comment>%1 is the type of object: note, image...</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1723"/>
        <source>Untitled post %1 published successfully.</source>
        <comment>%1 is a piece of the post</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1727"/>
        <source>Post %1 published successfully.</source>
        <comment>%1 is the title of the post</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1775"/>
        <source>Untitled post %1 updated successfully.</source>
        <comment>%1 is a piece of the post</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1779"/>
        <source>Post %1 updated successfully.</source>
        <comment>%1 is the title of the post</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1789"/>
        <source>Comment %1 updated successfully.</source>
        <comment>%1 is a piece of the comment</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1803"/>
        <source>Message liked or unliked successfully.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1817"/>
        <source>Likes received.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1924"/>
        <source>Received &apos;%1&apos;.</source>
        <comment>%1 is the name of a feed</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2020"/>
        <source>Message deleted successfully.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2170"/>
        <source>List of &apos;lists&apos; received.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2191"/>
        <source>Person list &apos;%1&apos; created successfully.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2257"/>
        <source>%1 (%2) added to list successfully.</source>
        <comment>1=contact name, 2=contact ID</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2277"/>
        <source>%1 (%2) removed from list successfully.</source>
        <comment>1=contact name, 2=contact ID</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2288"/>
        <source>Group %1 created successfully.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2299"/>
        <source>Group %1 joined successfully.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2312"/>
        <source>Left the %1 group successfully.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2399"/>
        <source>Avatar uploaded.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2443"/>
        <source>SSL errors in connection to %1!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2455"/>
        <source>Loading external image from %1 regardless of SSL errors, as configured...</source>
        <comment>%1 is a hostname</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2484"/>
        <source>The application is not registered with your server yet. Registering...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2521"/>
        <source>Getting OAuth token...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2542"/>
        <source>OAuth support error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2543"/>
        <source>Your installation of QOAuth, a library used by Dianara, doesn&apos;t seem to have HMAC-SHA1 support.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2547"/>
        <source>You probably need to install the OpenSSL plugin for QCA: %1, %2 or similar.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2604"/>
        <source>Authorization error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2605"/>
        <source>There was an OAuth error while trying to get the authorization token.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2608"/>
        <source>QOAuth error %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2640"/>
        <source>Application authorized successfully.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2678"/>
        <source>Waiting for proxy password...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2707"/>
        <source>Still waiting for profile. Trying again...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2816"/>
        <source>Some initial data was not received. Restarting initialization.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2824"/>
        <source>Some initial data was not received after several attempts. Something might be wrong with your server. You might still be able to use the service normally.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2834"/>
        <source>All initial data received. Initialization complete.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2845"/>
        <source>Ready.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TimeLine</name>
    <message>
        <location filename="../src/timeline.cpp" line="67"/>
        <source>Welcome to Dianara</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="69"/>
        <source>Dianara is a &lt;b&gt;Pump.io&lt;/b&gt; client.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="72"/>
        <source>If you don&apos;t have a Pump account yet, you can get one at the following address, for instance:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="79"/>
        <source>Press &lt;b&gt;F1&lt;/b&gt; if you want to open the Help window.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="82"/>
        <source>First, configure your account from the &lt;b&gt;Settings - Account&lt;/b&gt; menu.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="85"/>
        <source>After the process is done, your profile and timelines should update automatically.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="89"/>
        <source>Take a moment to look around the menus and the Configuration window.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="93"/>
        <source>You can also set your profile data and picture from the &lt;b&gt;Settings - Edit Profile&lt;/b&gt; menu.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="97"/>
        <source>There are tooltips everywhere, so if you hover over a button or a text field with your mouse, you&apos;ll probably see some extra information.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="104"/>
        <source>Dianara&apos;s blog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="106"/>
        <source>Pump.io User Guide</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="116"/>
        <source>Direct Messages Timeline</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="117"/>
        <source>Here, you&apos;ll see posts specifically directed to you.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="126"/>
        <source>Activity Timeline</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="127"/>
        <source>You&apos;ll see your own posts here.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="135"/>
        <source>Favorites Timeline</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="136"/>
        <source>Posts and comments you&apos;ve liked.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="210"/>
        <source>Newest</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="233"/>
        <source>Newer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="240"/>
        <source>Older</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="349"/>
        <source>Requesting...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="443"/>
        <source>Loading...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="565"/>
        <source>Page %1 of %2.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="569"/>
        <source>Showing %1 posts per page.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="572"/>
        <source>%1 posts in total.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="576"/>
        <source>Click here or press Control+G to jump to a specific page</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="672"/>
        <source>&apos;%1&apos; cannot be updated because a comment is currently being composed.</source>
        <comment>%1 = feed&apos;s name</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="764"/>
        <source>%1 more posts pending for next update.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="770"/>
        <source>Click here to receive them now.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="1029"/>
        <source>There are no posts</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Timestamp</name>
    <message>
        <location filename="../src/timestamp.cpp" line="61"/>
        <source>Invalid timestamp!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/timestamp.cpp" line="93"/>
        <source>A minute ago</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/timestamp.cpp" line="97"/>
        <source>%1 minutes ago</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/timestamp.cpp" line="108"/>
        <source>An hour ago</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/timestamp.cpp" line="112"/>
        <source>%1 hours ago</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/timestamp.cpp" line="79"/>
        <source>Just now</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/timestamp.cpp" line="83"/>
        <source>In the future</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/timestamp.cpp" line="123"/>
        <source>Yesterday</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/timestamp.cpp" line="127"/>
        <source>%1 days ago</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/timestamp.cpp" line="138"/>
        <source>A month ago</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/timestamp.cpp" line="142"/>
        <source>%1 months ago</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/timestamp.cpp" line="151"/>
        <source>A year ago</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/timestamp.cpp" line="155"/>
        <source>%1 years ago</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UserPosts</name>
    <message>
        <location filename="../src/userposts.cpp" line="37"/>
        <source>Posts by %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/userposts.cpp" line="62"/>
        <source>Loading...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/userposts.cpp" line="64"/>
        <source>&amp;Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/userposts.cpp" line="146"/>
        <source>Received &apos;%1&apos;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/userposts.cpp" line="152"/>
        <source>%1 posts</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
