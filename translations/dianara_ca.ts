<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="ca_ES">
<context>
    <name>ASActivity</name>
    <message>
        <location filename="../src/asactivity.cpp" line="88"/>
        <location filename="../src/asactivity.cpp" line="126"/>
        <source>Public</source>
        <translation>Públic</translation>
    </message>
    <message>
        <location filename="../src/asactivity.cpp" line="398"/>
        <source>%1 by %2</source>
        <comment>1=kind of object: note, comment, etc; 2=author&apos;s name</comment>
        <translation>%1 de %2</translation>
    </message>
</context>
<context>
    <name>ASObject</name>
    <message>
        <location filename="../src/asobject.cpp" line="237"/>
        <source>Note</source>
        <comment>Noun, an object type</comment>
        <translation>Nota</translation>
    </message>
    <message>
        <location filename="../src/asobject.cpp" line="242"/>
        <source>Article</source>
        <comment>Noun, an object type</comment>
        <translation>Article</translation>
    </message>
    <message>
        <location filename="../src/asobject.cpp" line="247"/>
        <source>Image</source>
        <comment>Noun, an object type</comment>
        <translation>Imatge</translation>
    </message>
    <message>
        <location filename="../src/asobject.cpp" line="252"/>
        <source>Audio</source>
        <comment>Noun, an object type</comment>
        <translation>Àudio</translation>
    </message>
    <message>
        <location filename="../src/asobject.cpp" line="257"/>
        <source>Video</source>
        <comment>Noun, an object type</comment>
        <translation>Vídeo</translation>
    </message>
    <message>
        <location filename="../src/asobject.cpp" line="262"/>
        <source>File</source>
        <comment>Noun, an object type</comment>
        <translation>Arxiu</translation>
    </message>
    <message>
        <location filename="../src/asobject.cpp" line="267"/>
        <source>Comment</source>
        <comment>Noun, as in object type: a comment</comment>
        <translation>Comentari</translation>
    </message>
    <message>
        <location filename="../src/asobject.cpp" line="272"/>
        <source>Group</source>
        <comment>Noun, an object type</comment>
        <translation>Grup</translation>
    </message>
    <message>
        <location filename="../src/asobject.cpp" line="277"/>
        <source>Collection</source>
        <comment>Noun, an object type</comment>
        <translation>Col·lecció</translation>
    </message>
    <message>
        <location filename="../src/asobject.cpp" line="282"/>
        <source>Other</source>
        <comment>As in: other type of post</comment>
        <translation>Altre</translation>
    </message>
    <message>
        <location filename="../src/asobject.cpp" line="338"/>
        <source>No detailed location</source>
        <translation>No hi ha ubicació detallada</translation>
    </message>
    <message>
        <location filename="../src/asobject.cpp" line="363"/>
        <source>Deleted on %1</source>
        <translation>Eliminat el %1</translation>
    </message>
    <message>
        <location filename="../src/asobject.cpp" line="524"/>
        <location filename="../src/asobject.cpp" line="594"/>
        <source>and one other</source>
        <translation>i un més</translation>
    </message>
    <message>
        <location filename="../src/asobject.cpp" line="528"/>
        <location filename="../src/asobject.cpp" line="598"/>
        <source>and %1 others</source>
        <translation>i %1 més</translation>
    </message>
</context>
<context>
    <name>ASPerson</name>
    <message>
        <location filename="../src/asperson.cpp" line="146"/>
        <source>Hometown</source>
        <translation>Ciutat</translation>
    </message>
</context>
<context>
    <name>AccountDialog</name>
    <message>
        <location filename="../src/accountdialog.cpp" line="75"/>
        <source>Your Pump.io address:</source>
        <translation>La teva adreça pump.io:</translation>
    </message>
    <message>
        <source>Webfinger ID, like username@pumpserver.org</source>
        <translation type="obsolete">ID Webfinger, tipus usuari@servidorpump.org</translation>
    </message>
    <message>
        <source>Your address, as username@server</source>
        <translation type="obsolete">La teva adreça, com usuari@servidor</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="83"/>
        <source>Get &amp;Verifier Code</source>
        <translation>Obtenir codi de &amp;verificació</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="114"/>
        <source>Verifier code:</source>
        <translation>Codi de verificació:</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="117"/>
        <source>Enter or paste the verifier code provided by your Pump server here</source>
        <translation>Introdueix o enganxa aquí el codi de verificació proporcionat pel teu servidor Pump</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="151"/>
        <source>&amp;Save Details</source>
        <translation>&amp;Guardar dades</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="371"/>
        <source>If the browser doesn&apos;t open automatically, copy this address manually</source>
        <translation>Si el navegador web no s&apos;obre automàticament, copia aquesta adreça manualment</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="25"/>
        <source>Account Configuration</source>
        <translation>Configuració del compte</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="42"/>
        <source>First, enter your Webfinger ID, your pump.io address.</source>
        <translation>Primer, introdueix el teu identificador Webfinger, la teva adreça pump.io.</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="44"/>
        <source>Your address looks like username@pumpserver.org, and you can find it in your profile, in the web interface.</source>
        <translation>La teva adreça es com usuari@servidorpump.org, i pots trobar-la al teu perfil, a la interfície web.</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="48"/>
        <source>If your profile is at https://pump.example/yourname, then your address is yourname@pump.example</source>
        <translation>Si el teu perfil es troba a https://pump.exemple/elteunom, llavors la teva adreça es elteunom@pump.exemple</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="52"/>
        <source>If you don&apos;t have an account yet, you can sign up for one at %1. This link will take you to a random public server.</source>
        <comment>1=link to website</comment>
        <translation>Si encara no tens un compte, pots registrar-ne un a %1. Aquest enllaç et portarà a un servidor públic aleatori.</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="60"/>
        <source>If you need help: %1</source>
        <translation>Si necessites ajuda: %1</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="63"/>
        <source>Pump.io User Guide</source>
        <translation>Guia d&apos;usuari de Pump.io</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="78"/>
        <source>Your address, like username@pumpserver.org</source>
        <translation>La teva adreça, com usuari@servidorpump.org</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="86"/>
        <source>After clicking this button, a web browser will open, requesting authorization for Dianara</source>
        <translation>Quan premis aquest botó, s&apos;obrirà un navegador web, demanant autorització per Dianara</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="96"/>
        <source>Once you have authorized Dianara from your Pump server web interface, you&apos;ll receive a code called VERIFIER.
Copy it and paste it into the field below.</source>
        <comment>Don&apos;t translate the VERIFIER word!</comment>
        <translation>Un cop que hagis autoritzat a Dianara des de la interfície web del teu servidor Pump, rebràs un codi anomenat VERIFIER.
Copia&apos;l i enganxa&apos;l al camp de sota.</translation>
    </message>
    <message>
        <source>Enter the verifier code provided by your Pump server here</source>
        <translation type="obsolete">Introdueix aquí el codi de verificació proporcionat pel teu servidor Pump</translation>
    </message>
    <message>
        <source>Paste the verifier here</source>
        <translation type="obsolete">Enganxa el verificador aquí</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="124"/>
        <source>&amp;Authorize Application</source>
        <translation>&amp;Autoritzar l&apos;aplicació</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="159"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Cancel·lar</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="167"/>
        <source>Your account is properly configured.</source>
        <translation>El teu compte està configurat correctament.</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="171"/>
        <source>Press Unlock if you wish to configure a different account.</source>
        <translation>Prem Desbloquejar si vols configurar un compte diferent.</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="179"/>
        <source>&amp;Unlock</source>
        <translation>&amp;Desbloquejar</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="312"/>
        <source>A web browser will start now, where you can get the verifier code</source>
        <translation>Ara s&apos;iniciarà un navegador web, on podràs obtenir el codi de verificació</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="321"/>
        <source>Your Pump address is invalid</source>
        <translation>La teva adreça Pump no es vàlida</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="340"/>
        <source>Verifier code is empty</source>
        <translation>El codi de verificació està buit</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="352"/>
        <source>Dianara is authorized to access your data</source>
        <translation>Dianara te autorització per accedir al teu compte</translation>
    </message>
</context>
<context>
    <name>AudienceSelector</name>
    <message>
        <location filename="../src/audienceselector.cpp" line="32"/>
        <source>&apos;To&apos; List</source>
        <translation>Llista &apos;Per a&apos;</translation>
    </message>
    <message>
        <source>&apos;CC&apos; List</source>
        <translation type="obsolete">Llista &apos;CC&apos;</translation>
    </message>
    <message>
        <location filename="../src/audienceselector.cpp" line="36"/>
        <source>&apos;Cc&apos; List</source>
        <translation>Llista &apos;Cc&apos;</translation>
    </message>
    <message>
        <location filename="../src/audienceselector.cpp" line="57"/>
        <source>&amp;Add to Selected</source>
        <translation>&amp;Afegir a seleccionats</translation>
    </message>
    <message>
        <location filename="../src/audienceselector.cpp" line="71"/>
        <source>All Contacts</source>
        <translation>Tots els contactes</translation>
    </message>
    <message>
        <location filename="../src/audienceselector.cpp" line="77"/>
        <source>Select people from the list on the left.
You can drag them with the mouse, click or double-click on them, or select them and use the button below.</source>
        <translation>Selecciona gent de la llista de l&apos;esquerra.
Pots arrossegar-los amb el ratolí, fer clic o doble clic en ells, o seleccionar-los i fer servir el botó de sota.</translation>
    </message>
    <message>
        <location filename="../src/audienceselector.cpp" line="92"/>
        <source>Clear &amp;List</source>
        <translation>Esborrar &amp;llista</translation>
    </message>
    <message>
        <location filename="../src/audienceselector.cpp" line="100"/>
        <source>&amp;Done</source>
        <translation>&amp;Fet</translation>
    </message>
    <message>
        <location filename="../src/audienceselector.cpp" line="106"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Cancel·lar</translation>
    </message>
    <message>
        <location filename="../src/audienceselector.cpp" line="126"/>
        <source>Selected People</source>
        <translation>Gent seleccionada</translation>
    </message>
</context>
<context>
    <name>AvatarButton</name>
    <message>
        <location filename="../src/avatarbutton.cpp" line="142"/>
        <source>Open %1&apos;s profile in web browser</source>
        <translation>Obrir el perfil de %1 al navegador web</translation>
    </message>
    <message>
        <location filename="../src/avatarbutton.cpp" line="146"/>
        <source>Open your profile in web browser</source>
        <translation>Obrir el teu perfil al navegador web</translation>
    </message>
    <message>
        <location filename="../src/avatarbutton.cpp" line="165"/>
        <source>Send message to %1</source>
        <translation>Enviar missatge a %1</translation>
    </message>
    <message>
        <location filename="../src/avatarbutton.cpp" line="174"/>
        <source>Browse messages</source>
        <translation>Veure missatges</translation>
    </message>
    <message>
        <location filename="../src/avatarbutton.cpp" line="230"/>
        <source>Stop following</source>
        <translation>Deixar de seguir</translation>
    </message>
    <message>
        <location filename="../src/avatarbutton.cpp" line="241"/>
        <source>Follow</source>
        <translation>Seguir</translation>
    </message>
    <message>
        <location filename="../src/avatarbutton.cpp" line="282"/>
        <source>Stop following?</source>
        <translation>Deixar de seguir?</translation>
    </message>
    <message>
        <location filename="../src/avatarbutton.cpp" line="283"/>
        <source>Are you sure you want to stop following %1?</source>
        <translation>Estàs segur de que vols deixar de seguir a %1?</translation>
    </message>
    <message>
        <location filename="../src/avatarbutton.cpp" line="286"/>
        <source>&amp;Yes, stop following</source>
        <translation>&amp;Sí, deixar de seguir</translation>
    </message>
    <message>
        <location filename="../src/avatarbutton.cpp" line="287"/>
        <source>&amp;No</source>
        <translation>&amp;No</translation>
    </message>
</context>
<context>
    <name>ColorPicker</name>
    <message>
        <location filename="../src/colorpicker.cpp" line="34"/>
        <source>Change</source>
        <translation>Canviar</translation>
    </message>
</context>
<context>
    <name>Comment</name>
    <message>
        <location filename="../src/comment.cpp" line="250"/>
        <source>Posted on %1</source>
        <translation>Publicat el %1</translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="255"/>
        <source>Modified on %1</source>
        <translation>Modificat el %1</translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="113"/>
        <source>Like or unlike this comment</source>
        <translation>Dir que t&apos;agrada o que ja no t&apos;agrada aquest comentari</translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="120"/>
        <source>Quote</source>
        <comment>This is a verb, infinitive</comment>
        <translation>Citar</translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="127"/>
        <source>Reply quoting this comment</source>
        <translation>Respondre citant aquest comentari</translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="135"/>
        <source>Edit</source>
        <translation>Editar</translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="141"/>
        <source>Modify this comment</source>
        <translation>Modificar aquest comentari</translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="147"/>
        <source>Delete</source>
        <translation>Eliminar</translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="153"/>
        <source>Erase this comment</source>
        <translation>Esborrar aquest comentari</translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="294"/>
        <source>Unlike</source>
        <translation>Ja no m&apos;agrada</translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="300"/>
        <source>Like</source>
        <translation>M&apos;agrada</translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="322"/>
        <source>%1 like this comment</source>
        <comment>Plural: %1=list of people like John, Jane, Smith</comment>
        <translation>A %1 els hi agrada aquest comentari</translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="316"/>
        <source>%1 likes this comment</source>
        <comment>Singular: %1=name of just 1 person</comment>
        <translation>A %1 li agrada aquest comentari</translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="542"/>
        <source>WARNING: Delete comment?</source>
        <translation>AVÍS: Eliminar comentari?</translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="543"/>
        <source>Are you sure you want to delete this comment?</source>
        <translation>Estàs segur de que vols eliminar aquest comentari?</translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="544"/>
        <source>&amp;Yes, delete it</source>
        <translation>&amp;Sí, eliminar-ho</translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="544"/>
        <source>&amp;No</source>
        <translation>&amp;No</translation>
    </message>
</context>
<context>
    <name>CommenterBlock</name>
    <message>
        <location filename="../src/commenterblock.cpp" line="124"/>
        <source>You can press Control+Enter to send the comment with the keyboard</source>
        <translation>Pots premer Control+Enter per enviar el comentari amb el teclat</translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation type="obsolete">&amp;Cancel·lar</translation>
    </message>
    <message>
        <location filename="../src/commenterblock.cpp" line="40"/>
        <source>Reload comments</source>
        <translation>Actualitzar comentaris</translation>
    </message>
    <message>
        <location filename="../src/commenterblock.cpp" line="121"/>
        <location filename="../src/commenterblock.cpp" line="462"/>
        <source>Comment</source>
        <comment>Infinitive verb</comment>
        <translation>Comentar</translation>
    </message>
    <message>
        <location filename="../src/commenterblock.cpp" line="131"/>
        <source>Cancel</source>
        <translation>Cancel·lar</translation>
    </message>
    <message>
        <location filename="../src/commenterblock.cpp" line="133"/>
        <source>Press ESC to cancel the comment if there is no text</source>
        <translation>Prem ESC per cancel·lar el comentari si no hi ha text</translation>
    </message>
    <message>
        <location filename="../src/commenterblock.cpp" line="360"/>
        <source>Show all %1 comments</source>
        <translation>Mostrar els %1 comentaris</translation>
    </message>
    <message>
        <location filename="../src/commenterblock.cpp" line="497"/>
        <source>Error: Already composing</source>
        <translation>Error: Ja s&apos;està redactant</translation>
    </message>
    <message>
        <location filename="../src/commenterblock.cpp" line="498"/>
        <source>You can&apos;t edit a comment at this time, because another comment is already being composed.</source>
        <translation>No pots editar un comentari en aquest moment, perque ja s&apos;està redactant un altre comentari.</translation>
    </message>
    <message>
        <location filename="../src/commenterblock.cpp" line="511"/>
        <source>Editing comment</source>
        <translation>Editant comentari</translation>
    </message>
    <message>
        <location filename="../src/commenterblock.cpp" line="566"/>
        <source>Posting comment failed.

Try again.</source>
        <translation>Ha fallat la publicació del comentari.

Torna-ho a provar.</translation>
    </message>
    <message>
        <location filename="../src/commenterblock.cpp" line="596"/>
        <source>Sending comment...</source>
        <translation>Enviant comentari...</translation>
    </message>
    <message>
        <location filename="../src/commenterblock.cpp" line="601"/>
        <source>Updating comment...</source>
        <translation>Actualitzant comentari...</translation>
    </message>
    <message>
        <location filename="../src/commenterblock.cpp" line="610"/>
        <source>Comment is empty.</source>
        <translation>El comentari està buit.</translation>
    </message>
</context>
<context>
    <name>Composer</name>
    <message>
        <location filename="../src/composer.cpp" line="66"/>
        <source>Bold</source>
        <translation>Negreta</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="71"/>
        <source>Italic</source>
        <translation>Cursiva</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="113"/>
        <source>Make a link</source>
        <translation>Fer un enllaç</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="35"/>
        <source>Click here or press Control+N to post a note...</source>
        <translation>Fes clic aquí o prem Control+N per publicar una nota...</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="179"/>
        <source>Type a message here to post it</source>
        <translation>Escriu un missatge aquí per publicar-ho</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="62"/>
        <source>Normal</source>
        <translation>Normal</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="42"/>
        <source>Symbols</source>
        <translation>Símbols</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="60"/>
        <source>Formatting</source>
        <translation>Format</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="76"/>
        <source>Underline</source>
        <translation>Subratllat</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="81"/>
        <source>Strikethrough</source>
        <translation>Barrat</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="88"/>
        <source>Header</source>
        <translation>Títol</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="93"/>
        <source>List</source>
        <translation>Llista</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="97"/>
        <source>Table</source>
        <translation>Taula</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="101"/>
        <source>Preformatted block</source>
        <translation>Bloc preformatat</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="105"/>
        <source>Quote block</source>
        <translation>Bloc de cita</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="118"/>
        <source>Insert an image from a web site</source>
        <translation>Inserir una imatge des d&apos;un lloc web</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="123"/>
        <source>Insert line</source>
        <translation>Inserir línia</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="449"/>
        <source>Insert as image?</source>
        <translation>Inserir com imatge?</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="450"/>
        <source>The link you are pasting seems to point to an image.</source>
        <translation>Sembla que l&apos;enllaç que estàs enganxant apunta a una imatge.</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="452"/>
        <source>Insert as visible image</source>
        <translation>Inserir com imatge visible</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="453"/>
        <source>Insert as link</source>
        <translation>Inserir com enllaç</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="629"/>
        <source>Table Size</source>
        <translation>Mida de la taula</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="633"/>
        <source>How many rows (height)?</source>
        <translation>Quantes files (alçada)?</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="644"/>
        <source>How many columns (width)?</source>
        <translation>Quantes columnes (amplada)?</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="723"/>
        <source>Type or paste a web address here.
You could also select some text first, to turn it into a link.</source>
        <translation>Escriu o enganxa una adreça web aquí.
També pots seleccionar text abans, per convertir-ho en un enllaç.</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="773"/>
        <source>Type or paste the image address here.
The link must point to the image file directly.</source>
        <translation>Escriu o enganxa l&apos;adreça de la imatge aquí.
L&apos;enllaç ha d&apos;apuntar a l&apos;arxiu d&apos;imatge directament.</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="138"/>
        <source>Text Formatting Options</source>
        <translation>Opcions de format de text</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="143"/>
        <source>Paste Text Without Formatting</source>
        <translation>Enganxar text sense format</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="772"/>
        <source>Insert an image from a URL</source>
        <translation>Inserir una imatge des d&apos;una URL</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="789"/>
        <source>Error: Invalid URL</source>
        <translation>Error: URL no vàlida</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="790"/>
        <source>The address you entered (%1) is not valid.
Image addresses should begin with http:// or https://</source>
        <translation>L&apos;adreça que has introduit (%1) no es vàlida.
Les adreces d&apos;imatges han de començar amb http:// o https://</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="881"/>
        <source>Cancel message?</source>
        <translation>Cancel·lar el missatge?</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="882"/>
        <source>Are you sure you want to cancel this message?</source>
        <translation>Segur que vols cancel·lar aquest missatge?</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="883"/>
        <source>&amp;Yes, cancel it</source>
        <translation>&amp;Sí, cancel·lar-ho</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="883"/>
        <source>&amp;No</source>
        <translation>&amp;No</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="722"/>
        <source>Insert a link</source>
        <translation>Inserir un enllaç</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="134"/>
        <source>&amp;Format</source>
        <comment>Button for text formatting and related options</comment>
        <translation>&amp;Format</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="186"/>
        <source>Type a comment here</source>
        <translation>Escriu un comentari aquí</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="735"/>
        <source>Make a link from selected text</source>
        <translation>Fer un link del text seleccionat</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="736"/>
        <source>Type or paste a web address here.
The selected text (%1) will be converted to a link.</source>
        <translation>Escriu o enganxa una adreça web aquí.
El text seleccionat (%1) serà convertit en un enllaç.</translation>
    </message>
</context>
<context>
    <name>ConfigDialog</name>
    <message>
        <location filename="../src/configdialog.cpp" line="68"/>
        <source>minutes</source>
        <translation>minuts</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="82"/>
        <source>Top</source>
        <translation>Part superior</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="84"/>
        <source>Bottom</source>
        <translation>Part inferior</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="86"/>
        <source>Left side</source>
        <translation>Costat esquerre</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="88"/>
        <source>Right side</source>
        <translation>Costat dret</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="32"/>
        <source>Program Configuration</source>
        <translation>Configuració del programa</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="70"/>
        <source>Timeline &amp;update interval</source>
        <translation>Interval d&apos;&amp;actualització de la línia temporal</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="90"/>
        <source>&amp;Tabs position</source>
        <translation>Posició de les &amp;pestanyes</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="96"/>
        <source>&amp;Movable tabs</source>
        <translation>Pes&amp;tanyes mòbils</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="136"/>
        <source>Minor Feeds</source>
        <translation>Línies temporals menors</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="210"/>
        <source>posts</source>
        <comment>Goes after a number, as: 25 posts</comment>
        <translation>missatges</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="214"/>
        <source>&amp;Posts per page, main timeline</source>
        <translation>&amp;Missatges per pàgina, línia temporal principal</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="219"/>
        <source>posts</source>
        <comment>This goes after a number, like: 10 posts</comment>
        <translation>missatges</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="223"/>
        <source>Posts per page, &amp;other timelines</source>
        <translation>Missatges per pàgina, &amp;altres línies temporals</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="325"/>
        <source>Public posts as &amp;default</source>
        <translation>Missatges públics de manera &amp;predeterminada</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="108"/>
        <source>Pro&amp;xy Settings</source>
        <translation>Paràmetres de pro&amp;xy</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="111"/>
        <source>Network configuration</source>
        <translation>Configuració de xarxa</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="116"/>
        <source>Set Up F&amp;ilters</source>
        <translation>Configurar f&amp;iltres</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="119"/>
        <source>Filtering rules</source>
        <translation>Normes de filtrat</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="227"/>
        <source>Highlighted activities, except mine</source>
        <translation>Activitats destacades, excepte les meves</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="228"/>
        <source>Any highlighted activity</source>
        <translation>Qualsevol activitat destacada</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="229"/>
        <source>Always</source>
        <translation>Sempre</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="230"/>
        <source>Never</source>
        <translation>Mai</translation>
    </message>
    <message>
        <source>Show snippets in minor feed</source>
        <translation type="obsolete">Mostrar fragments a la ĺinia temporal menor</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="237"/>
        <source>characters</source>
        <comment>This is a suffix, after a number</comment>
        <translation>caràcters</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="240"/>
        <source>Snippet limit</source>
        <translation>Límit dels fragments</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="130"/>
        <source>Post Titles</source>
        <translation>Títols del missatges</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="132"/>
        <source>Post Contents</source>
        <translation>Contingut dels missatges</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="134"/>
        <source>Comments</source>
        <translation>Comentaris</translation>
    </message>
    <message>
        <source>Minor Feed</source>
        <translation type="obsolete">Línia temporal menor</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="156"/>
        <source>You are among the recipients of the activity, such as a comment addressed to you.</source>
        <translation>Et trobes entre els destinataris de l&apos;activitat, com per exemple un comentari dirigit a tu.</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="160"/>
        <source>Used also when highlighting posts addressed to you in the timelines.</source>
        <translation>Utilitzat també quan es destaquen missatges dirigits a tu a les línies temporals.</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="166"/>
        <source>The activity is in reply to something done by you, such as a comment posted in reply to one of your notes.</source>
        <translation>La activitat es en resposta a alguna cosa feta per tu, com un comentari publicat en resposta a una de les teves notes.</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="173"/>
        <source>You are the object of the activity, such as someone adding you to a list.</source>
        <translation>Ets l&apos;objecte de l&apos;activitat, com per exemple, quan algú t&apos;afegeix a una llista.</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="179"/>
        <source>The activity is related to one of your objects, such as someone liking one of your posts.</source>
        <translation>L&apos;activitat està relacionada amb un dels teus objectes, com per exemple quan a algú li agrada un dels teus missatges.</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="183"/>
        <source>Used also when highlighting your own posts in the timelines.</source>
        <translation>Utilitzat també quan es destaquen els teus propis missatges a les línies temporals.</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="189"/>
        <source>Item highlighted due to filtering rules.</source>
        <translation>Element destacat per normes de filtrat.</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="194"/>
        <source>Item is new.</source>
        <translation>L&apos;element es nou.</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="232"/>
        <source>Show snippets in minor feeds</source>
        <translation>Mostrar fragments a les ĺinies temporals menors</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="253"/>
        <source>Hide duplicated posts</source>
        <translation>Ocultar missatges duplicats</translation>
    </message>
    <message>
        <source>Only for embedded images</source>
        <translation type="obsolete">Només per imatges incrustades...</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="296"/>
        <source>Avatar size</source>
        <translation>Mida dels avatars</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="298"/>
        <source>Show extended share information</source>
        <translation>Mostrar informació adicional als compartits</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="300"/>
        <source>Show extra information</source>
        <translation>Mostrar informació extra</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="302"/>
        <source>Highlight post author&apos;s comments</source>
        <translation>Destacar comentaris de l&apos;autor del missatge</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="304"/>
        <source>Highlight your own comments</source>
        <translation>Destacar els teus propis comentaris</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="306"/>
        <source>Ignore SSL errors in images</source>
        <translation>Ignorar errors d&apos;SSL a les imatges</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="329"/>
        <source>Show character counter</source>
        <translation>Mostrar contador de caràcters</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="344"/>
        <source>Don&apos;t inform followers when following someone</source>
        <translation>No informar als seguidors al seguir a algú</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="346"/>
        <source>Don&apos;t inform followers when handling lists</source>
        <translation>No informar als seguidors al manipular llistes</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="359"/>
        <source>As system notifications</source>
        <translation>Com a notificacions del sistema</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="361"/>
        <source>Using own notifications</source>
        <translation>Fent servir notificacions pròpies</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="363"/>
        <source>Don&apos;t show notifications</source>
        <translation>No mostrar notificacions</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="366"/>
        <source>Notification Style</source>
        <translation>Estil de les notificacions</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="375"/>
        <source>Notify when receiving:</source>
        <translation>Notificar quan es rebin:</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="380"/>
        <source>New posts</source>
        <translation>Missatges nous</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="385"/>
        <source>Highlighted posts</source>
        <translation>Missatges destacats</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="390"/>
        <source>New activities in minor feed</source>
        <translation>Noves activitats a la línia temporal menor</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="395"/>
        <source>Highlighted activities in minor feed</source>
        <translation>Activitats destacades a la línia temporal menor</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="416"/>
        <source>Default</source>
        <translatorcomment>icona, femeni</translatorcomment>
        <translation>Predeterminada</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="417"/>
        <source>System iconset, if available</source>
        <translation>Icona del sistema, si es troba disponible</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="418"/>
        <source>Show your current avatar</source>
        <translation>Mostrar el teu avatar actual</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="419"/>
        <source>Custom icon</source>
        <translation>Icona personalitzada</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="422"/>
        <source>System Tray Icon &amp;Type</source>
        <translation>&amp;Tipus d&apos;icona a la safata del sistema</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="447"/>
        <source>Hide window on startup</source>
        <translation>Ocultar finestra a l&apos;inici</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="474"/>
        <source>General Options</source>
        <translation>Opcions generals</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="477"/>
        <source>Fonts</source>
        <translation>Fonts</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="480"/>
        <source>Colors</source>
        <translation>Colors</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="483"/>
        <source>Timelines</source>
        <translation>Línies temporals</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="486"/>
        <source>Posts</source>
        <translation>Missatges</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="489"/>
        <source>Composer</source>
        <translation>Editor</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="492"/>
        <source>Privacy</source>
        <translation>Privacitat</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="495"/>
        <source>Notifications</source>
        <translation>Notificacions</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="498"/>
        <source>System Tray</source>
        <translation>Safata del sistema</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="425"/>
        <source>S&amp;elect...</source>
        <translation>S&amp;eleccionar...</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="442"/>
        <source>Custom &amp;Icon</source>
        <translation>&amp;Icona personalitzada</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="791"/>
        <source>Select custom icon</source>
        <translation>Selecciona icona personalitzada</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="527"/>
        <source>Dianara stores data in this folder:</source>
        <translation>Dianara emmagatzema dades en aquesta carpeta:</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="258"/>
        <source>Jump to new posts line on update</source>
        <translation>Saltar a la línia de nous missatges quan s&apos;actualitzi</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="288"/>
        <source>Only for images inserted from web sites.</source>
        <translation>Només per imatges inserides des de llocs web.</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="291"/>
        <source>Use with care.</source>
        <translation>Utilitzar amb cura.</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="327"/>
        <source>Use attachment filename as initial post title</source>
        <translation>Utilitzar nom d&apos;arxiu de l&apos;adjunt com a títol inicial del missatge</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="538"/>
        <source>&amp;Save Configuration</source>
        <translation>&amp;Guardar configuració</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="544"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Cancel·lar</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="612"/>
        <source>This is a system notification</source>
        <translation>Això es una notificació del sistema</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="616"/>
        <source>System notifications are not available!</source>
        <translation>Les notificacions del sistema no estan disponibles!</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="618"/>
        <source>Own notifications will be used.</source>
        <translation>Es faran servir les notificacions pròpies.</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="629"/>
        <source>This is a basic notification</source>
        <translation>Això és una notificació bàsica</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="793"/>
        <source>Image files</source>
        <translation>Arxius d&apos;imatge</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="795"/>
        <source>All files</source>
        <translation>Tots els arxius</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="813"/>
        <source>Invalid image</source>
        <translation>Imatge no vàlida</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="814"/>
        <source>The selected image is not valid.</source>
        <translation>La imatge seleccionada no es vàlida.</translation>
    </message>
</context>
<context>
    <name>ContactCard</name>
    <message>
        <location filename="../src/contactcard.cpp" line="73"/>
        <source>Hometown</source>
        <translation>Ciutat</translation>
    </message>
    <message>
        <location filename="../src/contactcard.cpp" line="80"/>
        <source>Joined: %1</source>
        <translation>Es va unir: %1</translation>
    </message>
    <message>
        <location filename="../src/contactcard.cpp" line="88"/>
        <source>Updated: %1</source>
        <translation>Actualitzat: %1</translation>
    </message>
    <message>
        <location filename="../src/contactcard.cpp" line="107"/>
        <source>Bio for %1</source>
        <comment>Abbreviation for Biography, but you can use the full word; %1=contact name</comment>
        <translation>Bio de %1</translation>
    </message>
    <message>
        <location filename="../src/contactcard.cpp" line="120"/>
        <source>This user doesn&apos;t have a biography</source>
        <translation>Aquest usuari no té una biografia</translation>
    </message>
    <message>
        <location filename="../src/contactcard.cpp" line="124"/>
        <source>No biography for %1</source>
        <comment>%1=contact name</comment>
        <translation>No hi ha biografia per %1</translation>
    </message>
    <message>
        <location filename="../src/contactcard.cpp" line="145"/>
        <source>Open Profile in Web Browser</source>
        <translation>Obrir el perfil al navegador web</translation>
    </message>
    <message>
        <location filename="../src/contactcard.cpp" line="152"/>
        <source>Send Message</source>
        <translation>Enviar missatge</translation>
    </message>
    <message>
        <location filename="../src/contactcard.cpp" line="159"/>
        <source>Browse Messages</source>
        <translation>Veure missatges</translation>
    </message>
    <message>
        <location filename="../src/contactcard.cpp" line="165"/>
        <source>In Lists...</source>
        <translation>En llistes...</translation>
    </message>
    <message>
        <location filename="../src/contactcard.cpp" line="182"/>
        <source>User Options</source>
        <translation>Opcions d&apos;usuari</translation>
    </message>
    <message>
        <location filename="../src/contactcard.cpp" line="215"/>
        <source>Follow</source>
        <translation>Seguir</translation>
    </message>
    <message>
        <location filename="../src/contactcard.cpp" line="227"/>
        <source>Stop Following</source>
        <translation>Deixar de seguir</translation>
    </message>
    <message>
        <location filename="../src/contactcard.cpp" line="288"/>
        <source>Stop following?</source>
        <translation>Deixar de seguir?</translation>
    </message>
    <message>
        <location filename="../src/contactcard.cpp" line="289"/>
        <source>Are you sure you want to stop following %1?</source>
        <translation>Estàs segur de que vols deixar de seguir a %1?</translation>
    </message>
    <message>
        <location filename="../src/contactcard.cpp" line="291"/>
        <source>&amp;Yes, stop following</source>
        <translation>&amp;Sí, deixar de seguir</translation>
    </message>
    <message>
        <location filename="../src/contactcard.cpp" line="291"/>
        <source>&amp;No</source>
        <translation>&amp;No</translation>
    </message>
</context>
<context>
    <name>ContactList</name>
    <message>
        <location filename="../src/contactlist.cpp" line="32"/>
        <source>Type a partial name or ID to find a contact...</source>
        <translation>Escriu una part d&apos;un nom o ID per trobar un contacte...</translation>
    </message>
    <message>
        <location filename="../src/contactlist.cpp" line="50"/>
        <source>F&amp;ull List</source>
        <translation>Llista c&amp;ompleta</translation>
    </message>
</context>
<context>
    <name>ContactManager</name>
    <message>
        <location filename="../src/contactmanager.cpp" line="44"/>
        <source>username@server.org or https://server.org/username</source>
        <translation>usuari@servidor.org o https://servidor.org/usuari</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="47"/>
        <source>&amp;Enter address to follow:</source>
        <translation>&amp;Introdueix adreça per seguir:</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="67"/>
        <source>&amp;Follow</source>
        <translation>&amp;Seguir</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="110"/>
        <source>Reload Followers</source>
        <translation>Actualitzar Seguidors</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="115"/>
        <source>Reload Following</source>
        <translation>Actualitzar Seguint</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="121"/>
        <source>Export Followers</source>
        <translation>Exportar Seguidors</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="126"/>
        <source>Export Following</source>
        <translation>Exportar Seguint</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="132"/>
        <source>Reload Lists</source>
        <translation>Actualitzar llistes</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="182"/>
        <source>Follo&amp;wers</source>
        <translation>Se&amp;guidors</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="185"/>
        <source>Followin&amp;g</source>
        <translation>&amp;Seguint</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="188"/>
        <source>&amp;Lists</source>
        <translation>&amp;Llistes</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="203"/>
        <source>Export list of &apos;following&apos; to a file</source>
        <translation>Exportar llista de &apos;seguint&apos; a un arxiu</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="204"/>
        <source>Export list of &apos;followers&apos; to a file</source>
        <translation>Exportar llista de &apos;seguidors&apos; a un arxiu</translation>
    </message>
</context>
<context>
    <name>DownloadWidget</name>
    <message>
        <location filename="../src/downloadwidget.cpp" line="46"/>
        <source>Download</source>
        <translation>Descarregar</translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="47"/>
        <source>Save the attached file to your folders</source>
        <translation>Guardar l&apos;arxiu adjunt a les teves carpetes</translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="59"/>
        <source>Cancel</source>
        <translation>Cancel·lar</translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="116"/>
        <source>Save File As...</source>
        <translation>Guardar arxiu com...</translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="119"/>
        <source>All files</source>
        <translation>Tots els arxius</translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="153"/>
        <source>Abort download?</source>
        <translation>Interrompre la descàrrega?</translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="154"/>
        <source>Do you want to stop downloading the attached file?</source>
        <translation>Vols aturar la descàrrega de l&apos;arxiu adjunt?</translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="155"/>
        <source>&amp;Yes, stop</source>
        <translation>&amp;Sí, aturar</translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="155"/>
        <source>&amp;No, continue</source>
        <translation>&amp;No, continuar</translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="162"/>
        <source>Download aborted</source>
        <translation>Descàrrega interrompuda</translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="176"/>
        <source>Download completed</source>
        <translation>Descàrrega completada</translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="185"/>
        <source>Download failed</source>
        <translation>Ha fallat la descàrrega</translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="205"/>
        <source>Downloading %1 KiB...</source>
        <translation>Descarregant %1 KiB...</translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="207"/>
        <source>%1 KiB downloaded</source>
        <translatorcomment>hmmmmmmmm FIXME</translatorcomment>
        <translation>%1 KiB descarregats</translation>
    </message>
</context>
<context>
    <name>EmailChanger</name>
    <message>
        <location filename="../src/emailchanger.cpp" line="28"/>
        <source>Change E-mail Address</source>
        <translation>Canviar adreça d&apos;e-mail</translation>
    </message>
    <message>
        <location filename="../src/emailchanger.cpp" line="66"/>
        <source>Change</source>
        <translation>Canviar</translation>
    </message>
    <message>
        <location filename="../src/emailchanger.cpp" line="74"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Cancel·lar</translation>
    </message>
    <message>
        <location filename="../src/emailchanger.cpp" line="92"/>
        <source>E-mail Address:</source>
        <translation>Adreça d&apos;e-mail:</translation>
    </message>
    <message>
        <location filename="../src/emailchanger.cpp" line="93"/>
        <source>Again:</source>
        <translation>Una altra vegada:</translation>
    </message>
    <message>
        <source>Repeat:</source>
        <translation type="obsolete">Repeteix:</translation>
    </message>
    <message>
        <location filename="../src/emailchanger.cpp" line="94"/>
        <source>Your Password:</source>
        <translation>La teva contrasenya:</translation>
    </message>
    <message>
        <location filename="../src/emailchanger.cpp" line="149"/>
        <source>E-mail addresses don&apos;t match!</source>
        <translation>Les adreces d&apos;e-mail no coincideixen!</translation>
    </message>
    <message>
        <location filename="../src/emailchanger.cpp" line="157"/>
        <source>Password is empty!</source>
        <translation>La contrasenya està buida!</translation>
    </message>
</context>
<context>
    <name>FilterEditor</name>
    <message>
        <location filename="../src/filtereditor.cpp" line="28"/>
        <source>Filter Editor</source>
        <translation>Editor de filtres</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="75"/>
        <source>Application</source>
        <translation>Aplicació</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="36"/>
        <source>%1 if %2 contains: %3</source>
        <comment>This explains a filter rule, like: Hide if Author ID contains JohnDoe</comment>
        <translation>%1 si %2 conté: %3</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="43"/>
        <source>Here you can set some rules for hiding or highlighting stuff. You can filter by content, author or application.

For instance, you can filter out messages posted by the application Open Farm Game, or which contain the word NSFW in the message. You could also highlight messages that contain your name.</source>
        <translation>Aquí pots establir algunes normes per ocultar o destacar coses. Pots filtrar per contingut, autor o aplicació.

Per exemple, pots filtrar missatges publicats per la aplicació Open Farm Game, o que contenen la paraula NSFW al missatge. També podries destacar missatges que contenen el teu nom.</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="60"/>
        <source>Hide</source>
        <translation>Amagar</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="63"/>
        <source>Highlight</source>
        <translation>Destacar</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="69"/>
        <source>Post Contents</source>
        <translation>Contingut de la publicació</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="72"/>
        <source>Author ID</source>
        <translation>ID de l&apos;autor</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="78"/>
        <source>Activity Description</source>
        <translation>Descripció de l&apos;activitat</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="81"/>
        <source>Keywords...</source>
        <translation>Paraules clau...</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="85"/>
        <source>&amp;Add Filter</source>
        <translation>&amp;Afegir filtre</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="93"/>
        <source>Filters in use</source>
        <translation>Filtres en ús</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="97"/>
        <source>&amp;Remove Selected Filter</source>
        <translation>&amp;Eliminar filtre seleccionat</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="104"/>
        <source>&amp;Save Filters</source>
        <translation>&amp;Guardar filtres</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="110"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Cancel·lar</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="124"/>
        <source>if</source>
        <translation>si</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="128"/>
        <source>contains</source>
        <translation>conté</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="134"/>
        <source>&amp;New Filter</source>
        <translation>&amp;Nou filtre</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="141"/>
        <source>C&amp;urrent Filters</source>
        <translation>Filtres &amp;actuals</translation>
    </message>
</context>
<context>
    <name>FontPicker</name>
    <message>
        <location filename="../src/fontpicker.cpp" line="50"/>
        <source>Change</source>
        <translation>Canviar</translation>
    </message>
</context>
<context>
    <name>HelpWidget</name>
    <message>
        <location filename="../src/helpwidget.cpp" line="26"/>
        <source>Basic Help</source>
        <translation>Ajuda bàsica</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="39"/>
        <source>Getting started</source>
        <translation>Començant</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="70"/>
        <source>The first time you start Dianara, you should see the Account Configuration dialog. There, enter your Pump.io address as name@server and press the Get Verifier Code button.</source>
        <translation>La primera vegada que arrenquis Dianara, hauries de veure la finestra de Configuració del compte. Allà, introdueix la teva adreça de Pump.io com nom@servidor i prem el botó Obtenir codi de verificació.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="75"/>
        <source>Then, your usual web browser should load the authorization page in your Pump.io server. There, you&apos;ll have to copy the full VERIFIER code, and paste it into Dianara&apos;s second field. Then press Authorize Application, and once it&apos;s confirmed, press Save Details.</source>
        <translation>A continuació, el teu navegador web habitual hauria de carregar la pàgina d&apos;autorització al teu servidor Pump.io. Allà, hauràs de copiar el codi &apos;VERIFIER&apos; complet, i enganxar-ho al segon camp de Dianara. Llavors prem &apos;Autoritzar l&apos;aplicació&apos;, i quan sigui confirmat, prem &apos;Guardar dades&apos;.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="81"/>
        <source>At this point, your profile, contact lists and timelines will be loaded.</source>
        <translation>En aquest moment,es carregarà el teu perfil, les llistes de contactes i les línies temporals.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="84"/>
        <source>You should take a look at the Program Configuration window, under the Settings - Configure Dianara menu. There are several interesting options there.</source>
        <translation>Hauries de fer una ullada a la finestra &apos;Configuració del programa&apos;, al menú Configuració - Configurar Dianara. Allà trobaràs diverses opcions interessants.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="40"/>
        <source>Settings</source>
        <translation>Configuració</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="104"/>
        <source>You can configure several things to your liking in the settings, like the time interval between timeline updates, how many posts per page you want, highlight colors, notifications or how the system tray icon looks.</source>
        <translation>Pots ajustar diverses coses al teu gust a la configuració, com l&apos;interval de temps entre actualitzacions de la línia temporal, quants missatges per pàgina vols, colors per destacar missatges, notificacions o quin aspecte tindrà la icona de la safata del sistema.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="41"/>
        <source>Timelines</source>
        <translation>Línies temporals</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="47"/>
        <source>Contents</source>
        <translation>Taula de continguts</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="88"/>
        <source>Keep in mind that there are a lot of places in Dianara where you can get more information by hovering over some text or button with your mouse, and waiting for the tooltip to appear.</source>
        <translation>Recorda que hi ha molts llocs a Dianara on pots obtenir més informació mantenint el ratolí sobre algun text o botó, i esperant a que aparegui l&apos;indicador de funció (tooltip).</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="109"/>
        <source>Here, you can also activate the option to always publish your posts as Public by default. You can always change that at the moment of posting.</source>
        <translation>Aquí, també pots activar l&apos;opció per publicar sempre els teus missatges com públics de forma predeterminada. Sempre pots canviar això en el moment de publicar.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="122"/>
        <source>The main timeline, where you&apos;ll see all the stuff posted or shared by the people you follow.</source>
        <translation>La línia temporal principal, on veuràs tot el que ha publicat o compartit la gent a la que segueixes.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="125"/>
        <source>Messages timeline, where you&apos;ll see messages sent to you specifically. These messages might have been sent to other people too.</source>
        <translation>Línia temporal de missatges, on veuràs missatges enviats a tu específicament. Aquests missatges poden haver estat enviats també a altres persones.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="129"/>
        <source>Activity timeline, where you&apos;ll see your own posts, or posts shared by you.</source>
        <translation>Línia temporal d&apos;activitat, on veuràs els teus propis missatges, o missatges que has compartit.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="132"/>
        <source>Favorites timeline, where you&apos;ll see the posts and comments you&apos;ve liked. This can be used as a bookmark system.</source>
        <translation>Línia temporal de favorits, on veuràs els missatges que t&apos;han agradat. Això es pot fer servir com un sistema d&apos;adreces d&apos;interès.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="137"/>
        <source>The fifth timeline is the minor timeline, also known as the Meanwhile. This is visible on the left side, though it can be hidden. Here you&apos;ll see minor activities done by everyone you follow, such as comment actions, liking posts or following people.</source>
        <translation>La cinquena línia temporal es la línia temporal menor, també coneguda com el &quot;Mentrestant&quot;. És visible a la banda esquerra, encara que es pot amagar. Aquí veuràs activitats secundàries fetes per tothom, com ara accions de comentar, marcar missatges amb &quot;M&apos;agrada&quot; o seguir a gent.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="148"/>
        <source>These activities might have a &apos;+&apos; button in them. Press it to open the post they&apos;re referencing. Also, as in many other places, you can hover with your mouse to see relevant information in the tooltip.</source>
        <translation>Aquestes activitats poden tenir un botó &quot;+&quot;. Prem-ho per obrir el missatge al que fan referència. A més, com a molts altres llocs, pots mantenir el ratolí a sobre per veure informació rellevant a l&apos;indicador de funció (tooltip).</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="42"/>
        <source>Posting</source>
        <translation>Publicant</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="93"/>
        <source>If you&apos;re new to Pump.io, take a look at this guide:</source>
        <translation>Si es la primera vegada que utilitzes Pump.io, fes una ullada a aquesta guia:</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="153"/>
        <source>New messages appear highlighted in a different color. You can mark them as read just by clicking on any empty parts of the message.</source>
        <translation>Els missatges nous es mostren destacats en un color diferent. Els pots marcar com llegits fent clic a qualsevol part buida del missatge.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="163"/>
        <source>You can post notes by clicking in the text field at the top of the window or by pressing Control+N. Setting a title for your post is optional, but highly recommended, as it will help to better identify references to your post in the minor feed, e-mail notifications, etc.</source>
        <translation>Pots publicar notes fent clic al camp de text de la part superior de la finestra o prement Control+N. Afegir un títol al missatge és opcional, però molt recomanable, ja que ajudarà a identificar millor les referències al teu missatge a la línia temporal menor, notificacions per e-mail, etc.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="170"/>
        <source>It is possible to attach images, audio, video, and general files, like PDF documents, to your post.</source>
        <translation>És possible adjuntar imatges, àudio, vídeo i arxius generals, com documents PDF, al teu missatge.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="173"/>
        <source>You can use the Format button to add formatting to your text, like bold or italics. Some of these options require text to be selected before they are used.</source>
        <translation>Pots fer servir el botó Format per afegir format al text, com ara negreta o cursiva. Algunes d&apos;aquestes opcions necessiten que el text sigui seleccionat abans d&apos;utilitzar-les.</translation>
    </message>
    <message>
        <source>You can select who will see your post by using the To and CC buttons.</source>
        <translation type="obsolete">Pots seleccionar qui veurà el teu missatge fent servir els botons &apos;Per a&apos; i &apos;CC&apos;.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="180"/>
        <source>If you add a specific person to the &apos;To&apos; list, they will receive your message in their direct messages tab.</source>
        <translation>Si afegeixes una persona específica a la llista &apos;Per a&apos;, rebrà el teu missatge a la seva pestanya de missatges directes.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="191"/>
        <source>You can create private messages by adding specific people to these lists, and unselecting the Followers or the Public options.</source>
        <translation>Pots fer missatges privats afegint gent específica a aquestes llistes, i desmarcant les opcions Públic i Seguidors.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="43"/>
        <source>Managing contacts</source>
        <translation>Gestionant els contactes</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="201"/>
        <source>You can see the lists of people you follow, and who follow you from the Contacts tab.</source>
        <translation>Pots veure les llistes de gent que segueixes, i que et segueix, a la pestanya Contactes.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="204"/>
        <source>There, you can also manage person lists, used mainly to send posts to specific groups of people.</source>
        <translation>Allà pots gestionar també les llistes de persones, que s&apos;utilitzen principalment per enviar missatges a grups de gent específics.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="211"/>
        <source>You can click on any avatars in the posts, the comments, and the Meanwhile column, and you will get a menu with several options, one of which is following or unfollowing that person.</source>
        <translation>Pots fer clic a qualsevol avatar als missatges, als comentaris, i a la columna &quot;Mentrestant&quot;, i veuràs un menú amb algunes opcions, una de les quals és seguir o deixar de seguir a aquesta persona.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="44"/>
        <source>Keyboard controls</source>
        <translation>Control per teclat</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="95"/>
        <source>Pump.io User Guide</source>
        <translation>Guia d&apos;usuari de Pump.io</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="119"/>
        <source>There are seven timelines:</source>
        <translation>Hi ha set línies temporals:</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="143"/>
        <source>The sixth and seventh timelines are also minor timelines, similar to the Meanwhile, but containing only activities directly addressed to you (Mentions) and activities done by you (Actions).</source>
        <translation>La sisena i la setena línies temporals també son línies temporals menors, semblants al &quot;Mentrestant&quot;, pero contenen només activitats dirigides a tu (Mencions) i activitats fetes per tu (Accions).</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="177"/>
        <source>You can select who will see your post by using the To and Cc buttons.</source>
        <translation>Pots seleccionar qui veurà el teu missatge fent servir els botons &apos;Per a&apos; i &apos;Cc&apos;.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="183"/>
        <source>You can also type &apos;@&apos; and the first characters of the name of a contact to bring up a popup menu with matching choices.</source>
        <translation>També pots teclejar &apos;@&apos; i els primers caràcters del nom d&apos;un contacte per mostrar un menú emergent amb opcions que coincideixin.</translation>
    </message>
    <message>
        <source>Choose one with the arrows and press Enter to complete the name. This will add that person to the recipients list.</source>
        <translation type="obsolete">Escull una amb les tecles de cursor i prem Enter per completar el nom. Això afegirà aquesta persona a la llista de destinataris.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="216"/>
        <source>You can also send a direct message (initially private) to that contact from this menu.</source>
        <translation>També pots enviar un missatge directe (inicialment privat) a aquest contacte des d&apos;aquest menú.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="219"/>
        <source>You can find a list with some Pump.io users and other information here:</source>
        <translation>Pots trobar una llista amb alguns usuaris de Pump.io i altres dades aquí:</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="225"/>
        <source>Users by language</source>
        <translation>Usuaris per idioma</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="234"/>
        <source>The most common actions found on the menus have keyboard shortcuts written next to them, like F5 or Control+N.</source>
        <translation>Les accions més comunes que es troben als menús tenen tecles de drecera escrites al costat, com F5 o Control+N.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="238"/>
        <source>Besides that, you can use:</source>
        <translation>A part d&apos;això, pots utilitzar:</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="241"/>
        <source>Control+Up/Down/PgUp/PgDown/Home/End to move around the timeline.</source>
        <translatorcomment>Hmm....</translatorcomment>
        <translation>Control+Amunt/Avall/RePag/AvPag/Inici/Fi per moure&apos;t per la línia temporal.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="244"/>
        <source>Control+Left/Right to jump one page in the timeline.</source>
        <translation>Control+Esquerra/Dreta per passar una pàgina a la línia temporal.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="247"/>
        <source>Control+G to go to any page in the timeline directly.</source>
        <translation>Control+G per anar a qualsevol pàgina de la línia temporal directament.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="250"/>
        <source>Control+1/2/3 to switch between the minor feeds.</source>
        <translation>Control+1/2/3 per canviar entre les línies temporals menors.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="253"/>
        <source>Control+Enter to post, when you&apos;re done composing a note or a comment. If the note is empty, you can cancel it by pressing ESC.</source>
        <translation>Control+Enter per publicar, quan hagis acabat de redactar una nota o un comentari. Si la nota es troba buida, la pots cancel·lar prement ESC.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="45"/>
        <source>Command line options</source>
        <translation>Opcions de línia d&apos;ordres</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="187"/>
        <source>Choose one with the arrow keys and press Enter to complete the name. This will add that person to the recipients list.</source>
        <translation>Escull una amb les tecles de cursor i prem Enter per completar el nom. Això afegirà aquesta persona a la llista de destinataris.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="207"/>
        <source>There is a text field at the top, where you can directly enter addresses of new contacts to follow them.</source>
        <translation>Hi ha un camp de text a la part superior, on pots introduir directament adreces de contactes nous per seguir-los.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="257"/>
        <source>While composing a note, press Enter to jump from the title to the message body. Also, pressing the Up arrow while you&apos;re at the start of the message, jumps back to the title.</source>
        <translation>Mentre redactes una nota, prem Enter per passar del títol al cos del missatge. A més, prement la fletxa amunt quan siguis al principi del missatge, torna al títol.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="262"/>
        <source>Control+Enter to finish creating a list of recipients for a post, in the &apos;To&apos; or &apos;Cc&apos; lists.</source>
        <translation>Control+Enter per acabar de crear una llista de destinataris per un missatge, a les llistes &apos;Per a&apos; o &apos;Cc&apos;.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="272"/>
        <source>You can use the --config parameter to run the program with a different configuration. This can be useful to use two or more different accounts. You can even run two instances of Dianara at the same time.</source>
        <translation>Pots fer servir el paràmetre --config per executar el programa amb una configuració diferent. Això pot ser útil per utilitzar dos o més comptes diferents. Fins i tot pots executar dues instàncies de Dianara a la vegada.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="277"/>
        <source>Use the --debug parameter to have extra information in your terminal window, about what the program is doing.</source>
        <translation>Fes servir el paràmetre --debug per tenir informació addicional a la finestra de la terminal, sobre el que està fent el programa.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="291"/>
        <source>&amp;Close</source>
        <translation>&amp;Tancar</translation>
    </message>
</context>
<context>
    <name>ImageViewer</name>
    <message>
        <location filename="../src/imageviewer.cpp" line="50"/>
        <source>Image</source>
        <translation>Imatge</translation>
    </message>
    <message>
        <source>Resolution and file size</source>
        <translation type="obsolete">Resolució i mida de l&apos;arxiu</translation>
    </message>
    <message>
        <source>ESC to close, secondary-click for options</source>
        <translation type="obsolete">ESC per tancar, clic secundari per opcions</translation>
    </message>
    <message>
        <location filename="../src/imageviewer.cpp" line="61"/>
        <source>&amp;Save As...</source>
        <translation>&amp;Guardar com...</translation>
    </message>
    <message>
        <location filename="../src/imageviewer.cpp" line="77"/>
        <source>&amp;Restart Animation</source>
        <translation>&amp;Reiniciar animació</translation>
    </message>
    <message>
        <location filename="../src/imageviewer.cpp" line="88"/>
        <source>&amp;Close</source>
        <translation>&amp;Tancar</translation>
    </message>
    <message>
        <location filename="../src/imageviewer.cpp" line="138"/>
        <source>Save Image...</source>
        <translation>Guardar imatge...</translation>
    </message>
    <message>
        <location filename="../src/imageviewer.cpp" line="145"/>
        <source>Close Viewer</source>
        <translation>Tancar visualitzador</translation>
    </message>
    <message>
        <location filename="../src/imageviewer.cpp" line="174"/>
        <source>Save Image As...</source>
        <translation>Guardar imatge com...</translation>
    </message>
    <message>
        <location filename="../src/imageviewer.cpp" line="176"/>
        <source>Image files</source>
        <translation>Arxius d&apos;imatge</translation>
    </message>
    <message>
        <location filename="../src/imageviewer.cpp" line="177"/>
        <source>All files</source>
        <translation>Tots els arxius</translation>
    </message>
    <message>
        <location filename="../src/imageviewer.cpp" line="188"/>
        <source>Error saving image</source>
        <translation>Error guardant la imatge</translation>
    </message>
    <message>
        <location filename="../src/imageviewer.cpp" line="189"/>
        <source>There was a problem while saving %1.

Filename should end in .jpg or .png extensions.</source>
        <translation>Hi ha hagut un problema al guardar %1.

El nom de l&apos;arxiu hauria d&apos;acabar amb l&apos;extensió .jpg o .png.</translation>
    </message>
</context>
<context>
    <name>ListsManager</name>
    <message>
        <location filename="../src/listsmanager.cpp" line="38"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="38"/>
        <source>Members</source>
        <translation>Membres</translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="51"/>
        <source>Add Mem&amp;ber</source>
        <translation>Afegir mem&amp;bre</translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="58"/>
        <source>&amp;Remove Member</source>
        <translation>&amp;Eliminar membre</translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="66"/>
        <source>&amp;Delete Selected List</source>
        <translation>&amp;Esborrar llista seleccionada</translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="73"/>
        <source>Add New &amp;List</source>
        <translation>Afegir nova &amp;llista</translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="90"/>
        <source>Create L&amp;ist</source>
        <translation>Crear ll&amp;ista</translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="97"/>
        <source>&amp;Add to List</source>
        <translation>&amp;Afegir a llista</translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="309"/>
        <source>Are you sure you want to delete %1?</source>
        <comment>1=Name of a person list</comment>
        <translation>Estàs segur de que vols eliminar %1?</translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="441"/>
        <source>Remove person from list?</source>
        <translation>Treure persona de la llista?</translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="442"/>
        <source>Are you sure you want to remove %1 from the %2 list?</source>
        <comment>1=Name of a person, 2=name of a list</comment>
        <translation>Estàs segur de que vols treure a %1 de la llista %2?</translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="447"/>
        <source>&amp;Yes</source>
        <translation>&amp;Si</translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="76"/>
        <source>Type a name for the new list...</source>
        <translation>Escriu un nom per la nova llista...</translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="84"/>
        <source>Type an optional description here</source>
        <translation>Escriu una descripció opcional aquí</translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="308"/>
        <source>WARNING: Delete list?</source>
        <translation>AVÍS: Eliminar llista?</translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="311"/>
        <source>&amp;Yes, delete it</source>
        <translation>&amp;Sí, eliminar-la</translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="311"/>
        <location filename="../src/listsmanager.cpp" line="447"/>
        <source>&amp;No</source>
        <translation>&amp;No</translation>
    </message>
</context>
<context>
    <name>LogViewer</name>
    <message>
        <location filename="../src/logviewer.cpp" line="25"/>
        <source>Log</source>
        <translation>Registre</translation>
    </message>
    <message>
        <location filename="../src/logviewer.cpp" line="54"/>
        <source>Clear &amp;Log</source>
        <translation>Esborrar el &amp;registre</translation>
    </message>
    <message>
        <location filename="../src/logviewer.cpp" line="60"/>
        <source>&amp;Close</source>
        <translation>&amp;Tancar</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../src/mainwindow.cpp" line="2359"/>
        <source>&amp;Messages</source>
        <translation>&amp;Missatges</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="395"/>
        <source>&amp;Contacts</source>
        <translation>&amp;Contactes</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="988"/>
        <source>&amp;Quit</source>
        <translation>&amp;Sortir</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="855"/>
        <source>&amp;Session</source>
        <translation>&amp;Sessió</translation>
    </message>
    <message>
        <source>Meanwhile...</source>
        <translation type="obsolete">Mentrestant...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1000"/>
        <source>Side &amp;Panel</source>
        <translation>Cuadre &amp;lateral</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1020"/>
        <source>Status &amp;Bar</source>
        <translation>&amp;Barra d&apos;estat</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2352"/>
        <source>&amp;Timeline</source>
        <translation>Línia &amp;temporal</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2366"/>
        <source>&amp;Activity</source>
        <translation>&amp;Activitat</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="684"/>
        <location filename="../src/mainwindow.cpp" line="1268"/>
        <source>Initializing...</source>
        <translation>Inicialitzant...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="723"/>
        <source>Your account is not configured yet.</source>
        <translation>El teu compte no està configurat encara.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="707"/>
        <source>Dianara started.</source>
        <translation>Dianara iniciat.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="155"/>
        <source>Minor activities done by everyone, such as replying to posts</source>
        <translation>Activitats menors fetes per tothom, com per exemple respostes a missatges</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="174"/>
        <source>Minor activities addressed to you</source>
        <translation>Activitats menors dirigides a tu</translation>
    </message>
    <message>
        <source>Actions</source>
        <translation type="obsolete">Accions</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="190"/>
        <source>Minor activities done by you</source>
        <translation>Activitats menors fetes per tu</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="398"/>
        <source>The people you follow, the ones who follow you, and your person lists</source>
        <translation>La gent a la que segueixes, la que et segueix, i les teves llistes de persones</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="708"/>
        <source>Running with Qt v%1.</source>
        <translation>Funcionant amb Qt v%1.</translation>
    </message>
    <message>
        <source>&amp;Update Timeline</source>
        <translation type="obsolete">Act&amp;ualitzar línia temporal</translation>
    </message>
    <message>
        <source>Update &amp;Messages</source>
        <translation type="obsolete">Actualitzar &amp;missatges</translation>
    </message>
    <message>
        <source>Update &amp;Activity</source>
        <translation type="obsolete">Actualitzar &amp;activitat</translation>
    </message>
    <message>
        <source>Update Fa&amp;vorites</source>
        <translation type="obsolete">Actualitzar &amp;favorits</translation>
    </message>
    <message>
        <source>Update Mentions</source>
        <translation type="obsolete">Actualitzar mencions</translation>
    </message>
    <message>
        <source>Update Actions</source>
        <translation type="obsolete">Actualitzar accions</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="953"/>
        <source>Auto-update &amp;Timelines</source>
        <translation>Auto-actualitzar línies &amp;temporals</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="963"/>
        <source>Mark All as Read</source>
        <translation>Marcar tot com a llegit</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="974"/>
        <source>&amp;Post a Note</source>
        <translation>&amp;Publicar una nota</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="997"/>
        <source>&amp;View</source>
        <translation>&amp;Veure</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1011"/>
        <source>&amp;Toolbar</source>
        <translation>&amp;Barra d&apos;eines</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1032"/>
        <source>Full &amp;Screen</source>
        <translation>&amp;Pantalla completa</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1043"/>
        <source>&amp;Log</source>
        <translation>&amp;Registre</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1055"/>
        <source>S&amp;ettings</source>
        <translation>&amp;Configuració</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1059"/>
        <source>Edit &amp;Profile</source>
        <translation>Editar &amp;perfil</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1067"/>
        <source>&amp;Account</source>
        <translation>&amp;Compte</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1084"/>
        <source>&amp;Configure Dianara</source>
        <translation>&amp;Configurar Dianara</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1094"/>
        <source>&amp;Help</source>
        <translation>Aj&amp;uda</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1098"/>
        <source>Basic &amp;Help</source>
        <translation>&amp;Ajuda bàsica</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1110"/>
        <source>Visit &amp;Website</source>
        <translation>Visitar lloc &amp;web</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1117"/>
        <source>Report a &amp;Bug</source>
        <translation>Informar d&apos;un &amp;error</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1128"/>
        <source>Pump.io User &amp;Guide</source>
        <translation>&amp;Guia d&apos;usuari de Pump.io</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1142"/>
        <source>List of Some Pump.io &amp;Users</source>
        <translation>Llista d&apos;alguns &amp;usuaris de Pump.io</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1149"/>
        <source>Pump.io &amp;Network Status Website</source>
        <translation>Web de l&apos;estat de la xarxa Pump.io</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1159"/>
        <source>About &amp;Dianara</source>
        <translation>Sobre &amp;Dianara</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1206"/>
        <source>Toolbar</source>
        <translation>Barra d&apos;eines</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1252"/>
        <source>Open the log viewer</source>
        <translation>Obrir el visualitzador del registre</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1275"/>
        <source>Auto-updating enabled</source>
        <translation>Auto-actualitzacions activades</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1282"/>
        <source>Auto-updating disabled</source>
        <translation>Auto-actualitzacions desactivades</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1799"/>
        <source>Proxy password required</source>
        <translation>Contrasenya de proxy necessària</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1800"/>
        <source>You have configured a proxy server with authentication, but the password is not set.</source>
        <translation>Has configurat un servidor proxy amb autenticació, pero la contrasenya no està definida.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1805"/>
        <source>Enter the password for your proxy server:</source>
        <translation>Introdueix la contrasenya pel teu servidor proxy:</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1878"/>
        <source>Your biography is empty</source>
        <translation>La teva biografia està buida</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1913"/>
        <source>Click to edit your profile</source>
        <translation>Fes clic per editar el teu perfil</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1938"/>
        <source>Starting automatic update of timelines, once every %1 minutes.</source>
        <translation>Iniciant actualització automàtica de línies temporals, una vegada cada %1 minuts.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1945"/>
        <source>Stopping automatic update of timelines.</source>
        <translation>Aturant actualització automàtica de línies temporals.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2123"/>
        <source>Received %1 older posts in &apos;%2&apos;.</source>
        <comment>%1 is a number, %2 = name of a timeline</comment>
        <translation>S&apos;han rebut %1 missatges anteriors a &apos;%2&apos;.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2174"/>
        <source>1 more pending to receive.</source>
        <comment>singular, one post</comment>
        <translation>1 més pendent de rebre.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2179"/>
        <source>%1 more pending to receive.</source>
        <comment>plural, several posts</comment>
        <translation>%1 més pendents de rebre.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2191"/>
        <location filename="../src/mainwindow.cpp" line="2576"/>
        <source>Also:</source>
        <translation>A més:</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2299"/>
        <source>Last update: %1</source>
        <translation>Última actualització: %1</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2559"/>
        <source>1 more pending to receive.</source>
        <comment>singular, 1 activity</comment>
        <translation>1 més pendent de rebre.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2564"/>
        <source>%1 more pending to receive.</source>
        <comment>plural, several activities</comment>
        <translation>%1 més pendents de rebre.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2130"/>
        <location filename="../src/mainwindow.cpp" line="2514"/>
        <source>&apos;%1&apos; updated.</source>
        <comment>%1 is the name of a feed</comment>
        <translation>&apos;%1&apos; actualitzada.</translation>
    </message>
    <message>
        <source>1 pending to receive.</source>
        <comment>singular, one post</comment>
        <translation type="obsolete">1 pendent de rebre.</translation>
    </message>
    <message>
        <source>%1 pending to receive.</source>
        <comment>plural, several posts</comment>
        <translation type="obsolete">%1 pendents de rebre.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2156"/>
        <source>1 highlighted.</source>
        <comment>singular, refers to a post</comment>
        <translation>1 destacat.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2161"/>
        <source>%1 highlighted.</source>
        <comment>plural, refers to posts</comment>
        <translation>%1 destacats.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2198"/>
        <source>1 filtered out.</source>
        <comment>singular, refers to a post</comment>
        <translation>1 filtrat.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2203"/>
        <source>%1 filtered out.</source>
        <comment>plural, refers to posts</comment>
        <translation>%1 filtrats.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2216"/>
        <source>1 deleted.</source>
        <comment>singular, refers to a post</comment>
        <translation>1 eliminat.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2221"/>
        <source>%1 deleted.</source>
        <comment>plural, refers to posts</comment>
        <translation>%1 eliminats.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2270"/>
        <source>No new posts.</source>
        <translation>No hi ha missatges nous.</translation>
    </message>
    <message>
        <source>Mentions</source>
        <translation type="obsolete">Mencions</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2526"/>
        <source>There is 1 new activity.</source>
        <translation>Hi ha 1 activitat nova.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2530"/>
        <source>There are %1 new activities.</source>
        <translation>Hi han %1 activitats noves.</translation>
    </message>
    <message>
        <source>1 pending to receive.</source>
        <comment>singular, 1 activity</comment>
        <translation type="obsolete">1 pendent de rebre.</translation>
    </message>
    <message>
        <source>%1 pending to receive.</source>
        <comment>plural, several activities</comment>
        <translation type="obsolete">%1 pendents de rebre.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2541"/>
        <source>1 highlighted.</source>
        <comment>singular, refers to an activity</comment>
        <translation>1 destacada.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2546"/>
        <source>%1 highlighted.</source>
        <comment>plural, refers to activities</comment>
        <translation>%1 destacades.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2580"/>
        <source>1 filtered out.</source>
        <comment>singular, refers to one activity</comment>
        <translation>1 filtrada.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2585"/>
        <source>%1 filtered out.</source>
        <comment>plural, several activities</comment>
        <translation>%1 filtrades.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2640"/>
        <source>No new activities.</source>
        <translation>No hi ha activitats noves.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2713"/>
        <source>Error storing image!</source>
        <translation>Error emmagatzemant la imatge!</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2715"/>
        <source>%1 bytes</source>
        <translation>%1 bytes</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2757"/>
        <source>Link to: %1</source>
        <translation>Enllaç a: %1</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3045"/>
        <source>With Dianara you can see your timelines, create new posts, upload pictures and other media, interact with posts, manage your contacts and follow new people.</source>
        <translation>Amb Dianara pots veure les teves línies temporals, crear nous missatges, penjar fotos i multimèdia, interactuar amb els missatges, gestionar els teus contactes i seguir gent nova.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3054"/>
        <source>English translation by JanKusanagi.</source>
        <comment>TRANSLATORS: Change this with your language and name. If there was another translator before you, add your name after theirs ;)</comment>
        <translation>Traducció al català per JanKusanagi.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3102"/>
        <source>&amp;Hide Window</source>
        <translation>&amp;Ocultar finestra</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3112"/>
        <source>&amp;Show Window</source>
        <translation>&amp;Mostrar finestra</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3134"/>
        <source>Closing due to environment shutting down...</source>
        <translation>Tancant a causa de que l&apos;entorn s&apos;està aturant...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3157"/>
        <location filename="../src/mainwindow.cpp" line="3242"/>
        <source>Quit?</source>
        <translation>Sortir?</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3158"/>
        <source>You are composing a note or a comment.</source>
        <translation>Estàs redactant una nota o un comentari.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3160"/>
        <source>Do you really want to close Dianara?</source>
        <translation>Realment vols tancar Dianara?</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3161"/>
        <location filename="../src/mainwindow.cpp" line="3250"/>
        <source>&amp;Yes, close the program</source>
        <translation>&amp;Sí, tancar el programa</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3161"/>
        <location filename="../src/mainwindow.cpp" line="3250"/>
        <source>&amp;No</source>
        <translation>&amp;No</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3184"/>
        <source>Shutting down Dianara...</source>
        <translation>Tancant Dianara...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3243"/>
        <source>System tray icon is not available.</source>
        <translation>La icona de la safata del sistema no està disponible.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3245"/>
        <source>Dianara cannot be hidden in the system tray.</source>
        <translation>Dianara no es pot ocultar a la safata del sistema.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3248"/>
        <source>Do you want to close the program completely?</source>
        <translation>Vols tancar el programa completament?</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2142"/>
        <source>There is 1 new post.</source>
        <translation>Hi ha 1 missatge nou.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2146"/>
        <source>There are %1 new posts.</source>
        <translation>Hi han %1 missatges nous.</translation>
    </message>
    <message>
        <source>Timeline updated.</source>
        <translation type="obsolete">Línia temporal actualitzada.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2136"/>
        <source>Timeline updated at %1.</source>
        <translation>Línia temporal actualitzada a les %1.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2347"/>
        <source>Total posts: %1</source>
        <translation>Total de missatges: %1</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2429"/>
        <source>Your Pump.io account is not configured</source>
        <translation>El teu compte Pump.io no està configurat</translation>
    </message>
    <message>
        <source>Received %1 older items in &apos;%2&apos;.</source>
        <comment>%1 is a number, %2 = name of feed</comment>
        <translation type="obsolete">S&apos;han rebut %1 elements anteriors a &apos;%2&apos;.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3042"/>
        <source>Dianara is a pump.io social networking client.</source>
        <translation>Dianara es un client de xarxa social per pump.io.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3061"/>
        <source>Thanks to all the testers, translators and packagers, who help make Dianara better!</source>
        <translation>Gràcies a tots els &apos;testers&apos;, traductors i empaquetadors, que ajuden a fer Dianara millor!</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3068"/>
        <source>Dianara is licensed under the GNU GPL license, and uses some Oxygen icons: http://www.oxygen-icons.org/ (LGPL licensed)</source>
        <translation>Dianara es troba llicenciat sota la llicència GNU GPL, i utilitza algunes icones Oxygen: http://www.oxygen-icons.org/ (Llicència LGPL)</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2353"/>
        <source>The main timeline</source>
        <translation>La línia temporal principal</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="669"/>
        <source>Press F1 for help</source>
        <translation>Prem F1 per veure l&apos;ajuda</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="729"/>
        <source>Click here to configure your account</source>
        <translation>Fes clic aquí per configurar el teu compte</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="858"/>
        <source>Update %1</source>
        <translation>Actualitzar %1</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2367"/>
        <source>Your own posts</source>
        <translation>Els teus propis missatges</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2374"/>
        <source>Your favorited posts</source>
        <translation>Els missatges que t&apos;agraden</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2360"/>
        <source>Messages sent explicitly to you</source>
        <translation>Missatges enviats explícitament a tu</translation>
    </message>
    <message>
        <source>Update Minor &amp;Feed</source>
        <translatorcomment>meh...</translatorcomment>
        <translation type="obsolete">Actualitzar línia temporal meno&amp;r</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1076"/>
        <source>&amp;Filters and Highlighting</source>
        <translatorcomment>hmmm</translatorcomment>
        <translation>&amp;Filtres i destacats</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1135"/>
        <source>Some Pump.io &amp;Tips</source>
        <translation>Alguns &amp;consells sobre Pump.io</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2373"/>
        <source>Favor&amp;ites</source>
        <translation>Fa&amp;vorits</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2506"/>
        <source>Received %1 older activities in &apos;%2&apos;.</source>
        <comment>%1 is a number, %2 = name of feed</comment>
        <translation>S&apos;han rebut %1 activitats anteriors a &apos;%2&apos;.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2519"/>
        <source>Minor feed updated at %1.</source>
        <translation>Línia temporal menor actualitzada a les %1.</translation>
    </message>
    <message>
        <source>Minor feed updated.</source>
        <translation type="obsolete">Línia temporal menor actualitzada.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3032"/>
        <source>About Dianara</source>
        <translation>Sobre Dianara</translation>
    </message>
</context>
<context>
    <name>MinorFeed</name>
    <message>
        <location filename="../src/minorfeed.cpp" line="64"/>
        <source>Older Activities</source>
        <translation>Activitats anteriors</translation>
    </message>
    <message>
        <location filename="../src/minorfeed.cpp" line="67"/>
        <source>Get previous minor activities</source>
        <translation>Obtenir activitats menors anteriors</translation>
    </message>
    <message>
        <location filename="../src/minorfeed.cpp" line="94"/>
        <source>There are no activities to show yet.</source>
        <translation>Encara no hi ha activitats per mostrar.</translation>
    </message>
    <message>
        <location filename="../src/minorfeed.cpp" line="466"/>
        <source>Get %1 newer</source>
        <comment>As in: Get 3 newer (activities)</comment>
        <translation>Rebre %1 més noves</translation>
    </message>
</context>
<context>
    <name>MinorFeedItem</name>
    <message>
        <location filename="../src/minorfeeditem.cpp" line="67"/>
        <source>Using %1</source>
        <comment>Application used to generate this activity</comment>
        <translation>Utilitzant %1</translation>
    </message>
    <message>
        <location filename="../src/minorfeeditem.cpp" line="81"/>
        <source>To: %1</source>
        <comment>1=people to whom this activity was sent</comment>
        <translation>Per a: %1</translation>
    </message>
    <message>
        <location filename="../src/minorfeeditem.cpp" line="88"/>
        <source>Cc: %1</source>
        <comment>1=people to whom this activity was sent as CC</comment>
        <translation>Cc: %1</translation>
    </message>
    <message>
        <source>CC: %1</source>
        <comment>1=people to whom this activity was sent as CC</comment>
        <translation type="obsolete">CC: %1</translation>
    </message>
    <message>
        <location filename="../src/minorfeeditem.cpp" line="257"/>
        <source>Open referenced post</source>
        <translation>Obrir el missatge referenciat</translation>
    </message>
</context>
<context>
    <name>MiscHelpers</name>
    <message>
        <location filename="../src/mischelpers.cpp" line="238"/>
        <source>bytes</source>
        <translation>bytes</translation>
    </message>
</context>
<context>
    <name>PageSelector</name>
    <message>
        <location filename="../src/pageselector.cpp" line="26"/>
        <source>Jump to page</source>
        <translation>Saltar a pàgina</translation>
    </message>
    <message>
        <location filename="../src/pageselector.cpp" line="34"/>
        <source>Page number:</source>
        <translation>Número de pàgina:</translation>
    </message>
    <message>
        <location filename="../src/pageselector.cpp" line="48"/>
        <source>&amp;First</source>
        <comment>As in: first page</comment>
        <translation>&amp;Primera</translation>
    </message>
    <message>
        <location filename="../src/pageselector.cpp" line="55"/>
        <source>&amp;Last</source>
        <comment>As in: last page</comment>
        <translation>Últim&amp;a</translation>
    </message>
    <message>
        <location filename="../src/pageselector.cpp" line="62"/>
        <source>Newer</source>
        <comment>As in: newer pages</comment>
        <translation>Més noves</translation>
    </message>
    <message>
        <location filename="../src/pageselector.cpp" line="78"/>
        <source>Older</source>
        <comment>As in: older pages</comment>
        <translation>Més antigues</translation>
    </message>
    <message>
        <source>&amp;First</source>
        <translation type="obsolete">&amp;Primera</translation>
    </message>
    <message>
        <source>&amp;Last</source>
        <translation type="obsolete">Ultim&amp;a</translation>
    </message>
    <message>
        <source>Newer</source>
        <translation type="obsolete">Més noves</translation>
    </message>
    <message>
        <source>Older</source>
        <translation type="obsolete">Més antigues</translation>
    </message>
    <message>
        <location filename="../src/pageselector.cpp" line="86"/>
        <source>&amp;Go</source>
        <translation>&amp;Anar</translation>
    </message>
    <message>
        <source>Go to &amp;last page</source>
        <translation type="obsolete">Anar a l&apos;última &amp;pàgina</translation>
    </message>
    <message>
        <location filename="../src/pageselector.cpp" line="94"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Cancel·lar</translation>
    </message>
</context>
<context>
    <name>PeopleWidget</name>
    <message>
        <location filename="../src/peoplewidget.cpp" line="40"/>
        <source>&amp;Search:</source>
        <translation>Ce&amp;rcar:</translation>
    </message>
    <message>
        <location filename="../src/peoplewidget.cpp" line="43"/>
        <source>Enter a name here to search for it</source>
        <translation>Escriu aquí un nom per buscar-ho</translation>
    </message>
    <message>
        <location filename="../src/peoplewidget.cpp" line="95"/>
        <source>Add a contact to a list</source>
        <translation>Afegir un contacte a una llista</translation>
    </message>
    <message>
        <location filename="../src/peoplewidget.cpp" line="105"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Cancel·lar</translation>
    </message>
</context>
<context>
    <name>Post</name>
    <message>
        <location filename="../src/post.cpp" line="1698"/>
        <source>Like this post</source>
        <translation>Dir que t&apos;agrada aquest missatge</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1699"/>
        <source>Like</source>
        <translation>M&apos;agrada</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="252"/>
        <source>Shared on %1</source>
        <translation>Compartit el %1</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="343"/>
        <location filename="../src/post.cpp" line="526"/>
        <source>&amp;Close</source>
        <translation>&amp;Tancar</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="958"/>
        <source>In</source>
        <translation>A</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="208"/>
        <location filename="../src/post.cpp" line="421"/>
        <source>To</source>
        <translation>Per a</translation>
    </message>
    <message>
        <source>CC</source>
        <translation type="obsolete">CC</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="256"/>
        <location filename="../src/post.cpp" line="910"/>
        <source>Using %1</source>
        <comment>1=Program used for posting or sharing</comment>
        <translation>Utilitzant %1</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="505"/>
        <source>Parent</source>
        <comment>As in &apos;Open the parent post&apos;. Try to use the shortest word!</comment>
        <translation>Pare</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="513"/>
        <source>Open the parent post, to which this one replies</source>
        <translation>Obrir el missatge pare, al que aquest respon</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="675"/>
        <source>Modify this post</source>
        <translation>Modificar aquest missatge</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="749"/>
        <source>Join Group</source>
        <translation>Unir-se al grup</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="754"/>
        <source>%1 members in the group</source>
        <translation>%1 membres al grup</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1332"/>
        <source>1 like</source>
        <translation>Li agrada a 1</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1401"/>
        <source>1 comment</source>
        <translation>1 comentari</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1485"/>
        <source>Shared %1 times</source>
        <translation>Compartit %1 cops</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="649"/>
        <source>Share</source>
        <translation>Compartir</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="37"/>
        <source>Click to download the attachment</source>
        <translation>Fes clic per descarregar l&apos;arxiu adjunt</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="142"/>
        <source>Post</source>
        <comment>Noun, not verb</comment>
        <translation>Missatge</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="903"/>
        <source>Type</source>
        <comment>As in: type of object</comment>
        <translation>Tipus</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="917"/>
        <source>Modified on %1</source>
        <translation>Modificat el %1</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="673"/>
        <source>Edit</source>
        <translation>Editar</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1071"/>
        <source>Image is animated. Click on it to play.</source>
        <translation>La imatge és animada. Fes clic per reproduir-la.</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1098"/>
        <source>Loading image...</source>
        <translation>Carregant imatge...</translation>
    </message>
    <message>
        <source>Attached file</source>
        <translation type="obsolete">Arxiu adjunt</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1337"/>
        <source>%1 likes</source>
        <translation>Li agrada a %1</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1406"/>
        <source>%1 comments</source>
        <translation>%1 comentaris</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="684"/>
        <source>Delete</source>
        <translation>Eliminar</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="199"/>
        <source>Via %1</source>
        <translatorcomment>Meh...</translatorcomment>
        <translation>A través de %1</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1613"/>
        <source>Edited: %1</source>
        <translation>Editat: %1</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="901"/>
        <source>Posted on %1</source>
        <comment>1=Date</comment>
        <translation>Publicat el %1</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="630"/>
        <source>If you select some text, it will be quoted.</source>
        <translation>Si selecciones part del text, serà citat.</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="657"/>
        <source>Unshare</source>
        <translation>Deixar de compartir</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="659"/>
        <source>Unshare this post</source>
        <translation>Deixar de compartir aquest missatge</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="302"/>
        <source>Open post in web browser</source>
        <translation>Obrir el missatge al navegador web</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="214"/>
        <location filename="../src/post.cpp" line="437"/>
        <source>Cc</source>
        <translation>Cc</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="311"/>
        <source>Copy post link to clipboard</source>
        <translatorcomment>portapapers?</translatorcomment>
        <translation>Copiar l&apos;enllaç del missatge al porta-retalls</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="330"/>
        <source>Normalize text colors</source>
        <translation>Normalitzar colors del text</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="626"/>
        <source>Comment</source>
        <comment>verb, for the comment button</comment>
        <translation>Comentar</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="628"/>
        <source>Reply to this post.</source>
        <translation>Respondre a aquest missatge.</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="651"/>
        <source>Share this post with your contacts</source>
        <translation>Compartir aquest missatge amb els teus contactes</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="686"/>
        <source>Erase this post</source>
        <translation>Esborrar aquest missatge</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1093"/>
        <source>Couldn&apos;t load image!</source>
        <translation>No s&apos;ha pogut carregar la imatge!</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1116"/>
        <source>Attached Audio</source>
        <translation>Àudio adjunt</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1126"/>
        <source>Attached Video</source>
        <translation>Vídeo adjunt</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1136"/>
        <source>Attached File</source>
        <translation>Arxiu adjunt</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1306"/>
        <source>%1 likes this</source>
        <comment>One person</comment>
        <translation>Li agrada a %1</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1311"/>
        <source>%1 like this</source>
        <comment>More than one person</comment>
        <translation>Els hi agrada a %1</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1446"/>
        <source>%1 shared this</source>
        <comment>%1 = One person name</comment>
        <translation>%1 ha compartit això</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1451"/>
        <source>%1 shared this</source>
        <comment>%1 = Names for more than one person</comment>
        <translation>%1 han compartit això</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1480"/>
        <source>Shared once</source>
        <translation>Compartit un cop</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1691"/>
        <source>You like this</source>
        <translation>T&apos;agrada això</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1692"/>
        <source>Unlike</source>
        <translation>Ja no m&apos;agrada</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1820"/>
        <source>Share post?</source>
        <translation>Compartir missatge?</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1821"/>
        <source>Do you want to share %1&apos;s post?</source>
        <translation>Vols compartir el missatge de %1?</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1823"/>
        <source>&amp;Yes, share it</source>
        <translation>&amp;Sí, compartir-ho</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1823"/>
        <location filename="../src/post.cpp" line="1842"/>
        <location filename="../src/post.cpp" line="1886"/>
        <source>&amp;No</source>
        <translation>&amp;No</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1839"/>
        <source>Unshare post?</source>
        <translation>Deixar de compartir el missatge?</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1840"/>
        <source>Do you want to unshare %1&apos;s post?</source>
        <translation>Vols deixar de compartir el missatge de %1?</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1842"/>
        <source>&amp;Yes, unshare it</source>
        <translation>&amp;Sí, deixar de compartir-ho</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1883"/>
        <source>WARNING: Delete post?</source>
        <translatorcomment>hmm... advertència?</translatorcomment>
        <translation>AVÍS: Eliminar missatge?</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1884"/>
        <source>Are you sure you want to delete this post?</source>
        <translation>Estàs segur de que vols eliminar aquest missatge?</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1886"/>
        <source>&amp;Yes, delete it</source>
        <translation>&amp;Sí, eliminar-ho</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="36"/>
        <source>Click the image to see it in full size</source>
        <translation>Fes clic a la imatge per veure-la a mida completa</translation>
    </message>
</context>
<context>
    <name>ProfileEditor</name>
    <message>
        <location filename="../src/profileeditor.cpp" line="27"/>
        <source>Profile Editor</source>
        <translation>Editor de perfil</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="44"/>
        <source>This is your Pump address</source>
        <translation>Aquesta es la teva adreça Pump</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="46"/>
        <source>This is the e-mail address associated with your account, for things such as notifications and password recovery</source>
        <translation>Aquesta és l&apos;adreça d&apos;e-mail associada al teu compte, per coses com ara notificacions i recuperació de la contrasenya</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="61"/>
        <source>Change &amp;E-mail...</source>
        <translation>Canviar &amp;e-mail...</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="73"/>
        <source>Change &amp;Avatar...</source>
        <translation>Canviar &amp;avatar...</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="83"/>
        <source>This is your visible name</source>
        <translation>Aquest es el teu nom visible</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="102"/>
        <source>&amp;Save Profile</source>
        <translation>&amp;Guardar perfil</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="109"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Cancel·lar</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="136"/>
        <source>Webfinger ID</source>
        <translation>ID Webfinger</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="137"/>
        <source>E-mail</source>
        <translation>E-mail</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="139"/>
        <source>Avatar</source>
        <translation>Avatar</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="141"/>
        <source>Full &amp;Name</source>
        <translation>&amp;Nom complet</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="142"/>
        <source>&amp;Hometown</source>
        <translation>Ciuta&amp;t</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="143"/>
        <source>&amp;Bio</source>
        <translation>&amp;Bio</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="186"/>
        <source>Not set</source>
        <comment>In reference to the e-mail not being set for the account</comment>
        <translation>Sense definir</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="243"/>
        <source>Select avatar image</source>
        <translation>Selecciona imatge d&apos;avatar</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="245"/>
        <source>Image files</source>
        <translation>Arxius d&apos;imatge</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="247"/>
        <source>All files</source>
        <translation>Tots els arxius</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="270"/>
        <source>Invalid image</source>
        <translation>Imatge no vàlida</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="271"/>
        <source>The selected image is not valid.</source>
        <translation>La imatge seleccionada no es vàlida.</translation>
    </message>
</context>
<context>
    <name>ProxyDialog</name>
    <message>
        <location filename="../src/proxydialog.cpp" line="30"/>
        <source>Proxy Configuration</source>
        <translation>Configuració de proxy</translation>
    </message>
    <message>
        <location filename="../src/proxydialog.cpp" line="38"/>
        <source>Do not use a proxy</source>
        <translation>No fer servir un proxy</translation>
    </message>
    <message>
        <location filename="../src/proxydialog.cpp" line="55"/>
        <source>Your proxy username</source>
        <translation>El teu nom d&apos;usuari pel proxy</translation>
    </message>
    <message>
        <location filename="../src/proxydialog.cpp" line="60"/>
        <source>Note: Password is not stored in a secure manner. If you wish, you can leave the field empty, and you&apos;ll be prompted for the password on startup.</source>
        <translation>Nota: La contrasenya no s&apos;emmagatzema de forma segura. Si ho desitges, pots deixar el camp buit, i se&apos;t demanarà la contrasenya a l&apos;inici.</translation>
    </message>
    <message>
        <location filename="../src/proxydialog.cpp" line="74"/>
        <source>&amp;Save</source>
        <translation>&amp;Guardar</translation>
    </message>
    <message>
        <location filename="../src/proxydialog.cpp" line="80"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Cancel·lar</translation>
    </message>
    <message>
        <location filename="../src/proxydialog.cpp" line="95"/>
        <source>Proxy &amp;Type</source>
        <translation>&amp;Tipus de proxy</translation>
    </message>
    <message>
        <location filename="../src/proxydialog.cpp" line="97"/>
        <source>&amp;Hostname</source>
        <translation>&amp;Servidor</translation>
    </message>
    <message>
        <location filename="../src/proxydialog.cpp" line="99"/>
        <source>&amp;Port</source>
        <translation>&amp;Port</translation>
    </message>
    <message>
        <location filename="../src/proxydialog.cpp" line="101"/>
        <source>Use &amp;Authentication</source>
        <translation>Utilitzar &amp;autenticació</translation>
    </message>
    <message>
        <location filename="../src/proxydialog.cpp" line="103"/>
        <source>&amp;User</source>
        <translation>&amp;Usuari</translation>
    </message>
    <message>
        <location filename="../src/proxydialog.cpp" line="105"/>
        <source>Pass&amp;word</source>
        <translation>Con&amp;trasenya</translation>
    </message>
</context>
<context>
    <name>Publisher</name>
    <message>
        <location filename="../src/publisher.cpp" line="151"/>
        <location filename="../src/publisher.cpp" line="184"/>
        <location filename="../src/publisher.cpp" line="431"/>
        <location filename="../src/publisher.cpp" line="448"/>
        <source>Public</source>
        <translation>Públic</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="156"/>
        <location filename="../src/publisher.cpp" line="189"/>
        <location filename="../src/publisher.cpp" line="435"/>
        <location filename="../src/publisher.cpp" line="452"/>
        <source>Followers</source>
        <translation>Seguidors</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="252"/>
        <source>Picture</source>
        <translation>Foto</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="258"/>
        <source>Audio</source>
        <translation>Àudio</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="264"/>
        <source>Video</source>
        <translation>Vídeo</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="286"/>
        <source>Ad&amp;d...</source>
        <translation>&amp;Afegir...</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="288"/>
        <source>Upload media, like pictures or videos</source>
        <translation>Pujar multimèdia, com fotos o vídeos</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="801"/>
        <source>Select Picture...</source>
        <translation>Seleccionar foto...</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="69"/>
        <source>Title</source>
        <translation>Títol</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="803"/>
        <source>Find the picture in your folders</source>
        <translation>Trobar la foto a les teves carpetes</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="171"/>
        <location filename="../src/publisher.cpp" line="204"/>
        <source>People...</source>
        <translation>Persones...</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="178"/>
        <source>Select who will see this post</source>
        <translation>Selecciona qui veurà aquest missatge</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="176"/>
        <source>To...</source>
        <translation>Per a...</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="161"/>
        <location filename="../src/publisher.cpp" line="194"/>
        <source>Lists</source>
        <translation>Llistes</translation>
    </message>
    <message>
        <source>CC...</source>
        <translation type="obsolete">CC...</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="211"/>
        <source>Select who will get a copy of this post</source>
        <translation>Selecciona qui rebrà una còpia d&apos;aquest missatge</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="270"/>
        <source>Other</source>
        <comment>as in other kinds of files</comment>
        <translation>Altres</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="320"/>
        <source>Cancel</source>
        <translation>Cancel·lar</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="322"/>
        <source>Cancel the post</source>
        <translation>Cancel·lar el missatge</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="805"/>
        <source>Picture not set</source>
        <translation>No s&apos;ha escollit una foto</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="824"/>
        <source>Select Audio File...</source>
        <translation>Seleccionar arxiu d&apos;àudio...</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="826"/>
        <source>Find the audio file in your folders</source>
        <translation>Trobar l&apos;arxiu d&apos;audio a les teves carpetes</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="828"/>
        <source>Audio file not set</source>
        <translation>No s&apos;ha escollit un arxiu d&apos;àudio</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="848"/>
        <source>Select Video...</source>
        <translation>Seleccionar vídeo...</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="850"/>
        <source>Find the video in your folders</source>
        <translation>Trobar el vídeo a les teves carpetes</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="852"/>
        <source>Video not set</source>
        <translation>No s&apos;ha escollit un vídeo</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="871"/>
        <source>Select File...</source>
        <translation>Seleccionar arxiu...</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="873"/>
        <source>Find the file in your folders</source>
        <translation>Trobar l&apos;arxiu a les teves carpetes</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="875"/>
        <source>File not set</source>
        <translation>No s&apos;ha escollit un arxiu</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="912"/>
        <location filename="../src/publisher.cpp" line="955"/>
        <source>Error: Already composing</source>
        <translation>Error: Ja s&apos;està redactant</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="913"/>
        <source>You can&apos;t edit a post at this time, because a post is already being composed.</source>
        <translation>No pots editar un missatge en aquest moment, perque ja s&apos;està redactant un missatge.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="929"/>
        <source>Update</source>
        <translation>Actualitzar</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="942"/>
        <source>Editing post</source>
        <translation>Editant missatge</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="956"/>
        <source>You can&apos;t create a message for %1 at this time, because a post is already being composed.</source>
        <translation>No pots crear un missatge per a %1 en aquest moment, perque ja s&apos;està redactant un missatge.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1022"/>
        <source>Posting failed.

Try again.</source>
        <translation>Ha fallat la publicació.

Torna-ho a provar.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1311"/>
        <source>Warning: You have no followers yet</source>
        <translation>Avís: Encara no tens seguidors</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1312"/>
        <source>You&apos;re trying to post to your followers only, but you don&apos;t have any followers yet.</source>
        <translation>Estàs intentant publicar només per als teus seguidors, però no tens cap seguidor encara.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1316"/>
        <source>If you post like this, no one will be able to see your message.</source>
        <translation>Si publiques d&apos;aquesta manera, ningú podrà veure el teu missatge.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1324"/>
        <source>&amp;Cancel, go back to the post</source>
        <translation>&amp;Cancel·lar, tornar al missatge</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1374"/>
        <source>Updating...</source>
        <translation>Actualitzant...</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1385"/>
        <source>Post is empty.</source>
        <translation>El missatge està buit.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1389"/>
        <source>File not selected.</source>
        <translation>No s&apos;ha seleccionat un arxiu.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1410"/>
        <source>Select one image</source>
        <translation>Selecciona una imatge</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1411"/>
        <source>Image files</source>
        <translation>Arxius d&apos;imatge</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1440"/>
        <source>Select one file</source>
        <translation>Selecciona un arxiu</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1443"/>
        <source>Invalid file</source>
        <translation>Arxiu no vàlid</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1444"/>
        <source>The file type cannot be detected.</source>
        <translation>El tipus d&apos;arxiu no es pot detectar.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1451"/>
        <source>All files</source>
        <translation>Tots els arxius</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1466"/>
        <source>Since you&apos;re uploading an image, you could scale it down a little or save it in a more compressed format, like JPG.</source>
        <translation>Com que el que estàs pujant és una imatge, podries reduir-la una mica o guardarla en un format més comprimit, com JPG.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1472"/>
        <source>Dianara currently limits file uploads to 10 MiB per post, to prevent possible storage or network problems in the servers.</source>
        <translation>Actualment, Dianara limita les pujades d&apos;arxius a 10 MiB per missatge, per a evitar possibles problemes d&apos;emmagatzematge, o de xarxa, als servidors.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1477"/>
        <source>This is a temporary measure, since the servers cannot set their own limits yet.</source>
        <translation>Això és una mesura temporal, ja que els servidors no poden establir els seus propis límits encara.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1471"/>
        <source>File is too big</source>
        <translation>L&apos;arxiu és massa gran</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="78"/>
        <source>Add a brief title for the post here (recommended)</source>
        <translation>Afegeix un títol breu per al missatge aquí (recomanat)</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1319"/>
        <source>Do you want to make the post public instead of followers-only?</source>
        <translation>Vols fer el missatge públic en comptes de només per seguidors?</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1322"/>
        <source>&amp;Yes, make it public</source>
        <translation>&amp;Sí, fer-ho públic</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1482"/>
        <source>Sorry for the inconvenience.</source>
        <translation>Lamentem les molèsties.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1502"/>
        <source>Resolution</source>
        <translation>Resolució</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1513"/>
        <source>Type</source>
        <translation>Tipus</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1515"/>
        <source>Size</source>
        <translation>Mida</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1555"/>
        <source>%1 KiB of %2 KiB uploaded</source>
        <translation>%1 KiB de %2 KiB pujats</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1414"/>
        <source>Invalid image</source>
        <translation>Imatge no vàlida</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="64"/>
        <source>Setting a title helps make the Meanwhile feed more informative</source>
        <translation>Afegir un títol ajuda a fer el contingut del &quot;Mentrestant&quot; més informatiu</translation>
    </message>
    <message>
        <source>Add a brief title for the post (recommended)</source>
        <translation type="obsolete">Afegeix un títol breu per al missatge (recomanat)</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="106"/>
        <source>Remove</source>
        <translatorcomment>Treure? Eliminar?</translatorcomment>
        <translation>Treure</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="108"/>
        <source>Cancel the attachment, and go back to a regular note</source>
        <translation>Cancel·lar l&apos;adjunt i tornar a una nota normal</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="209"/>
        <source>Cc...</source>
        <translation>Cc...</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="312"/>
        <location filename="../src/publisher.cpp" line="713"/>
        <source>Post</source>
        <comment>verb</comment>
        <translation>Publicar</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1323"/>
        <source>&amp;No, post to my followers only</source>
        <translation>&amp;No, publicar només per als meus seguidors</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1415"/>
        <source>The image format cannot be detected.
The extension might be wrong, like a GIF image renamed to image.jpg or similar.</source>
        <translation>No es pot detectar el format de la imatge.
Potser l&apos;extensió està equivocada, com una imatge GIF reanomenada a imatge.jpg o semblant.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1421"/>
        <source>Select one audio file</source>
        <translation>Selecciona un arxiu d&apos;àudio</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1422"/>
        <source>Audio files</source>
        <translation>Arxius d&apos;àudio</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1425"/>
        <source>Invalid audio file</source>
        <translation>Arxiu d&apos;àudio no vàlid</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1426"/>
        <source>The audio format cannot be detected.</source>
        <translation>El format d&apos;àudio no es pot detectar.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1430"/>
        <source>Select one video file</source>
        <translation>Selecciona un arxiu de vídeo</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1431"/>
        <source>Video files</source>
        <translation>Arxius de vídeo</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1435"/>
        <source>Invalid video file</source>
        <translation>Arxiu de vídeo no vàlid</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1436"/>
        <source>The video format cannot be detected.</source>
        <translation>El format de vídeo no es pot detectar.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1344"/>
        <source>Posting...</source>
        <translation>Publicant...</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="314"/>
        <source>Hit Control+Enter to post with the keyboard</source>
        <translation>Prem Control+Enter per publicar amb el teclat</translation>
    </message>
</context>
<context>
    <name>PumpController</name>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="840"/>
        <source>Getting likes...</source>
        <translatorcomment>meh....</translatorcomment>
        <translation>Rebent &quot;likes&quot;...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="872"/>
        <source>Getting comments...</source>
        <translation>Rebent comentaris...</translation>
    </message>
    <message>
        <source>Getting minor feed...</source>
        <translatorcomment>meh...</translatorcomment>
        <translation type="obsolete">Rebent línia temporal menor...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1524"/>
        <source>Error connecting to %1</source>
        <translation>Error connectant a %1</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1533"/>
        <source>Unhandled HTTP error code %1</source>
        <translation>Codi d&apos;error HTTP no gestionat: %1</translation>
    </message>
    <message>
        <source>Post published successfully.</source>
        <translation type="obsolete">Missatge publicat correctament.</translation>
    </message>
    <message>
        <source>Comment posted successfully.</source>
        <translation type="obsolete">Comentari publicat correctament.</translation>
    </message>
    <message>
        <source>Minor feed received.</source>
        <translatorcomment>meh...</translatorcomment>
        <translation type="obsolete">Línia temporal menor rebuda.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2036"/>
        <source>Following %1 (%2) successfully.</source>
        <comment>%1 is a person&apos;s name, %2 is the ID</comment>
        <translation>Seguint a %1 (%2) correctament.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2046"/>
        <source>Stopped following %1 (%2) successfully.</source>
        <comment>%1 is a person&apos;s name, %2 is the ID</comment>
        <translation>S&apos;ha deixat de seguir a %1 (%2) correctament.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2096"/>
        <source>List of &apos;following&apos; completely received.</source>
        <translation>Llista de &apos;seguint&apos; completament rebuda.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2104"/>
        <source>Partial list of &apos;following&apos; received.</source>
        <translation>Part de la llista de &apos;seguint&apos; rebuda.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2127"/>
        <source>List of &apos;followers&apos; completely received.</source>
        <translation>Llista de &apos;seguidors&apos; completament rebuda.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2133"/>
        <source>Partial list of &apos;followers&apos; received.</source>
        <translation>Part de la llista de &apos;seguidors&apos; rebuda.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2191"/>
        <source>Person list &apos;%1&apos; created successfully.</source>
        <translation>Llista de persones &apos;%1&apos; creada correctament.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2222"/>
        <source>Person list received.</source>
        <translation>Llista de persones rebuda.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2390"/>
        <source>File uploaded successfully. Posting message...</source>
        <translation>Arxiu penjat correctament. Publicant missatge...</translation>
    </message>
    <message>
        <source>Timeline received. Updating post list...</source>
        <translation type="obsolete">S&apos;ha rebut la línia temporal. Actualitzant llista de missatges...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="236"/>
        <source>Authorized to use account %1. Getting initial data.</source>
        <translation>Autoritzat per fer servir el compte %1. Rebent dades inicials.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="241"/>
        <source>There is no authorized account.</source>
        <translation>No hi ha cap compte autoritzat.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="508"/>
        <source>Getting list of &apos;Following&apos;...</source>
        <translation>Rebent llista de &apos;Seguint&apos;...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="520"/>
        <source>Getting list of &apos;Followers&apos;...</source>
        <translation>Rebent llista de &apos;Seguidors&apos;...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="573"/>
        <source>Getting list of person lists...</source>
        <translation>Rebent llista de llistes de persones...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="609"/>
        <source>Creating person list...</source>
        <translation>Creant llista de persones...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="621"/>
        <source>Deleting person list...</source>
        <translation>Esborrant llista de persones...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="636"/>
        <source>Getting a person list...</source>
        <translation>Rebent una llista de persones...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="680"/>
        <source>Adding person to list...</source>
        <translation>Afegint una persona a una llista...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="722"/>
        <source>Removing person from list...</source>
        <translation>Treient una persona d&apos;una llista...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="770"/>
        <source>Creating group...</source>
        <translation>Creant grup...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="800"/>
        <source>Joining group...</source>
        <translation>Unint-se al grup...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="825"/>
        <source>Leaving group...</source>
        <translation>Sortint del grup...</translation>
    </message>
    <message>
        <source>Main timeline update requested, but updates are blocked.</source>
        <translation type="obsolete">Actualització de línia temporal principal sol·licitada, però les actualitzacions estan bloquejades.</translation>
    </message>
    <message>
        <source>Getting main timeline...</source>
        <translation type="obsolete">Rebent línia temporal principal...</translation>
    </message>
    <message>
        <source>Direct timeline update requested, but updates are blocked.</source>
        <translation type="obsolete">Actualització de línia temporal directa sol·licitada, però les actualitzacions estan bloquejades.</translation>
    </message>
    <message>
        <source>Getting direct messages timeline...</source>
        <translation type="obsolete">Rebent línia temporal de missatges directes...</translation>
    </message>
    <message>
        <source>Activity timeline update requested, but updates are blocked.</source>
        <translation type="obsolete">Actualització de línia temporal d&apos;activitat sol·licitada, però les actualitzacions estan bloquejades.</translation>
    </message>
    <message>
        <source>Getting activity timeline...</source>
        <translation type="obsolete">Rebent línia temporal d&apos;activitat...</translation>
    </message>
    <message>
        <source>Favorites timeline update requested, but updates are blocked.</source>
        <translation type="obsolete">Actualització de línia temporal de favorits sol·licitada, però les actualitzacions estan bloquejades.</translation>
    </message>
    <message>
        <source>Getting favorites timeline...</source>
        <translation type="obsolete">Rebent línia temporal de favorits...</translation>
    </message>
    <message>
        <source>Timeline update requested, but updates are blocked.</source>
        <translation type="obsolete">Actualització de línia temporal sol·licitada, però les actualitzacions estan bloquejades.</translation>
    </message>
    <message>
        <source>Getting timeline...</source>
        <translation type="obsolete">Rebent línia temporal...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="971"/>
        <source>Timeline</source>
        <translation>Línia temporal</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="974"/>
        <source>Messages</source>
        <translation>Missatges</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="994"/>
        <source>User timeline</source>
        <translation>Línia temporal d&apos;usuari</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1113"/>
        <source>Uploading %1</source>
        <comment>1=filename</comment>
        <translation>Pujant %1</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1375"/>
        <source>HTTP error</source>
        <comment>For the following HTTP error codesyou can check http://en.wikipedia.org/wiki/List_of_HTTP_status_codes in your language</comment>
        <translation>Error HTTP</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1387"/>
        <source>Gateway Timeout</source>
        <comment>HTTP 504 error string</comment>
        <translation>Temps d&apos;espera de la passarel·la excedit</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1397"/>
        <source>Service Unavailable</source>
        <comment>HTTP 503 error string</comment>
        <translation>Servei no disponible</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1415"/>
        <source>Not Implemented</source>
        <comment>HTTP 501 error string</comment>
        <translation>No implementat</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1425"/>
        <source>Internal Server Error</source>
        <comment>HTTP 500 error string</comment>
        <translation>Error intern</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1445"/>
        <source>Gone</source>
        <comment>HTTP 410 error string</comment>
        <translation>Ja no disponible</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1455"/>
        <source>Not Found</source>
        <comment>HTTP 404 error string</comment>
        <translation>No trobat</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1465"/>
        <source>Forbidden</source>
        <comment>HTTP 403 error string</comment>
        <translation>Prohibit</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1475"/>
        <source>Unauthorized</source>
        <comment>HTTP 401 error string</comment>
        <translation>No autoritzat</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1486"/>
        <source>Bad Request</source>
        <comment>HTTP 400 error string</comment>
        <translation>Sol·licitud incorrecta</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1505"/>
        <source>Moved Temporarily</source>
        <comment>HTTP 302 error string</comment>
        <translation>Mogut temporalment</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1515"/>
        <source>Moved Permanently</source>
        <comment>HTTP 301 error string</comment>
        <translation>Mogut permanentment</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1651"/>
        <source>Profile received.</source>
        <translation>Perfil rebut.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1653"/>
        <source>Followers</source>
        <translation>Seguidors</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1656"/>
        <source>Following</source>
        <translation>Seguint</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1670"/>
        <source>Profile updated.</source>
        <translation>Perfil actualitzat.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1679"/>
        <source>E-mail updated: %1</source>
        <translation>E-mail actualitzat: %1</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1707"/>
        <source>%1 published successfully. Updating post content...</source>
        <comment>%1 is the type of object: note, image...</comment>
        <translation>S&apos;ha publicat &apos;%1&apos; correctament. Actualitzant contingut del missatge...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1723"/>
        <source>Untitled post %1 published successfully.</source>
        <comment>%1 is a piece of the post</comment>
        <translation>Missatge sense títol %1 publicat correctament.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1727"/>
        <source>Post %1 published successfully.</source>
        <comment>%1 is the title of the post</comment>
        <translation>Missatge %1 publicat correctament.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1744"/>
        <source>Avatar published successfully.</source>
        <translation>Avatar publicat correctament.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1775"/>
        <source>Untitled post %1 updated successfully.</source>
        <comment>%1 is a piece of the post</comment>
        <translation>Missatge sense títol %1 actualitzat correctament.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1779"/>
        <source>Post %1 updated successfully.</source>
        <comment>%1 is the title of the post</comment>
        <translation>Missatge %1 actualitzat correctament.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1789"/>
        <source>Comment %1 updated successfully.</source>
        <comment>%1 is a piece of the comment</comment>
        <translation>Comentari %1 actualitzat correctament.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1837"/>
        <source>Comment %1 posted successfully.</source>
        <comment>%1 is a piece of the comment</comment>
        <translation>Comentari %1 publicat correctament.</translation>
    </message>
    <message>
        <source>Post updated successfully.</source>
        <translation type="obsolete">Missatge actualitzat correctament.</translation>
    </message>
    <message>
        <source>Comment updated successfully.</source>
        <translation type="obsolete">Comentari actualitzat correctament.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1803"/>
        <source>Message liked or unliked successfully.</source>
        <translation>Missatge marcat o desmarcat &quot;M&apos;agrada&quot; correctament.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1817"/>
        <source>Likes received.</source>
        <translatorcomment>meh...</translatorcomment>
        <translation>&quot;M&apos;agrada&quot; rebuts.</translation>
    </message>
    <message>
        <source>Comment &apos;%1&apos; posted successfully.</source>
        <comment>%1 is a piece of the comment</comment>
        <translation type="obsolete">Comentari &apos;%1&apos; publicat correctament.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1862"/>
        <source>1 comment received.</source>
        <translation>1 comentari rebut.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1866"/>
        <source>%1 comments received.</source>
        <translation>%1 comentaris rebuts.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1893"/>
        <source>Post by %1 shared successfully.</source>
        <comment>1=author of the post we are sharing</comment>
        <translation>Missatge de %1 compartit correctament.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1924"/>
        <source>Received &apos;%1&apos;.</source>
        <comment>%1 is the name of a feed</comment>
        <translation>S&apos;ha rebut &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1937"/>
        <source>Adding items...</source>
        <translation>Afegint elements...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2443"/>
        <source>SSL errors in connection to %1!</source>
        <translation>Errors d&apos;SSL a la connexió a %1!</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2455"/>
        <source>Loading external image from %1 regardless of SSL errors, as configured...</source>
        <comment>%1 is a hostname</comment>
        <translation>Carregant imatge externa de %1 tot i amb els errors SSL, com s&apos;ha configurat...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2655"/>
        <source>OAuth error while authorizing application.</source>
        <translation>Error de OAuth mentre s&apos;autoritzava a l&apos;aplicació.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2020"/>
        <source>Message deleted successfully.</source>
        <translation>Missatge eliminat correctament.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="867"/>
        <source>The comments for this post cannot be loaded due to missing data on the server.</source>
        <translation>Els comentaris d&apos;aquest missatge no es poden carregar a causa de que falten dades al servidor.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="924"/>
        <source>Getting &apos;%1&apos;...</source>
        <comment>%1 is the name of a feed</comment>
        <translation>Rebent &apos;%1&apos;...</translation>
    </message>
    <message>
        <source>Main timeline</source>
        <translation type="obsolete">Línia temporal principal</translation>
    </message>
    <message>
        <source>Direct messages</source>
        <translation type="obsolete">Missatges directes</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="977"/>
        <source>Activity</source>
        <translation>Activitat</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="980"/>
        <source>Favorites</source>
        <translation>Favorits</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="984"/>
        <source>Meanwhile</source>
        <translation>Mentrestant</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="987"/>
        <source>Mentions</source>
        <translation>Mencions</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="990"/>
        <source>Actions</source>
        <translation>Accions</translation>
    </message>
    <message>
        <source>Getting &apos;%1&apos;...</source>
        <comment>%1 is a feed&apos;s name</comment>
        <translation type="obsolete">Rebent &quot;%1&quot;...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2170"/>
        <source>List of &apos;lists&apos; received.</source>
        <translation>Llista de &apos;llistes&apos; rebuda.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2202"/>
        <source>Person list deleted successfully.</source>
        <translation>Llista de persones esborrada correctament.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2257"/>
        <source>%1 (%2) added to list successfully.</source>
        <comment>1=contact name, 2=contact ID</comment>
        <translation>S&apos;ha afegit a %1 (%2) a la llista correctament.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2277"/>
        <source>%1 (%2) removed from list successfully.</source>
        <comment>1=contact name, 2=contact ID</comment>
        <translation>S&apos;ha eliminat a %1 (%2) de la llista correctament.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2288"/>
        <source>Group %1 created successfully.</source>
        <translation>Grup %1 creat correctament.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2299"/>
        <source>Group %1 joined successfully.</source>
        <translation>S&apos;ha entrat al grup %1 correctament.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2312"/>
        <source>Left the %1 group successfully.</source>
        <translation>S&apos;ha sortit del grup %1 correctament.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2353"/>
        <source>File downloaded successfully.</source>
        <translation>Arxiu descarregat correctament.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2399"/>
        <source>Avatar uploaded.</source>
        <translation>Avatar penjat.</translation>
    </message>
    <message>
        <source>Loading embedded image from %1 regardless of SSL errors, as configured</source>
        <comment>%1 is a hostname</comment>
        <translation type="obsolete">Carregant imatge incrustada des de %1 tot i amb els errors SSL, com s&apos;ha configurat</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2484"/>
        <source>The application is not registered with your server yet. Registering...</source>
        <translation>La aplicació encara no es troba registrada amb el teu servidor. Registrant...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2521"/>
        <source>Getting OAuth token...</source>
        <translation>Rebent identificador d&apos;autorització (token) d&apos;OAuth...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2542"/>
        <source>OAuth support error</source>
        <translation>Error de compatibilitat d&apos;OAuth</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2543"/>
        <source>Your installation of QOAuth, a library used by Dianara, doesn&apos;t seem to have HMAC-SHA1 support.</source>
        <translation>Sembla que la teva instal·lació de QOAuth, una biblioteca utilitzada per Dianara, no te compatibilitat amb HMAC-SHA1.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2547"/>
        <source>You probably need to install the OpenSSL plugin for QCA: %1, %2 or similar.</source>
        <translation>Probablement necessitis instal·lar el connector d&apos;OpenSSL per QCA: %1, %2 o similar.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2604"/>
        <source>Authorization error</source>
        <translation>Error d&apos;autorització</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2605"/>
        <source>There was an OAuth error while trying to get the authorization token.</source>
        <translation>Hi ha hagut un error de OAuth mentre s&apos;intentava obtenir un identificador d&apos;autorització (token).</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2608"/>
        <source>QOAuth error %1</source>
        <translation>Error de QOAuth %1</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2640"/>
        <source>Application authorized successfully.</source>
        <translation>Aplicació autoritzada correctament.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2678"/>
        <source>Waiting for proxy password...</source>
        <translation>Esperant per la contrasenya del proxy...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2707"/>
        <source>Still waiting for profile. Trying again...</source>
        <translation>Encara s&apos;està esperant el perfil. Intentant-ho de nou...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2816"/>
        <source>Some initial data was not received. Restarting initialization.</source>
        <translation>Algunes dades inicials no s&apos;han rebut. Reiniciant la inicialització.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2824"/>
        <source>Some initial data was not received after several attempts. Something might be wrong with your server. You might still be able to use the service normally.</source>
        <translation>Algunes dades inicials no s&apos;han rebut després de diversos intents. Alguna cosa pot estar fallant al teu servidor. Es possible que puguis utilitzar el servei amb normalitat.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2834"/>
        <source>All initial data received. Initialization complete.</source>
        <translation>S&apos;han rebut totes les dades inicials. Inicialització completada.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2845"/>
        <source>Ready.</source>
        <translation>Preparat.</translation>
    </message>
</context>
<context>
    <name>TimeLine</name>
    <message>
        <location filename="../src/timeline.cpp" line="67"/>
        <source>Welcome to Dianara</source>
        <translation>Benvingut a Dianara</translation>
    </message>
    <message>
        <source>Dianara is a &lt;b&gt;pump.io&lt;/b&gt; client.</source>
        <translation type="obsolete">Dianara es un client &lt;b&gt;pump.io&lt;/b&gt;.</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="69"/>
        <source>Dianara is a &lt;b&gt;Pump.io&lt;/b&gt; client.</source>
        <translation>Dianara és un client &lt;b&gt;Pump.io&lt;/b&gt;.</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="79"/>
        <source>Press &lt;b&gt;F1&lt;/b&gt; if you want to open the Help window.</source>
        <translation>Prem &lt;b&gt;F1&lt;/b&gt; si vols obrir la finestra d&apos;ajuda.</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="82"/>
        <source>First, configure your account from the &lt;b&gt;Settings - Account&lt;/b&gt; menu.</source>
        <translation>En primer lloc, configura el teu compte des del menú &lt;b&gt;Configuració - Compte&lt;/b&gt;.</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="85"/>
        <source>After the process is done, your profile and timelines should update automatically.</source>
        <translation>Quan el procés estigui llest, el teu perfil i línies temporals haurien d&apos;actualitzar-se automàticament.</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="89"/>
        <source>Take a moment to look around the menus and the Configuration window.</source>
        <translation>Pren-te un moment per fer una ullada als menús i la finestra de Configuració.</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="93"/>
        <source>You can also set your profile data and picture from the &lt;b&gt;Settings - Edit Profile&lt;/b&gt; menu.</source>
        <translation>També pots omplir la teva informació de perfil i foto des del menú &lt;b&gt;Configuració - Editar perfil&lt;/b&gt;.</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="104"/>
        <source>Dianara&apos;s blog</source>
        <translation>Bloc de Dianara</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="210"/>
        <source>Newest</source>
        <translation>El més nou</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="233"/>
        <source>Newer</source>
        <translation>Més nous</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="240"/>
        <source>Older</source>
        <translation>Més antics</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="349"/>
        <source>Requesting...</source>
        <translation>Demanant...</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="443"/>
        <source>Loading...</source>
        <translation>Carregant...</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="565"/>
        <source>Page %1 of %2.</source>
        <translation>Pàgina %1 de %2.</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="569"/>
        <source>Showing %1 posts per page.</source>
        <translation>Mostrant %1 missatges per pàgina.</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="572"/>
        <source>%1 posts in total.</source>
        <translation>%1 missatges en total.</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="576"/>
        <source>Click here or press Control+G to jump to a specific page</source>
        <translation>Fes clic aquí o prem Control+G per saltar a una pàgina específica</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="672"/>
        <source>&apos;%1&apos; cannot be updated because a comment is currently being composed.</source>
        <comment>%1 = feed&apos;s name</comment>
        <translation>No es pot actualitzar &apos;%1&apos; perquè s&apos;està editant un comentari.</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="764"/>
        <source>%1 more posts pending for next update.</source>
        <translation>%1 missatges més pendents per la propera actualització.</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="770"/>
        <source>Click here to receive them now.</source>
        <translation>Fes clic aquí per rebre&apos;ls ara.</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="1029"/>
        <source>There are no posts</source>
        <translation>No hi ha missatges</translation>
    </message>
    <message>
        <source>Get %1 pending posts</source>
        <translation type="obsolete">Rebre %1 missatges pendents</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="72"/>
        <source>If you don&apos;t have a Pump account yet, you can get one at the following address, for instance:</source>
        <translation>Si encara no tens un compte Pump, pots aconseguir-ne un a la següent adreça, per exemple:</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="97"/>
        <source>There are tooltips everywhere, so if you hover over a button or a text field with your mouse, you&apos;ll probably see some extra information.</source>
        <translation>Hi ha indicadors de funció (tooltips) per tot arreu, pel que si mantens el ratolí sobre un botó o un camp de text, és probable que vegis alguna informació addicional.</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="106"/>
        <source>Pump.io User Guide</source>
        <translation>Guia d&apos;usuari de Pump.io</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="116"/>
        <source>Direct Messages Timeline</source>
        <translation>Línia temporal de missatges directes</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="117"/>
        <source>Here, you&apos;ll see posts specifically directed to you.</source>
        <translation>Aquí veuràs els missatges dirigits específicament a tu.</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="126"/>
        <source>Activity Timeline</source>
        <translation>Línia temporal d&apos;activitat</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="127"/>
        <source>You&apos;ll see your own posts here.</source>
        <translation>Aquí veuràs els teus propis missatges.</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="135"/>
        <source>Favorites Timeline</source>
        <translation>Línia temporal de favorits</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="136"/>
        <source>Posts and comments you&apos;ve liked.</source>
        <translation>Missatges i comentaris que t&apos;han agradat.</translation>
    </message>
</context>
<context>
    <name>Timestamp</name>
    <message>
        <location filename="../src/timestamp.cpp" line="61"/>
        <source>Invalid timestamp!</source>
        <translation>Hora/data no vàlida!</translation>
    </message>
    <message>
        <location filename="../src/timestamp.cpp" line="93"/>
        <source>A minute ago</source>
        <translation>Fa un minut</translation>
    </message>
    <message>
        <location filename="../src/timestamp.cpp" line="97"/>
        <source>%1 minutes ago</source>
        <translation>Fa %1 minuts</translation>
    </message>
    <message>
        <location filename="../src/timestamp.cpp" line="108"/>
        <source>An hour ago</source>
        <translation>Fa una hora</translation>
    </message>
    <message>
        <location filename="../src/timestamp.cpp" line="112"/>
        <source>%1 hours ago</source>
        <translation>Fa %1 hores</translation>
    </message>
    <message>
        <location filename="../src/timestamp.cpp" line="79"/>
        <source>Just now</source>
        <translation>Ara mateix</translation>
    </message>
    <message>
        <location filename="../src/timestamp.cpp" line="83"/>
        <source>In the future</source>
        <translation>En el futur</translation>
    </message>
    <message>
        <location filename="../src/timestamp.cpp" line="123"/>
        <source>Yesterday</source>
        <translation>Ahir</translation>
    </message>
    <message>
        <location filename="../src/timestamp.cpp" line="127"/>
        <source>%1 days ago</source>
        <translation>Fa %1 dies</translation>
    </message>
    <message>
        <location filename="../src/timestamp.cpp" line="138"/>
        <source>A month ago</source>
        <translation>Fa un mes</translation>
    </message>
    <message>
        <location filename="../src/timestamp.cpp" line="142"/>
        <source>%1 months ago</source>
        <translation>Fa %1 mesos</translation>
    </message>
    <message>
        <location filename="../src/timestamp.cpp" line="151"/>
        <source>A year ago</source>
        <translation>Fa un any</translation>
    </message>
    <message>
        <location filename="../src/timestamp.cpp" line="155"/>
        <source>%1 years ago</source>
        <translation>Fa %1 anys</translation>
    </message>
</context>
<context>
    <name>UserPosts</name>
    <message>
        <location filename="../src/userposts.cpp" line="37"/>
        <source>Posts by %1</source>
        <translation>Missatges de %1</translation>
    </message>
    <message>
        <location filename="../src/userposts.cpp" line="62"/>
        <source>Loading...</source>
        <translation>Carregant...</translation>
    </message>
    <message>
        <location filename="../src/userposts.cpp" line="64"/>
        <source>&amp;Close</source>
        <translation>&amp;Tancar</translation>
    </message>
    <message>
        <location filename="../src/userposts.cpp" line="146"/>
        <source>Received &apos;%1&apos;.</source>
        <translation>S&apos;ha rebut &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="../src/userposts.cpp" line="152"/>
        <source>%1 posts</source>
        <translation>%1 missatges</translation>
    </message>
</context>
</TS>
