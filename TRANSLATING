
 Dianara - A Pump.io client
 Copyright 2012-2015  JanKusanagi JRR <jancoding@gmx.com>

===============================================================================

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the
   Free Software Foundation, Inc.,
   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA.

   Or visit http://www.gnu.org/licenses/

===============================================================================


Dianara is translated (from its original English) to a few languages, mainly
Spanish, Catalan, Italian and German. There is a Polish translation in the
works, but it could use some help.

If you wish to contribute a new language, or help updating an existing one,
let me know via Pump.io (jankusanagi@identi.ca) or via e-mail, at the address
at the top of the document, so I can make sure that two people aren't working
on the same thing.


Here's a basic explanation of how the translation process is done:



Getting started
===============================================================================

If you want to translate Dianara to your language, you'll need to edit the
corresponging TS file from the /translations folder. These .ts files can be
edited with any plain text editor, but the best way to edit them is probably
using the Qt Linguist program.
(Qt Linguist manual: http://qt-project.org/doc/qt-4.8/linguist-manual.html)


For instance, if you wish to translate the program into French, you'll have
to open dianara_fr.ts, included with the source code of Dianara, in Qt Linguist.
The language code used is often the same as your country's Internet domain.

If you start Dianara from a terminal, you'll see something like:

   "Using translation file: :/translations/dianara_fr_FR.UTF8"

which indicates that your system uses fr_FR.UTF8 as language locale, and the
file you should edit is dianara_fr.ts, and generate a "compiled" language file
dianara_fr.qm.

Dianara should load the language configured in your operating system.
If you want to force it to use a specifc language, you can run it with
a specific LANG variable, like this:

LANG=fr ./dianara


If there's no file for your language, you can make a copy of dianara_EMPTY.ts
and rename it to your language's code, such as dianara_ja.ts for Japanese.
If you do this, make sure your file is not overwritten by mine when I add your
new language to the project; mine would be empty. This mainly applies to people
who clone the code using git.


*** If you have any doubts, or if the file for your language doesn't exist yet,
just contact me on Pump.io (jankusanagi@identi.ca) or via e-mail, at the address
at the top of this document.



Translating
===============================================================================

The translation itself is quite simple. There are a lot of "strings" (sentences
or words), grouped together based (roughly) on what widget uses them. So, some
are used in the Configuration dialog, some are used in the Publisher widget
(the part of the main window where you compose new posts), etc.


You just need to go through those lists, translating each word or sentence,
while keeping in mind a few simple rules:


- If a string contains %1, %2, etc, those will be replaced by something when
  used in the program. The translated string should contain those same values
  even if they're in different order due to language differences.
  There are sometimes "developer comments" visible in Qt Linguist for these,
  where there's some clarification of what will replace %1, %2, etc. when the
  program is running.

  Ex: "Do you want to share %1's post?" would translate to Spanish as
      "¿Quieres compartir el mensaje de %1?".


- Qt Linguist will warn you if the original string ends with a period and
  the translated string doesn't.


- Strings used in buttons and menus might include an & symbol. Those are used
  to mark which letter is used as keyboard accelerator (or shortcut), and should
  be included in the translated version, even if they're in front of a different
  letter. Qt Linguist will warn you if an accelerator is missing.

  Ex: The "&Help" menu might be translated into Spanish as "A&yuda", since there
      is no "H" in the translated string. When running in English, users will
      be able to access that menu by pressing Alt+H. When running in Spanish,
      that menu will be accessed by pressing Alt+Y.

  One thing to keep in mind is that accelerators that are visible at the same
  time on screen shouldn't use the same letter.
  For instance, "&Session" and "&Settings" shouldn't be in the same menu.
  "&Session" and "S&ettings" would work. There's no warning about this,
  so if the accelerators are duplicated in the translation, you'll see that
  using them in the program doesn't work as expected.
  This is a small problem, but try to avoid it.


Note that the 'HelpWidget' class is quite big and full of very long strings.
It's mostly inline documentation, not user interface, so it could probably
be left for last.



Testing it
===============================================================================

Testing your translation is optional, and it requires building Dianara from
source. There are instructions about that in the INSTALL file.

To test your updated translation, first use the "File > Release" option in Qt
Linguist. That will generate a .qm file. Make sure it's saved to the same
/translations folder as the .ts file. Then rebuild the Dianara binary as usual.

To ensure the newest changes are applied, delete the qrc_dianara.* files in
the build/ folder (if they exist), before rebuilding.



When done
===============================================================================

Once you're done, or when you want to submit your translation even if it's not
yet complete (which is perfectly fine), you can send the .ts file via e-mail to
the address at the top, or you can create a Pull Request in gitlab.com, if
you're comfortable using git.


Thank you in advance!
